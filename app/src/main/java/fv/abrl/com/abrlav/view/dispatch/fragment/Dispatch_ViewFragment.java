package fv.abrl.com.abrlav.view.dispatch.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.utils.FileDownloader;
import fv.abrl.com.abrlav.common.utils.RecyclerItemClickListener;
import fv.abrl.com.abrlav.dispatch.fragment.PrintStoreFragment;
import fv.abrl.com.abrlav.grn.adapter.CollectionCenterListAdapter;
import fv.abrl.com.abrlav.grn.adapter.VendorNameListAdapter;
import fv.abrl.com.abrlav.grn.fragment.GrnVendorSelectionFragment;
import fv.abrl.com.abrlav.grn.paracelable.GetCollectionCenterDataParacelable;
import fv.abrl.com.abrlav.grn.paracelable.GetCollectionCenterListParcelable;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.view.dispatch.adapter.DispatchViewSelectStoreAdapterRv;
import fv.abrl.com.abrlav.view.dispatch.parcelable.DispatchViewStoresListParcelable;
import fv.abrl.com.abrlav.view.dispatch.parcelable.DispatchViewStoresParcelable;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Dispatch_ViewFragment  extends Fragment {

    private Fragment fragment;
    private Unbinder unbinder;
    private View view;
    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;
    public Dispatch_ViewFragment() {

        // Required empty public constructor

    }

    @BindView(R.id.dispatchSpinnerList)
    Spinner dispatchSpinnerList;

    @BindView(R.id.spinnerlist)
    Spinner collectioncenterspinnerlist;

    @BindView(R.id.searchvendorname)
    EditText searchvendorname;


    @BindView(R.id.indicator)
    AVLoadingIndicatorView indicator;

    @BindView(R.id.clearSearchText)
    ImageView clearSearchText;


    @BindView(R.id.storeList)
    RecyclerView storeList;
    DispatchViewSelectStoreAdapterRv mAdapter;

    ArrayList<DispatchViewStoresListParcelable> storelist = new ArrayList<DispatchViewStoresListParcelable>();
    ArrayList<GetCollectionCenterListParcelable> collectioncenterlist = new ArrayList<GetCollectionCenterListParcelable>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view                    =  inflater.inflate(R.layout.fragment_dispatch_view, container, false);
        unbinder                =  ButterKnife.bind(this,view);

        sharedPref               = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                   = sharedPref.edit();



        try{
            ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                    .getColor(R.color.bottombar)));

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "Dispatch View" + "</font>"));

            HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);

            if (Build.VERSION.SDK_INT >= 21) {
                getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
            }


        }catch (Exception e){
            e.printStackTrace();
        }

        mAdapter                =       new DispatchViewSelectStoreAdapterRv(getActivity(), storelist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        storeList.setLayoutManager(mLayoutManager);
        storeList.setItemAnimator(new DefaultItemAnimator());
        storeList.setAdapter(mAdapter);





        try{
            dispatchList();
        }catch (Exception e){
            e.printStackTrace();
        }

        try{
            getCollectionCenterlist();
        }catch (Exception e){
            e.printStackTrace();
        }

        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        List<String> datelist = new ArrayList<String>();
        datelist.add("Today");
        datelist.add("Yesterday");

        // Creating adapter for spinner
        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datelist);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dispatchSpinnerList.setAdapter(categoryAdapter);


        storeList.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        try{
                           Log.e("url-jj",DispatchViewSelectStoreAdapterRv.filteredarraylist.get(position).getPdf_path());
                            indicator.setVisibility(View.VISIBLE);
                            new DownloadFile().execute(ApiConstants.pdfurl+"/"+DispatchViewSelectStoreAdapterRv.filteredarraylist.get(position).getPdf_path().replace(" ","%20"));

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                })
        );



        searchvendorname.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                try{
                    mAdapter.getFilter().filter(s);

                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onTextChanged(CharSequence s, int arg1, int arg2,
                                      int arg3) {
                try{
                    mAdapter.getFilter().filter(s);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        clearSearchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchvendorname.setText("");
            }
        });


    }

    private void dispatchList() {



        indicator.setVisibility(View.VISIBLE);

        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getDispatchViewList("430",new Callback<DispatchViewStoresParcelable>()

        {
            @Override
            public void success(DispatchViewStoresParcelable getccParacables, Response response) {

                if(0 != getccParacables.get$values().size()){

                    storelist.addAll(getccParacables.get$values());
                    mAdapter.notifyDataSetChanged();
                }else {
                    Toast.makeText(getActivity(),"No Item Found",Toast.LENGTH_SHORT).show();
                }
                indicator.setVisibility(View.GONE);

            }

            @Override
            public void failure(RetrofitError error) {

                indicator.setVisibility(View.GONE);
            }
        });

    }

    private void getCollectionCenterlist() {

        indicator.setVisibility(View.VISIBLE);

        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getCollectionList(new Callback<GetCollectionCenterDataParacelable>()

        {
            @Override
            public void success(GetCollectionCenterDataParacelable getccParacables, Response response) {




                collectioncenterlist.addAll(getccParacables.get$values());


                collectioncenterspinnerlist.setAdapter(new CollectionCenterListAdapter(getActivity(), collectioncenterlist));

                try{
                    if(!sharedPref.getString("collectioncenterpostion","default").equals("default")){
                        collectioncenterspinnerlist.setSelection(Integer.parseInt(sharedPref.getString("collectioncenterpostion","default")));
                        editor.putString("collectioncentercode",""+collectioncenterlist.get(0).getCC()).apply();

                    }else{

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                indicator.setVisibility(View.GONE);

            }

            @Override
            public void failure(RetrofitError error) {

                indicator.setVisibility(View.GONE);
                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }
    private class DownloadFile extends AsyncTask<String, Void, Void> {



        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "FNV");
            folder.mkdir();

            File pdfFile = new File(folder, "too.pdf");

            try{
                pdfFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);
            return null;

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getActivity(), "Download PDf successfully", Toast.LENGTH_SHORT).show();
                    viewPDF();
                }
            }, 5000);




            Log.d("Download complete", "----------");
        }
    } public void viewPDF()
    {

        indicator.setVisibility(View.GONE);
        try{
            File pdfFile = new File(Environment.getExternalStorageDirectory() + "/FNV/" + "too.pdf");  // -> filename = maven.pdf
            Uri path = FileProvider.getUriForFile(getActivity(), "fv.abrl.com.abrlav.provider", pdfFile);
            Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
            pdfIntent.setDataAndType(path, "application/pdf");
            pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            pdfIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(pdfIntent);
        }catch(ActivityNotFoundException e){
            Toast.makeText(getActivity(), "No Application available to view PDF", Toast.LENGTH_SHORT).show();
        }
    }
    }
