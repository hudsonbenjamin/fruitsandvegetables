package fv.abrl.com.abrlav.dispatch.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.dispatch.paracelable.DispatchItemDataParacelable;
import fv.abrl.com.abrlav.dispatch.paracelable.DispatchItemListParacelable;
import fv.abrl.com.abrlav.grn.fragment.GrnItemSelectionFragment;
import fv.abrl.com.abrlav.grn.paracelable.GrnItemParacelable;
import fv.abrl.com.abrlav.mpp.paracable.MppEntryDummyData;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreDataParacable;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreListParacelable;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/17/2018.
 */

public class DispatchSelectStoreAdapter extends BaseAdapter {
    private Context mContext;


    ArrayList<PickingStoreListParacelable> arrayList;
    ArrayList<DispatchItemListParacelable> innerlist = new ArrayList<DispatchItemListParacelable>();

    public DispatchSelectStoreAdapter(Context c,ArrayList<PickingStoreListParacelable> arrayList) {
        mContext = c;

        this.arrayList = arrayList;
    }



    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        final PickingStoreListParacelable retrivedata = arrayList.get(position);



        View view = convertView;


        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            view                                   =  new View(mContext);
            view                                   =  inflater.inflate(R.layout.adapter_dispatch_selectstore, null);



            final CheckBox dispatchstorename       = (CheckBox) view.findViewById(R.id.dispatchstorename);
            final TextView grnitemViewExpand       = (TextView) view.findViewById(R.id.grnitemViewExpand);
            final LinearLayout grnitemViewDetails  = (LinearLayout) view.findViewById(R.id.grnitemViewDetails);
            final ListView productItemList         = (ListView) view.findViewById(R.id.productItemList);


            dispatchstorename.setText(retrivedata.getSTORENAME());


            grnitemViewExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                if(grnitemViewDetails.getVisibility()==View.VISIBLE){
                    grnitemViewExpand.setBackground(mContext.getResources().getDrawable(R.drawable.arrow_down));
                    grnitemViewDetails.setVisibility(View.GONE);

                }else {

                        innerlist.clear();
                        pickitemlistbyStoreSelected(mContext, String.valueOf(retrivedata.getSTORE()),productItemList);
                        grnitemViewExpand.setBackground(mContext.getResources().getDrawable(R.drawable.arrow_up));
                        grnitemViewDetails.setVisibility(View.VISIBLE);


                }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

            if(retrivedata.getCheckeditem()==1){
                dispatchstorename.setChecked(true);
            }else{
                dispatchstorename.setChecked(false);
            }


            dispatchstorename.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        if (dispatchstorename.isChecked()) {
                            retrivedata.setCheckeditem(1);
                            dispatchstorename.setChecked(true);


                        } else {
                            retrivedata.setCheckeditem(0);
                            dispatchstorename.setChecked(false);
                        }
                    }

            });





        } else {
            view = (View) convertView;
        }



        return view;
    }

    private void pickitemlistbyStoreSelected(final Context ctx, String SelectedStore, final ListView productItemList) {



        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getstoreinnerproductlist(SelectedStore,new Callback<DispatchItemDataParacelable>()

        {
            @Override
            public void success(DispatchItemDataParacelable getccParacables, Response response) {


                if(0 != getccParacables.get$values().size()){

                    innerlist.addAll(getccParacables.get$values());
                    productItemList.setAdapter(new DispatchMultilevelItemListAdapter(ctx, innerlist));
                }else{
                    Toast.makeText(ctx,"No Item Found",Toast.LENGTH_SHORT).show();
                }



            }

            @Override
            public void failure(RetrofitError error) {


                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
}