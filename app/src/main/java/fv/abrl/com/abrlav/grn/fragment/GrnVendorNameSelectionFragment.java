package fv.abrl.com.abrlav.grn.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.ItemClickListener;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;

import fv.abrl.com.abrlav.common.utils.CommonFunction;
import fv.abrl.com.abrlav.common.utils.MyDividerItemDecoration;
import fv.abrl.com.abrlav.common.utils.RecyclerItemClickListener;

import fv.abrl.com.abrlav.grn.adapter.VendorNameListAdapter;
import fv.abrl.com.abrlav.grn.paracelable.$value;
import fv.abrl.com.abrlav.grn.paracelable.GetVendorDataParacable;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/14/2018.
 */

public class GrnVendorNameSelectionFragment extends Fragment  {




     @BindView(R.id.searchvendorname)
     EditText searchvendorname;

     @BindView(R.id.indicator)
     AVLoadingIndicatorView indicator;

    @BindView(R.id.selectCollectionCenter)
    TextView selectCollectionCenter;

    @BindView(R.id.vendornamelistrv)
    IndexFastScrollRecyclerView  vendornamelistrv;

    @BindView(R.id.clearSearchText)
    ImageView clearSearchText;


    private Fragment fragment;
    private Unbinder unbinder;
    private View view;


    ArrayList<$value> vendorlist  = new ArrayList<$value>();
    ArrayList<$value> tempvendorlist  = new ArrayList<$value>();


    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    VendorNameListAdapter mAdapter;

    private String tempIndexer ="";






    public GrnVendorNameSelectionFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        sharedPref              = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                  = sharedPref.edit();

        // Inflate the layout for this fragment
        view                    =  inflater.inflate(R.layout.fragment_vendornameselection, container, false);
        unbinder                =  ButterKnife.bind(this,view);


        try{
            ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                    .getColor(R.color.bottombar)));

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "Vendor Selection" + "</font>"));
            HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
            if (Build.VERSION.SDK_INT >= 21) {
                getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        mAdapter                =       new VendorNameListAdapter(getActivity(), vendorlist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        vendornamelistrv.setLayoutManager(mLayoutManager);
        vendornamelistrv.setItemAnimator(new DefaultItemAnimator());
        vendornamelistrv.setIndexTextSize(12);
        vendornamelistrv.setIndexBarColor("#ffffff");
        vendornamelistrv.setIndexBarCornerRadius(0);
        vendornamelistrv.setIndexbarMargin(0);
        vendornamelistrv.setIndexbarWidth(40);
        vendornamelistrv.setPreviewPadding(0);
        vendornamelistrv.setIndexBarTextColor("#818181");
        vendornamelistrv.setIndexBarVisibility(true);
        vendornamelistrv.setIndexbarHighLateTextColor("#ed6c09");
        vendornamelistrv.setIndexBarHighLateTextVisibility(true);
        vendornamelistrv.setAdapter(mAdapter);

        try{
            tempvendorlist.clear();
            vendorlist();
        }catch (Exception e){
            e.printStackTrace();
        }


        return view;

    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);




        try{
            if(!sharedPref.getString("collectioncentername","default").equals("default")){
                selectCollectionCenter.setText(""+sharedPref.getString("collectioncentername","default"));
            }else{
                selectCollectionCenter.setText("Collection Center Name");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        searchvendorname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        clearSearchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchvendorname.setText("");
            }
        });

        searchvendorname.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub


            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                mAdapter.getFilter().filter(arg0);
            }
        });
       // vendornamelist.setTextFilterEnabled(true);

//        vendornamelist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//               try{
//
//                   editor.putString("VendorName",""+vendorlist.get(position).getSUPNAME()).apply();
//                   editor.putString("VendorCode",""+vendorlist.get(position).getSUPPLIER()).apply();
//
//
//
//                   fragment = new GrnVendorSelectionFragment();
//                   FragmentTransaction transaction = getFragmentManager().beginTransaction();
//                   transaction.replace(R.id.homeContainer,fragment);
//                   transaction.commit();
//               }catch (Exception e){
//                   e.printStackTrace();
//               }
//
//
//
//
//            }
//        });


        vendornamelistrv.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        try{
                            if(VendorNameListAdapter.filteredarraylist.get(position).getSUPPLIER()==0){

                            }else{
                                editor.putString("VendorName",""+VendorNameListAdapter.filteredarraylist.get(position).getSUPNAME()).apply();
                                editor.putString("VendorCode",""+VendorNameListAdapter.filteredarraylist.get(position).getSUPPLIER()).apply();

                                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                                fragment = new GrnVendorSelectionFragment();
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                transaction.replace(R.id.homeContainer,fragment,"GrnVendorSelectionFragment");
                                transaction.commit();
                            }



                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                })
        );

    }
    private void vendorlist() {

        indicator.setVisibility(View.VISIBLE);

        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getVendorlist(sharedPref.getString("collectioncentercode","default"),new Callback<GetVendorDataParacable>()

        {
            @Override
            public void success(GetVendorDataParacable getccParacables, Response response) {

                try{
                    if(0 != getccParacables.get$values().size()){
                        tempvendorlist.addAll(getccParacables.get$values());

                        for(int i=0;i<tempvendorlist.size();i++){

                            if(!tempIndexer.equals(String.valueOf(tempvendorlist.get(i).getSUPNAME().charAt(0)))){

                                $value s = new $value(0,String.valueOf(tempvendorlist.get(i).getSUPNAME().charAt(0)));
                                vendorlist.add(s);
                            }
                            tempIndexer = String.valueOf(tempvendorlist.get(i).getSUPNAME().charAt(0));
                            $value s2 = new $value(tempvendorlist.get(i).getSUPPLIER(),tempvendorlist.get(i).getSUPNAME());
                            vendorlist.add(s2);
                        }

                        mAdapter.notifyDataSetChanged();


                    }else{

                        Toast.makeText(getActivity(),"No Item Found",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();

                }

                indicator.setVisibility(View.GONE);





            }

            @Override
            public void failure(RetrofitError error) {

                indicator.setVisibility(View.GONE);
                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }





}