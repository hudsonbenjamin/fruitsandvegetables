package fv.abrl.com.abrlav.view.picking.menu.fragment;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.home.activity.HomeActivity;

public class PickingViewFragment extends Fragment {


    Fragment fragment;
    private Unbinder unbinder;

    @BindView(R.id.pickingtypeSpinnerList)
    Spinner pickingtypeSpinnerList;

    @BindView(R.id.dateSpinnerList)
    Spinner dateSpinnerList;



    public PickingViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view =  inflater.inflate(R.layout.picking_viewitem, container, false);
        unbinder = ButterKnife.bind(this,view);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.colorPrimary)));
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Picking Approval");
        HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_white_resize);
        if (Build.VERSION.SDK_INT >= 21) {
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }




}