package fv.abrl.com.abrlav.mpp.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.utils.RecyclerItemClickListener;
import fv.abrl.com.abrlav.grn.adapter.CollectionCenterListAdapter;
import fv.abrl.com.abrlav.grn.paracelable.GetCollectionCenterDataParacelable;
import fv.abrl.com.abrlav.grn.paracelable.GetCollectionCenterListParcelable;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.home.fragment.HomeFragment;
import fv.abrl.com.abrlav.mpp.adapter.MPPItemListAdapter;
import fv.abrl.com.abrlav.mpp.paracable.MPPEntryDraftDataParcelable;
import fv.abrl.com.abrlav.mpp.paracable.MPPEntrySubmitDataParcelable;
import fv.abrl.com.abrlav.mpp.paracable.MPPEntrySubmitItemParcelable;
import fv.abrl.com.abrlav.mpp.paracable.MPPItemDataParcelable;
import fv.abrl.com.abrlav.mpp.paracable.MPPItemListParacelable;
import fv.abrl.com.abrlav.notification.fragment.NotificationFragment;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/31/2018.
 */

public class MppEntryCancelFragment extends Fragment implements LocationListener {





    @BindView(R.id.mppFirstSubmit)
    TextView mppFisrtScreenSubmit;

    @BindView(R.id.mppentrysearch)
    EditText mppentrysearch;

    @BindView(R.id.recycler_view)
    IndexFastScrollRecyclerView recycler_view;

    @BindView(R.id.MppEntrySelectAll)
    TextView MppEntrySelectAll;

    @BindView(R.id.MppEntryClearAll)
    TextView MppEntryClearAll;

    @BindView(R.id.spinnerlist)
    Spinner spinnerlist;

    @BindView(R.id.enablelocation)
    LinearLayout enablelocation;

    @BindView(R.id.MPPLocation)
    TextView MPPLocation;

    @BindView(R.id.collectionCenterTab)
    LinearLayout collectionCenterTab;

    @BindView(R.id.selectionTab)
    LinearLayout selectionTab;

    @BindView(R.id.collectionView)
    View collectionView;

    @BindView(R.id.selectionView)
    View selectionView;

    @BindView(R.id.indicator)
    AVLoadingIndicatorView indicator;

    @BindView(R.id.mppEntryCancel)
    TextView mppEntryCancel;


    public static TextView selectedMPPListCount;


    private Unbinder unbinder;

    private Fragment fragment;

    private MPPItemListAdapter mAdapter;

    ArrayList<GetCollectionCenterListParcelable> collectioncenterlist = new ArrayList<GetCollectionCenterListParcelable>();
    ArrayList<MPPItemListParacelable> addVeglist =  new ArrayList<MPPItemListParacelable>();


    public MppEntryCancelFragment() {
        // Required empty public constructor
    }






    public static ArrayList<MPPItemListParacelable> canceldatalist =  new ArrayList<MPPItemListParacelable>();;

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    protected LocationManager locationManager;
    RecyclerView.LayoutManager mLayoutManager;

    private BottomNavigationView bottomNavigationView;

    @SuppressLint("MissingPermission")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        sharedPref                  =   getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                      =   sharedPref.edit();






        try{
            ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                    .getColor(R.color.bottombar)));
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "MPP Entry" + "</font>"));

            HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
            if (Build.VERSION.SDK_INT >= 21) {
                getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
            }


        }catch (Exception e){
            e.printStackTrace();
        }
        View view               =   inflater.inflate(R.layout.fragment_mpp_entry, container, false);
        unbinder                =   ButterKnife.bind(this,view);

        selectedMPPListCount    =   (TextView) view.findViewById(R.id.selectedMPPListCount);





        mAdapter                =   new MPPItemListAdapter(getActivity(), MppFinalFragment.MppEntrycanceldatalist);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());

        recycler_view.setIndexTextSize(12);
        recycler_view.setIndexBarColor("#ffffff");
        recycler_view.setIndexBarCornerRadius(0);
        recycler_view.setIndexbarMargin(0);
        recycler_view.setIndexbarWidth(40);
        recycler_view.setPreviewPadding(0);
        recycler_view.setIndexBarTextColor("#818181");
        recycler_view.setIndexBarVisibility(true);
        recycler_view.setIndexbarHighLateTextColor("#ed6c09");
        recycler_view.setIndexBarHighLateTextVisibility(true);
        recycler_view.setAdapter(mAdapter);





        try{

            getCollectionCenterlist();
        }catch (Exception e){
            e.printStackTrace();
        }
//        try{
//           getLocation();
//        }catch (Exception e){
//            e.printStackTrace();
//        }




//
//        recycler_view.setOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                if (dx > 0 ) {
//
//
//                    collectionCenterTab.animate()
//                            .translationY(collectionCenterTab.getHeight())
//                            .alpha(0.0f)
//                            .setDuration(500)
//                            .setListener(new AnimatorListenerAdapter() {
//                                @Override
//                                public void onAnimationEnd(Animator animation) {
//                                    super.onAnimationEnd(animation);
//                                    collectionCenterTab.setVisibility(View.VISIBLE);
//                                }
//                            });
//
//                    selectionTab.animate()
//                            .translationY(selectionTab.getHeight())
//                            .alpha(0.0f)
//                            .setDuration(500)
//                            .setListener(new AnimatorListenerAdapter() {
//                                @Override
//                                public void onAnimationEnd(Animator animation) {
//                                    super.onAnimationEnd(animation);
//                                    selectionTab.setVisibility(View.VISIBLE);
//                                }
//                            });
//
//                    selectionView.animate()
//                            .translationY(selectionView.getHeight())
//                            .alpha(0.0f)
//                            .setDuration(500)
//                            .setListener(new AnimatorListenerAdapter() {
//                                @Override
//                                public void onAnimationEnd(Animator animation) {
//                                    super.onAnimationEnd(animation);
//                                    selectionView.setVisibility(View.VISIBLE);
//                                }
//                            });
//
//                    collectionView.animate()
//                            .translationY(collectionView.getHeight())
//                            .alpha(0.0f)
//                            .setDuration(500)
//                            .setListener(new AnimatorListenerAdapter() {
//                                @Override
//                                public void onAnimationEnd(Animator animation) {
//                                    super.onAnimationEnd(animation);
//                                    collectionView.setVisibility(View.VISIBLE);
//                                }
//                            });
//
//
//
////                    selectionTab.setVisibility(View.VISIBLE);
////                    selectionView.setVisibility(View.VISIBLE);
////                    collectionView.setVisibility(View.VISIBLE);
//
//
//                } else {
//
//
//
//                    collectionCenterTab.animate()
//                            .translationY(collectionCenterTab.getHeight())
//                            .alpha(0.0f)
//                            .setDuration(500)
//                            .setListener(new AnimatorListenerAdapter() {
//                                @Override
//                                public void onAnimationEnd(Animator animation) {
//                                    super.onAnimationEnd(animation);
//                                    collectionCenterTab.setVisibility(View.GONE);
//                                }
//                            });
//
//                    selectionTab.animate()
//                            .translationY(selectionTab.getHeight())
//                            .alpha(0.0f)
//                            .setDuration(500)
//                            .setListener(new AnimatorListenerAdapter() {
//                                @Override
//                                public void onAnimationEnd(Animator animation) {
//                                    super.onAnimationEnd(animation);
//                                    selectionTab.setVisibility(View.GONE);
//                                }
//                            });
//
//                    selectionView.animate()
//                            .translationY(selectionView.getHeight())
//                            .alpha(0.0f)
//                            .setDuration(500)
//                            .setListener(new AnimatorListenerAdapter() {
//                                @Override
//                                public void onAnimationEnd(Animator animation) {
//                                    super.onAnimationEnd(animation);
//                                    selectionView.setVisibility(View.GONE);
//                                }
//                            });
//
//                    collectionView.animate()
//                            .translationY(collectionView.getHeight())
//                            .alpha(0.0f)
//                            .setDuration(500)
//                            .setListener(new AnimatorListenerAdapter() {
//                                @Override
//                                public void onAnimationEnd(Animator animation) {
//                                    super.onAnimationEnd(animation);
//                                    collectionView.setVisibility(View.GONE);
//                                }
//                            });
//
//
////                    selectionTab.setVisibility(View.GONE);
////                    selectionView.setVisibility(View.GONE);
////                    collectionView.setVisibility(View.GONE);
//
//                }
//            }
//            });
//

        return view;
    }




    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        try{
            if(!sharedPref.getString("collectioncenterpostion","default").equals("default")){
                spinnerlist.setSelection(Integer.parseInt(sharedPref.getString("collectioncenterpostion","default")));
            }else{

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        spinnerlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                editor.putString("collectioncentername",collectioncenterlist.get(position).getCCNAME()).apply();
                editor.putString("collectioncenterpostion",""+position).apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        recycler_view.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        try{

                            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            //   Log.e("selected item name",MPPItemListAdapter.filteredarraylist.get(position).getITEMDESC());

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                })
        );


        MppEntrySelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{

                    for(int i=0;i <MppFinalFragment.MppEntrycanceldatalist.size();i++) {

                        MppFinalFragment.MppEntrycanceldatalist.get(i).setCheckboxvalue(1);

                    }
                    selectedMPPListCount.setText("Lines : "+MppFinalFragment.MppEntrycanceldatalist.size());
                    recycler_view.setAdapter(mAdapter);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
        MppEntryClearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{

                    for(int i=0;i <MppFinalFragment.MppEntrycanceldatalist.size();i++) {

                        MppFinalFragment.MppEntrycanceldatalist.get(i).setCheckboxvalue(0);
                        MppFinalFragment.MppEntrycanceldatalist.get(i).setMarketPrice("");

                    }
                    selectedMPPListCount.setText("Lines : 0");
                    recycler_view.setAdapter(mAdapter);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });


        mppEntryCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new HomeFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer,fragment);
                transaction.commit();
            }
        });









        mppFisrtScreenSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                for(int i=0;i <MppFinalFragment.MppEntrycanceldatalist.size();i++){

                    if(MppFinalFragment.MppEntrycanceldatalist.get(i).getCheckboxvalue()== 1&&!MppFinalFragment.MppEntrycanceldatalist.get(i).getMarketPrice().equals("")){
                        MPPItemListParacelable s1=new MPPItemListParacelable(
                                MppFinalFragment.MppEntrycanceldatalist.get(i).getITEM(),
                                MppFinalFragment.MppEntrycanceldatalist.get(i).getITEMDESC(),
                                MppFinalFragment.MppEntrycanceldatalist.get(i).getMarketPrice(),
                                MppFinalFragment.MppEntrycanceldatalist.get(i).getCheckboxvalue(),
                                MppFinalFragment.MppEntrycanceldatalist.get(i).getSTANDARDUOM(),
                                MppFinalFragment.MppEntrycanceldatalist.get(i).isSelected(),
                                Integer.parseInt(sharedPref.getString("collectioncentercode","0")));
                        addVeglist.add(s1);
                        canceldatalist.add(s1);
                        // }

                    }else{

                    }

                }



                if(addVeglist.size()>0){



                    showDialog(getActivity(),"Do you wish to continue?");


                }else{
                    Toast.makeText(getActivity(),"Please select at least one item",Toast.LENGTH_SHORT).show();
                }





            }
        });

        mppentrysearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.getFilter().filter(s);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });




    }



    private void DraftMPP(String result) {


        indicator.setVisibility(View.VISIBLE);
        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);



        api.draftEntryMPP(result,new Callback<MPPEntryDraftDataParcelable>()

        {
            @Override
            public void success(MPPEntryDraftDataParcelable getparcelable, Response response) {

                if(getparcelable.get$values().get(0).getMppdraftvaluesresult().equals("true")){
                    Toast.makeText(getActivity(),"Saved Successfully",Toast.LENGTH_SHORT).show();
                    indicator.setVisibility(View.GONE);
                    fragment = new MppFinalFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.homeContainer,fragment);
                    transaction.commit();

                }else{
                    indicator.setVisibility(View.GONE);
                    Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                }




            }

            @Override
            public void failure(RetrofitError error) {
                indicator.setVisibility(View.GONE);
                try{
                    Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

    }
    public void showDialog(Activity activity, String msg){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_customsubmit);

        TextView text = (TextView) dialog.findViewById(R.id.customDialogMessage);
        text.setText(msg);

        TextView customSubmit = (TextView) dialog.findViewById(R.id.customSubmit);
        ImageView closeDialog = (ImageView) dialog.findViewById(R.id.closeDialog);
        TextView customDraft = (TextView) dialog.findViewById(R.id.customDraft);


        customSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Gson gson = new GsonBuilder().create();
                JsonArray myCustomArray = gson.toJsonTree(addVeglist).getAsJsonArray();
                MppEntryFragment.dataarraylist.clear();
                submitMPP(myCustomArray.toString());
            }
        });

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        customDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                Gson gson = new GsonBuilder().create();
                JsonArray myCustomArray = gson.toJsonTree(addVeglist).getAsJsonArray();
                DraftMPP(myCustomArray.toString());
            }
        });

        dialog.show();

    }
    private void getCollectionCenterlist() {



        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getCollectionList(new Callback<GetCollectionCenterDataParacelable>()

        {
            @Override
            public void success(GetCollectionCenterDataParacelable getccParacables, Response response) {




                collectioncenterlist.addAll(getccParacables.get$values());


                spinnerlist.setAdapter(new CollectionCenterListAdapter(getActivity(), collectioncenterlist));

                try{
                    if(!sharedPref.getString("collectioncenterpostion","default").equals("default")){
                        spinnerlist.setSelection(Integer.parseInt(sharedPref.getString("collectioncenterpostion","default")));
                        editor.putString("collectioncentercode",""+collectioncenterlist.get(0).getCC()).apply();

                    }else{

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }


            }

            @Override
            public void failure(RetrofitError error) {


                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }

    void getLocation() {

        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {



        getaddress(location.getLatitude(),location.getLongitude());

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(getActivity(), "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }


    private void getaddress(double latitude, double longitude){


        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//            String city = addresses.get(0).getLocality();
            String subLocality = addresses.get(0).getSubLocality();
//            String SubAdminArea = addresses.get(0).getSubAdminArea();
//
//            String state = addresses.get(0).getAdminArea();
//            String country = addresses.get(0).getCountryName();
//            String knownName = addresses.get(0).getFeatureName();


            MPPLocation.setText(""+subLocality);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    private void submitMPP(String result) {

        indicator.setVisibility(View.VISIBLE);

        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        Log.d("hello",""+result);

        api.submitEntryMPP(result,new Callback<MPPEntrySubmitDataParcelable>()

        {
            @Override
            public void success(MPPEntrySubmitDataParcelable getparcelable, Response response) {


                if(0 != getparcelable.get$values().size()){

                    MppFinalFragment.MppEntrycanceldatalist = canceldatalist ; //cancel data transfer entry to final

                    MppEntryFragment.dataarraylist.addAll(getparcelable.get$values());


                    indicator.setVisibility(View.GONE);
                    fragment = new MppFinalFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.homeContainer,fragment);
                    transaction.commit();
                }else{
                    indicator.setVisibility(View.GONE);
                    Toast.makeText(getActivity(),"No Item Found",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                indicator.setVisibility(View.GONE);
                try{
                    Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

    }


}
