package fv.abrl.com.abrlav.picking.route.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.utils.RecyclerItemClickListener;
import fv.abrl.com.abrlav.grn.adapter.CollectionCenterListAdapter;
import fv.abrl.com.abrlav.grn.adapter.VendorNameListAdapter;
import fv.abrl.com.abrlav.grn.fragment.GrnVendorSelectionFragment;
import fv.abrl.com.abrlav.grn.paracelable.$value;
import fv.abrl.com.abrlav.grn.paracelable.GetCollectionCenterDataParacelable;
import fv.abrl.com.abrlav.grn.paracelable.GetCollectionCenterListParcelable;
import fv.abrl.com.abrlav.grn.paracelable.GetVendorDataParacable;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.picking.route.adapter.StoreNameListAdapter;
import fv.abrl.com.abrlav.picking.route.parcelable.PickingRouteDataParcelable;
import fv.abrl.com.abrlav.picking.route.parcelable.PickingRouteListParcelable;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/14/2018.
 */

public class PickByRouteSelectionFragment extends Fragment {




    private Unbinder unbinder;
    Fragment fragment;

    @BindView(R.id.routeList)
    IndexFastScrollRecyclerView recyc_routeList;
    StoreNameListAdapter mAdapter;

    @BindView(R.id.pickingRoutespinnerlist)
    Spinner pickingRoutespinnerlist;

    @BindView(R.id.RouteSearch)
    EditText RouteSearch;

    @BindView(R.id.clearRouteTxt)
    ImageView clearSearchText;

    @BindView(R.id.indicator)
    AVLoadingIndicatorView indicator;

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    public PickByRouteSelectionFragment() {
        // Required empty public constructor
    }

    ArrayList<PickingRouteListParcelable> routeArrayList = new ArrayList<PickingRouteListParcelable>();
    ArrayList<GetCollectionCenterListParcelable> collectioncenterlist = new ArrayList<GetCollectionCenterListParcelable>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        sharedPref    = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor        = sharedPref.edit();

        try{
        ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.bottombar)));
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "Pick By Route" + "</font>"));

        HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
        if (Build.VERSION.SDK_INT >= 21) {
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
        }
            }
        catch (Exception e){
                e.printStackTrace();
            }

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pickbyroute, container, false);
        unbinder = ButterKnife.bind(this, view);

        mAdapter                =       new StoreNameListAdapter(getActivity(), routeArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyc_routeList.setLayoutManager(mLayoutManager);
        recyc_routeList.setItemAnimator(new DefaultItemAnimator());

        recyc_routeList.setIndexTextSize(12);
        recyc_routeList.setIndexBarColor("#ffffff");
        recyc_routeList.setIndexBarCornerRadius(0);
        recyc_routeList.setIndexbarMargin(0);
        recyc_routeList.setIndexbarWidth(40);
        recyc_routeList.setPreviewPadding(0);
        recyc_routeList.setIndexBarTextColor("#818181");
        recyc_routeList.setIndexBarVisibility(true);
        recyc_routeList.setIndexbarHighLateTextColor("#ed6c09");
        recyc_routeList.setIndexBarHighLateTextVisibility(true);
        recyc_routeList.setAdapter(mAdapter);

        try{
            getRouteList();
        }catch (Exception e){
            e.printStackTrace();
        }

        try{
            getCollectionCenterlist();
        }catch (Exception e){
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);




        recyc_routeList.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        try{

                                editor.putString("RouteName",""+StoreNameListAdapter.filteredarraylist.get(position).getROUTENAME()).apply();
                                editor.putString("RouteCode",""+StoreNameListAdapter.filteredarraylist.get(position).getROUTEID()).apply();

                                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                                fragment = new PickByRouteItemSelectionFragment();
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                transaction.replace(R.id.homeContainer,fragment);
                                transaction.commit();




                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                })
        );

        RouteSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try{
                    mAdapter.getFilter().filter(s);

                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                try{
                    mAdapter.getFilter().filter(s);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        clearSearchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RouteSearch.setText("");
            }
        });

        pickingRoutespinnerlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                editor.putString("collectioncentername",collectioncenterlist.get(position).getCCNAME()).apply();
                editor.putString("collectioncenterpostion",""+position).apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

    }

    private void getRouteList() {


        indicator.setVisibility(View.VISIBLE);
        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getRouteList(sharedPref.getString("collectioncentercode","default"),new Callback<PickingRouteDataParcelable>()

        {
            @Override
            public void success(PickingRouteDataParcelable getccParacables, Response response) {

                try{
                    if(0 != getccParacables.get$values().size()){
                        routeArrayList.addAll(getccParacables.get$values());
                        mAdapter.notifyDataSetChanged();



                    }else{

                        Toast.makeText(getActivity(),"No Item Found",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();

                }
                indicator.setVisibility(View.GONE);






            }

            @Override
            public void failure(RetrofitError error) {

                indicator.setVisibility(View.GONE);
                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }

    private void getCollectionCenterlist() {



        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getCollectionList(new Callback<GetCollectionCenterDataParacelable>()

        {
            @Override
            public void success(GetCollectionCenterDataParacelable getccParacables, Response response) {

                collectioncenterlist.addAll(getccParacables.get$values());


                pickingRoutespinnerlist.setAdapter(new CollectionCenterListAdapter(getActivity(), collectioncenterlist));

                try{
                    if(!sharedPref.getString("collectioncenterpostion","default").equals("default")){
                        pickingRoutespinnerlist.setSelection(Integer.parseInt(sharedPref.getString("collectioncenterpostion","default")));
                        editor.putString("collectioncentercode",""+collectioncenterlist.get(0).getCC()).apply();}else{

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }


            }

            @Override
            public void failure(RetrofitError error) {


                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }
}