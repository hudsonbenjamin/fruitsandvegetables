package fv.abrl.com.abrlav.picking.item.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.home.fragment.HomeFragment;

/**
 * Created by Hudson on 7/14/2018.
 */

public class PickByItemCompleteFragment extends Fragment {


    @BindView(R.id.pickbyitemcompletesubmit)
    TextView pickbyitemcompletesubmit;


    private Unbinder unbinder;
    Fragment fragment;

    public PickByItemCompleteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        try{
        ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.bottombar)));
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "Pick By Item" + "</font>"));

        HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
        if (Build.VERSION.SDK_INT >= 21) {
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
                }
            }
        catch (Exception e){
                e.printStackTrace();
            }

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pickbyitemcomplete, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pickbyitemcompletesubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new HomeFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer,fragment);
                transaction.commit();
            }
        });


    }
}