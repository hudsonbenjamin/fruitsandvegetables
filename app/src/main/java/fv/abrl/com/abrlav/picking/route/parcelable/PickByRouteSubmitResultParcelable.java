
package fv.abrl.com.abrlav.picking.route.parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickByRouteSubmitResultParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<PickByRouteResult> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<PickByRouteResult> get$values() {
        return $values;
    }

    public void set$values(List<PickByRouteResult> $values) {
        this.$values = $values;
    }

}
