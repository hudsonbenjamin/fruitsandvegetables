
package fv.abrl.com.abrlav.dispatch.paracelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DispatchGenerateToParacelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("Storeoutput")
    @Expose
    private String storeoutput;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getStoreoutput() {
        return storeoutput;
    }

    public void setStoreoutput(String storeoutput) {
        this.storeoutput = storeoutput;
    }

}
