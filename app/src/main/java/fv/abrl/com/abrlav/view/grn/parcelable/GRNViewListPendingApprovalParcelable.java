
package fv.abrl.com.abrlav.view.grn.parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GRNViewListPendingApprovalParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("GRN_Invoice_Number")
    @Expose
    private String gRNInvoiceNumber;
    @SerializedName("ITEM")
    @Expose
    private Integer iTEM;
    @SerializedName("ITEM_DESC")
    @Expose
    private String iTEMDESC;
    @SerializedName("GRN_Quantity")
    @Expose
    private Integer gRNQuantity;
    @SerializedName("Vendor_Code")
    @Expose
    private Integer vendorCode;
    @SerializedName("SUP_NAME")
    @Expose
    private String sUPNAME;
    @SerializedName("SUPPLIER")
    @Expose
    private Integer sUPPLIER;
    @SerializedName("CC_Code")
    @Expose
    private Integer cCCode;
    @SerializedName("GRN_Cost")
    @Expose
    private Integer gRNCost;
    @SerializedName("MPP")
    @Expose
    private Integer mPP;
    @SerializedName("Market_Price")
    @Expose
    private Integer marketPrice;
    @SerializedName("STANDARD_UOM")
    @Expose
    private String sTANDARDUOM;

    @SerializedName("status")
    @Expose
    private String grnStatus;

    @SerializedName("GRN_Number")
    @Expose
    private int grnnumber;

    @SerializedName("Approverstatus")
    @Expose
    private String Approverstatus;



    public String getApproverstatus() {
        return Approverstatus;
    }

    public void setApproverstatus(String approverstatus) {
        Approverstatus = approverstatus;
    }




    public int getGrnnumber() {
        return grnnumber;
    }

    public void setGrnnumber(int grnnumber) {
        this.grnnumber = grnnumber;
    }




    public GRNViewListPendingApprovalParcelable(String itemdesc, Integer item, Integer ccCode, int passgrn_number, String Approverstatus) {

        this.iTEMDESC = itemdesc;
        this.iTEM = item;
        this.cCCode = ccCode;
        this.grnnumber = passgrn_number;
        this.Approverstatus = Approverstatus;


    }


    public String getGrnStatus() {
        return grnStatus;
    }

    public void setGrnStatus(String grnStatus) {
        this.grnStatus = grnStatus;
    }




    public int getItemchecked() {
        return itemchecked;
    }

    public void setItemchecked(int itemchecked) {
        this.itemchecked = itemchecked;
    }

    private int itemchecked = 0;


    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getGRNInvoiceNumber() {
        return gRNInvoiceNumber;
    }

    public void setGRNInvoiceNumber(String gRNInvoiceNumber) {
        this.gRNInvoiceNumber = gRNInvoiceNumber;
    }

    public Integer getITEM() {
        return iTEM;
    }

    public void setITEM(Integer iTEM) {
        this.iTEM = iTEM;
    }

    public String getITEMDESC() {
        return iTEMDESC;
    }

    public void setITEMDESC(String iTEMDESC) {
        this.iTEMDESC = iTEMDESC;
    }

    public Integer getGRNQuantity() {
        return gRNQuantity;
    }

    public void setGRNQuantity(Integer gRNQuantity) {
        this.gRNQuantity = gRNQuantity;
    }

    public Integer getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(Integer vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getSUPNAME() {
        return sUPNAME;
    }

    public void setSUPNAME(String sUPNAME) {
        this.sUPNAME = sUPNAME;
    }

    public Integer getSUPPLIER() {
        return sUPPLIER;
    }

    public void setSUPPLIER(Integer sUPPLIER) {
        this.sUPPLIER = sUPPLIER;
    }

    public Integer getCCCode() {
        return cCCode;
    }

    public void setCCCode(Integer cCCode) {
        this.cCCode = cCCode;
    }

    public Integer getGRNCost() {
        return gRNCost;
    }

    public void setGRNCost(Integer gRNCost) {
        this.gRNCost = gRNCost;
    }

    public Integer getMPP() {
        return mPP;
    }

    public void setMPP(Integer mPP) {
        this.mPP = mPP;
    }

    public Integer getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Integer marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getSTANDARDUOM() {
        return sTANDARDUOM;
    }

    public void setSTANDARDUOM(String sTANDARDUOM) {
        this.sTANDARDUOM = sTANDARDUOM;
    }



}
