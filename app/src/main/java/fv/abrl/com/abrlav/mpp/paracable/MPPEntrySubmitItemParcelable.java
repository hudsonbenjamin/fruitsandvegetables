
package fv.abrl.com.abrlav.mpp.paracable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MPPEntrySubmitItemParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("CC_Code")
    @Expose
    private int cCCode;
    @SerializedName("ITEM")
    @Expose
    private int iTEM;
    @SerializedName("ITEM_DESC")
    @Expose
    private String iTEMDESC;
    @SerializedName("STANDARD_UOM")
    @Expose
    private String sTANDARDUOM;
    @SerializedName("role")
    @Expose
    private int role;
    @SerializedName("MPP")
    @Expose
    private float mPP;
    @SerializedName("Marketprice")
    @Expose
    private float marketprice;
    @SerializedName("FinalMPP")
    @Expose
    private float finalMPP;
    private int checkboxvalue = 0;

    @SerializedName("MPPID")
    @Expose
    private int mPPID;

    public int getMPPID() {
        return mPPID;
    }

    public void setMPPID(int mPPID) {
        this.mPPID = mPPID;
    }

    public MPPEntrySubmitItemParcelable(int item, String itemdesc, float marketprice, int checkboxvalue, float finalMPP, float mpp,int ccode,int mppid) {
    this.iTEM = item;
    this.iTEMDESC = itemdesc;
    this.marketprice = marketprice;
    this.checkboxvalue =  checkboxvalue;
    this.finalMPP = finalMPP;
    this.mPP = mpp;
    this.cCCode = ccode;
    this.mPPID = mppid;

    }

    public int getCheckboxvalue() {
        return checkboxvalue;
    }

    public void setCheckboxvalue(int checkboxvalue) {
        this.checkboxvalue = checkboxvalue;
    }



    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getCCCode() {
        return cCCode;
    }

    public void setCCCode(int cCCode) {
        this.cCCode = cCCode;
    }

    public int getITEM() {
        return iTEM;
    }

    public void setITEM(int iTEM) {
        this.iTEM = iTEM;
    }

    public String getITEMDESC() {
        return iTEMDESC;
    }

    public void setITEMDESC(String iTEMDESC) {
        this.iTEMDESC = iTEMDESC;
    }

    public String getSTANDARDUOM() {
        return sTANDARDUOM;
    }

    public void setSTANDARDUOM(String sTANDARDUOM) {
        this.sTANDARDUOM = sTANDARDUOM;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public float getMPP() {
        return mPP;
    }

    public void setMPP(float mPP) {
        this.mPP = mPP;
    }

    public float getMarketprice() {
        return marketprice;
    }

    public void setMarketprice(float marketprice) {
        this.marketprice = marketprice;
    }

    public float getFinalMPP() {
        return finalMPP;
    }

    public void setFinalMPP(float finalMPP) {
        this.finalMPP = finalMPP;
    }

}
