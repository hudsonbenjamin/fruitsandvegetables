package fv.abrl.com.abrlav.grn.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.grn.fragment.GrnItemSelectionFragment;
import fv.abrl.com.abrlav.grn.paracelable.GrnItemParacelable;
import fv.abrl.com.abrlav.mpp.paracable.MppEntryDummyData;

/**
 * Created by Hudson on 7/13/2018.
 */

public class GrnSubmissionAdapter  extends BaseAdapter {
    private Context mContext;
    ArrayList<GrnItemParacelable> web;



    public GrnSubmissionAdapter(Context c, ArrayList<GrnItemParacelable> web ) {
        mContext = c;
        this.web = web;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return web.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        final GrnItemParacelable retrivedata = web.get(position);
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);
            grid = inflater.inflate(R.layout.adapter_grn_submission, null);

            final TextView finalentryCheckBox       = (TextView) grid.findViewById(R.id.grnitemselectionCB);
            final TextView grnitemViewExpand        = (TextView)  grid.findViewById(R.id.grnitemViewExpand);
            final TextView itemCode                 = (TextView)  grid.findViewById(R.id.itemCode);
            final TextView Mpp                      = (TextView)  grid.findViewById(R.id.grnItemSelectionMpp);
            final LinearLayout grnitemViewDetails   = (LinearLayout) grid.findViewById(R.id.grnitemViewDetails);
            final EditText grnquantity              = (EditText) grid.findViewById(R.id.grnquantity);
            final EditText grnCost                  = (EditText) grid.findViewById(R.id.grnCost);

            // final EditText finalMpp = (EditText) grid.findViewById(R.id.finalMpp);
            grnitemViewExpand.setVisibility(View.INVISIBLE);
            grnitemViewDetails.setVisibility(View.VISIBLE);



            finalentryCheckBox.setText(""+retrivedata.getITEMDESC()+" ("+retrivedata.getSTANDARDUOM()+")");
            Mpp.setText(""+retrivedata.getMPP());
            grnquantity.setText(""+retrivedata.getGrnQuantity());
            grnCost.setText(""+retrivedata.getGrnCost());
            itemCode.setText("Item Code : "+retrivedata.getITEM());


            grnquantity.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    // TODO Auto-generated method stub
                }

                @Override
                public void afterTextChanged(Editable s) {
                    try{
                        if(s.length()>0&&grnCost.getText().toString().trim().length() > 0){
                            retrivedata.setCheckedItem(1);
                            retrivedata.setGrnCost(Integer.valueOf(""+grnCost.getText().toString()));
                            retrivedata.setGrnQuantity(Integer.valueOf(""+grnquantity.getText().toString()));
                            //finalentryCheckBox.setChecked(true);

                        }else{
                            retrivedata.setCheckedItem(0);
                          //  finalentryCheckBox.setChecked(false);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }



                }
            });

            grnCost.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    // TODO Auto-generated method stub
                }

                @Override
                public void afterTextChanged(Editable s) {
                    try{
                        if(s.length()>0&&grnquantity.getText().toString().trim().length() > 0){
                            retrivedata.setCheckedItem(1);
                            retrivedata.setGrnCost(Integer.valueOf(""+grnCost.getText().toString()));
                            retrivedata.setGrnQuantity(Integer.valueOf(""+grnquantity.getText().toString()));
                           // finalentryCheckBox.setChecked(true);

                        }else{
                            retrivedata.setCheckedItem(0);
                          //  finalentryCheckBox.setChecked(false);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }


                }
            });

        } else {
            grid = (View) convertView;
        }



        return grid;
    }
    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
}