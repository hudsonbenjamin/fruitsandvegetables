package fv.abrl.com.abrlav.grn.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.grn.fragment.GrnItemSelectionFragment;
import fv.abrl.com.abrlav.grn.paracelable.$value;
import fv.abrl.com.abrlav.grn.paracelable.GrnItemParacelable;
import fv.abrl.com.abrlav.mpp.fragment.MppEntryFragment;
import fv.abrl.com.abrlav.mpp.paracable.MPPItemListParacelable;

/**
 * Created by Hudson on 7/26/2018.
 */

public class GrnItemSelectionAdapterRv extends RecyclerView.Adapter<GrnItemSelectionAdapterRv.MyViewHolder>
        implements Filterable, SectionIndexer {
    private Context mContext;
    ArrayList<GrnItemParacelable> arrayList;
    ArrayList<GrnItemParacelable> filteredarraylist;
    private ArrayList<Integer> mSectionPositions;
    private ArrayList<String> checkboxcount = new ArrayList<String>();
    private GrnItemParacelable getItem(int position) {
        return filteredarraylist.get(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CheckBox finalentryCheckBox;
        TextView grnitemViewExpand;
        TextView Mpp;
        TextView itemCode;
        LinearLayout grnitemViewDetails;
        EditText grnquantity;
        EditText grnCost;
        public MyViewHolder(View view) {
            super(view);

            finalentryCheckBox      = (CheckBox) view.findViewById(R.id.grnitemselectionCB);
            grnitemViewExpand       = (TextView)  view.findViewById(R.id.grnitemViewExpand);
            Mpp                     = (TextView)  view.findViewById(R.id.grnItemSelectionMpp);
            itemCode                = (TextView)  view.findViewById(R.id.itemCode);
            grnitemViewDetails      = (LinearLayout) view.findViewById(R.id.grnitemViewDetails);
            grnquantity             = (EditText) view.findViewById(R.id.grnquantity);
            grnCost                 = (EditText) view.findViewById(R.id.grnCost);


        }
    }
    public GrnItemSelectionAdapterRv(Context c, ArrayList<GrnItemParacelable> arrayList ) {
        mContext = c;
        this.arrayList = arrayList;
        this.filteredarraylist = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_grn_item_selection, parent, false);



        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.finalentryCheckBox.setText("" + filteredarraylist.get(position).getITEMDESC() + " (" + filteredarraylist.get(position).getSTANDARDUOM() + ")");
        holder.Mpp.setText("" + filteredarraylist.get(position).getMPP());
        holder.itemCode.setText("Item Code : " + filteredarraylist.get(position).getITEM());

        if(filteredarraylist.get(position).getGrnCost()==0){
            holder.grnCost.setText("");
        }else{
            holder.grnCost.setText(""+filteredarraylist.get(position).getGrnCost());
        }
        if(filteredarraylist.get(position).getGrnQuantity()==0){
            holder.grnquantity.setText("");
        }else{
            holder.grnquantity.setText(""+filteredarraylist.get(position).getGrnQuantity());
        }

        try{
            if(filteredarraylist.get(position).getSTANDARDUOM().equals("EA")){
                holder.grnquantity.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        holder.grnitemViewExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (holder.grnitemViewDetails.getVisibility() == View.VISIBLE) {
                    holder.grnitemViewExpand.setBackground(mContext.getResources().getDrawable(R.drawable.arrow_down));
                    holder.grnitemViewDetails.setVisibility(View.GONE);
                } else {
                    holder.grnitemViewExpand.setBackground(mContext.getResources().getDrawable(R.drawable.arrow_up));
                    holder.grnitemViewDetails.setVisibility(View.VISIBLE);
                }


            }
        });

        try{
            if(filteredarraylist.get(position).getCheckedItem()==1&&(!holder.grnquantity.getText().toString().equals("") && !holder.grnCost.getText().equals(""))){
                //if(filteredarraylist.get(position).getCheckboxvalue()==1){
                holder.finalentryCheckBox.setChecked(true);

            }else {

                holder.finalentryCheckBox.setChecked(false);
            }
        }catch (Exception e){

            holder.finalentryCheckBox.setChecked(false);
        }

//        if(filteredarraylist.get(position).getMPP()==0){
//            holder.grnquantity.setEnabled(true);
//            holder.grnCost.setEnabled(true);
//
//            Toast.makeText(mContext,"MPP is 0",Toast.LENGTH_SHORT).show();
//
//        }else{
//            holder.grnquantity.setEnabled(false);
//            holder.grnCost.setEnabled(false);
//        }




        try{
            if(filteredarraylist.get(position).getSTANDARDUOM().equals("EA")){
                holder.grnquantity.setKeyListener(DigitsKeyListener.getInstance(true,false));

            }else{
                holder.grnquantity.setKeyListener(DigitsKeyListener.getInstance(true,true));
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        holder.grnquantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {




                try {

                        String x1 = s.toString();
                        if(x1.startsWith("-"))
                        {
                            holder.grnquantity.setText("");

                        }

                        if(x1.startsWith("0"))
                        {
                            holder.grnquantity.setText("");
                            Toast.makeText(mContext,"0 is not allowed in starting",Toast.LENGTH_SHORT).show();
                        }




                    if (s.length() > 0 && holder.grnCost.getText().length() > 0) {

                        filteredarraylist.get(position).setCheckedItem(1);
                        filteredarraylist.get(position).setGrnCost(Double.parseDouble("" +holder.grnCost.getText()));
                        filteredarraylist.get(position).setGrnQuantity(Double.parseDouble("" + holder.grnquantity.getText().toString()));
                        holder.finalentryCheckBox.setChecked(true);

                    } else {

                        filteredarraylist.get(position).setCheckedItem(0);
                        filteredarraylist.get(position).setGrnQuantity(0);
                        holder.finalentryCheckBox.setChecked(false);
                    }  checkboxvaluereturn();

                } catch (Exception e) {
                    e.printStackTrace();
                    checkboxvaluereturn();
                    filteredarraylist.get(position).setCheckedItem(0);
                    filteredarraylist.get(position).setGrnQuantity(0);
                    holder.finalentryCheckBox.setChecked(false);
                }


            }
        });










        holder.grnCost.setKeyListener(DigitsKeyListener.getInstance(true,true));
        holder.grnCost.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String x1 = s.toString();
                if(x1.startsWith("-"))
                {
                    holder.grnCost.setText("");

                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (s.length() > 0 && holder.grnquantity.getText().length() > 0) {
                        holder.finalentryCheckBox.setChecked(true);

                        filteredarraylist.get(position).setCheckedItem(1);
                        filteredarraylist.get(position).setGrnCost(Double.parseDouble("" + holder.grnCost.getText().toString()));
                        filteredarraylist.get(position).setGrnQuantity(Double.parseDouble("" + holder.grnquantity.getText().toString()));


                    } else {
                        filteredarraylist.get(position).setCheckedItem(0);
                        holder.finalentryCheckBox.setChecked(false);
                        filteredarraylist.get(position).setGrnCost(0);

                    }
                    checkboxvaluereturn();
                } catch (Exception e) {
                    e.printStackTrace();
                    filteredarraylist.get(position).setCheckedItem(0);
                    holder.finalentryCheckBox.setChecked(false);
                    filteredarraylist.get(position).setGrnCost(0);
                }


            }
        });


        holder.finalentryCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!holder.grnquantity.getText().toString().isEmpty() && !holder.grnCost.getText().toString().isEmpty()) {

                    if (holder.finalentryCheckBox.isChecked()) {

                        filteredarraylist.get(position).setCheckedItem(1);
                        filteredarraylist.get(position).setGrnQuantity(Double.parseDouble("" + holder.grnquantity.getText().toString()));
                        filteredarraylist.get(position).setGrnCost(Double.parseDouble("" + holder.grnCost.getText().toString()));
                        // GrnItemParacelable s1 = new GrnItemParacelable(finalentryCheckBox.getText().toString(),Integer.valueOf(""+grnCost.getText().toString()),Integer.valueOf("" + grnquantity.getText().toString()), Integer.valueOf(""+Mpp.getText().toString()));
                        //GrnItemSelectionFragment.grnarrayselectedlist.add(s1);

                    } else {

                        filteredarraylist.get(position).setCheckedItem(0);
                        holder.finalentryCheckBox.setChecked(false);
                        // GrnItemSelectionFragment.grnarrayselectedlist.remove(retrivedata);

                    }
                    checkboxvaluereturn();
                } else {
                    checkboxvaluereturn();
                    filteredarraylist.get(position).setCheckedItem(0);
                    holder.finalentryCheckBox.setChecked(false);
                    Toast.makeText(mContext, "Please Enter GRN Qunantity and Cost", Toast.LENGTH_SHORT).show();

                }


            }

        });
    }
            @Override
            public int getItemCount() {
                return filteredarraylist.size();
            }

            @Override
            public Filter getFilter() {
                return new Filter() {
                    @Override
                    protected FilterResults performFiltering(CharSequence charSequence) {
                        String charString = charSequence.toString();
                        if (charString.isEmpty()) {
                            filteredarraylist = arrayList;
                        } else {
                            ArrayList<GrnItemParacelable> filteredList = new ArrayList<>();
                            for (GrnItemParacelable row : arrayList) {
                                if (row.getITEMDESC().toLowerCase().contains(charString.toLowerCase())) {
                                    filteredList.add(row);
                                }
                            }

                            filteredarraylist = filteredList;
                        }

                        FilterResults filterResults = new FilterResults();
                        filterResults.values = filteredarraylist;
                        return filterResults;
                    }

                    @Override
                    protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                        filteredarraylist = (ArrayList<GrnItemParacelable>) filterResults.values;

                        notifyDataSetChanged();
                    }
                };
            }

            @Override
            public int getItemViewType(int position) {
                return position;
            }


            @Override
            public int getSectionForPosition(int position) {
                return 0;
            }

            @Override
            public Object[] getSections() {
                List<String> sections = new ArrayList<>(26);
                mSectionPositions = new ArrayList<>(26);
                for (int i = 0, size = filteredarraylist.size(); i < size; i++) {
                    String section = String.valueOf(filteredarraylist.get(i).getITEMDESC().charAt(0)).toUpperCase();
                    if (!sections.contains(section)) {
                        sections.add(section);
                        mSectionPositions.add(i);
                    }
                }
                return sections.toArray(new String[0]);
            }

            @Override
            public int getPositionForSection(int sectionIndex) {
                return mSectionPositions.get(sectionIndex);
            }

    public long getItemId(int position) {
        return position;
    }


    private void checkboxvaluereturn(){
        try{
            checkboxcount.clear();
            for(int i=0;i<filteredarraylist.size();i++){
                if(filteredarraylist.get(i).getCheckedItem()==1){

                    checkboxcount.add(filteredarraylist.get(i).getITEMDESC());
                }


            }
            GrnItemSelectionFragment.grntotalItemCount.setText(": "+checkboxcount.size());
        }catch (Exception e){
            e.printStackTrace();
        }

    }



       }
