package fv.abrl.com.abrlav.picking.store.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.utils.CommonFunction;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.home.fragment.HomeFragment;
import fv.abrl.com.abrlav.picking.fragment.PickingMenuFragment;
import fv.abrl.com.abrlav.picking.store.adapter.PickbyStoreItemListAdapterRv;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreItemDataParacable;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreItemListParacelable;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/14/2018.
 */

public class PickbyStoreFragmentSecond extends Fragment {


    @BindView(R.id.pickbystoresecondSubmit)
    TextView pickbyItemSubmit;

    @BindView(R.id.storeNameTitle)
    TextView storeNameTitle;

    @BindView(R.id.productlistlv)
    IndexFastScrollRecyclerView productlistlv;

    @BindView(R.id.collectioncentername)
    TextView collectioncentername;

    private Unbinder unbinder;
    Fragment fragment;

    @BindView(R.id.clearSearchText)
   ImageView clearSearchText;

    @BindView(R.id.searchItems)
    EditText searchItems;

    @BindView(R.id.pickbystoreSecondCancel)
    TextView pickbystoreSecondCancel;

    @BindView(R.id.indicator)
    AVLoadingIndicatorView indicator;

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    public PickbyStoreFragmentSecond() {
        // Required empty public constructor
    }

    PickbyStoreItemListAdapterRv mAdapter;

    ArrayList<PickingStoreItemListParacelable> itemlist = new ArrayList<PickingStoreItemListParacelable>();

    public static ArrayList<PickingStoreItemListParacelable> selectedItem = new ArrayList<PickingStoreItemListParacelable>();
    public static ArrayList<PickingStoreItemListParacelable> cancelsenddata = new ArrayList<PickingStoreItemListParacelable>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        sharedPref              = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                  = sharedPref.edit();




            try{
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                            .getColor(R.color.bottombar)));
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "Pick By Store" + "</font>"));

                    HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
                    if (Build.VERSION.SDK_INT >= 21) {
                        getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
                    }
                }
            catch (Exception e){
                    e.printStackTrace();
                }

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pickbystorefragmentsecond, container, false);
        unbinder = ButterKnife.bind(this, view);

        try{
            if (!sharedPref.getString("Storename","default").equals("default")){
                storeNameTitle.setText(""+sharedPref.getString("Storename","default")+" - "+sharedPref.getString("storecode","default"));
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        mAdapter                =       new PickbyStoreItemListAdapterRv(getActivity(), itemlist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        productlistlv.setLayoutManager(mLayoutManager);
        productlistlv.setItemAnimator(new DefaultItemAnimator());
        productlistlv.setItemViewCacheSize(itemlist.size());
        productlistlv.setIndexTextSize(12);
        productlistlv.setIndexBarColor("#ffffff");
        productlistlv.setIndexBarCornerRadius(0);
        productlistlv.setIndexbarMargin(0);
        productlistlv.setIndexbarWidth(40);
        productlistlv.setPreviewPadding(0);
        productlistlv.setIndexBarTextColor("#818181");
        productlistlv.setIndexBarVisibility(true);
        productlistlv.setIndexbarHighLateTextColor("#ed6c09");
        productlistlv.setIndexBarHighLateTextVisibility(true);
        productlistlv.setAdapter(mAdapter);


        try{
            pickitemListbyStore(sharedPref.getString("storecode","default"));

        }catch (Exception e){
            e.printStackTrace();
        }

        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pickbystoreSecondCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new PickingMenuFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer,fragment);
                transaction.commit();
            }
        });

        pickbyItemSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectedItem.clear();
                cancelsenddata.clear();
                try{
                    for(int i =0;i<itemlist.size();i++){
                        if(itemlist.get(i).getCheckitem()==1){

                            PickingStoreItemListParacelable s1 = new PickingStoreItemListParacelable(
                                    itemlist.get(i).get$id(),
                                    itemlist.get(i).getITEMDESC(),
                                    itemlist.get(i).getITEM(),
                                    itemlist.get(i).getQuantity(),
                                    1,
                                    itemlist.get(i).getSTANDARDUOM(),
                                    sharedPref.getString("storecode","default"),
                                    sharedPref.getString("Storename","default"),itemlist.get(i).getSOH(),
                                    Integer.parseInt(sharedPref.getString("collectioncentercode","default")));
                            selectedItem.add(s1);
                            cancelsenddata.add(s1);

                        }else{
                            PickingStoreItemListParacelable s1 = new PickingStoreItemListParacelable(
                                    itemlist.get(i).get$id(),
                                    itemlist.get(i).getITEMDESC(),
                                    itemlist.get(i).getITEM(),
                                    itemlist.get(i).getQuantity(),
                                    1,
                                    itemlist.get(i).getSTANDARDUOM(),
                                    sharedPref.getString("storecode","default"),
                                    sharedPref.getString("Storename","default"),
                                    itemlist.get(i).getSOH(),
                                    Integer.parseInt(sharedPref.getString("collectioncentercode","default")));
                            cancelsenddata.add(s1);
                        }
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                try{
                    if(selectedItem.size()>0) {


                        if(CommonFunction.isNetworkAvailable(getActivity())) {
                            showDialog(getActivity(),"Do you wish to continue?");
                        }else{
                            Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                        }


                    }else{
                        Toast.makeText(getActivity(),"Please select atleast one item",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        try{
            if(!sharedPref.getString("collectioncentername","default").equals("default")){
                collectioncentername.setText(""+sharedPref.getString("collectioncentername","default"));
            }else{
                collectioncentername.setText("Collection Center Name");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        clearSearchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchItems.setText("");
            }
        });

        searchItems.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                mAdapter.getFilter().filter(arg0);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {


                mAdapter.getFilter().filter(arg0);

            }
        });

    }

    private void pickitemListbyStore(String storecode) {

        indicator.setVisibility(View.VISIBLE);

        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getproductlist(storecode,new Callback<PickingStoreItemDataParacable>()

        {
            @Override
            public void success(PickingStoreItemDataParacable getccParacables, Response response) {

                try{
                    if(0 != getccParacables.get$values().size()) {

                        itemlist.addAll(getccParacables.get$values());
                        mAdapter.notifyDataSetChanged();

                    }else{
                        Toast.makeText(getActivity(),"Item not found",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                indicator.setVisibility(View.GONE);

            }

            @Override
            public void failure(RetrofitError error) {

                try{
                    indicator.setVisibility(View.GONE);
                    Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                }


                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }

    public void showDialog(Activity activity, String msg){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_customsubmit);

        TextView text = (TextView) dialog.findViewById(R.id.customDialogMessage);
        text.setText(msg);

        TextView customSubmit = (TextView) dialog.findViewById(R.id.customSubmit);
        ImageView closeDialog = (ImageView) dialog.findViewById(R.id.closeDialog);
        TextView customDraft = (TextView) dialog.findViewById(R.id.customDraft);

        customDraft.setText("Cancel");

        customSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                fragment = new PickByStoreFragmentComplete();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer, fragment);
                transaction.commit();

            }
        });

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        customDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();

    }
}