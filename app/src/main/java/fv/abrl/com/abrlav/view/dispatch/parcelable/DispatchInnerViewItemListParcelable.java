
package fv.abrl.com.abrlav.view.dispatch.parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DispatchInnerViewItemListParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("ITEM_DESC")
    @Expose
    private String iTEMDESC;
    @SerializedName("Item")
    @Expose
    private Integer item;
    @SerializedName("Quantity")
    @Expose
    private Integer quantity;
    @SerializedName("GRN_Cost")
    @Expose
    private Integer gRNCost;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getITEMDESC() {
        return iTEMDESC;
    }

    public void setITEMDESC(String iTEMDESC) {
        this.iTEMDESC = iTEMDESC;
    }

    public Integer getItem() {
        return item;
    }

    public void setItem(Integer item) {
        this.item = item;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getGRNCost() {
        return gRNCost;
    }

    public void setGRNCost(Integer gRNCost) {
        this.gRNCost = gRNCost;
    }

}
