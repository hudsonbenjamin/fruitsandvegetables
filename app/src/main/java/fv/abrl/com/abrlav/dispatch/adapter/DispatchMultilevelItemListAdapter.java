package fv.abrl.com.abrlav.dispatch.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.dispatch.paracelable.DispatchItemListParacelable;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreDataParacable;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreListParacelable;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/17/2018.
 */

public class DispatchMultilevelItemListAdapter extends BaseAdapter {
    private Context mContext;


    ArrayList<DispatchItemListParacelable> arrayList;

    public DispatchMultilevelItemListAdapter(Context c,ArrayList<DispatchItemListParacelable> arrayList) {
        mContext = c;

        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        final DispatchItemListParacelable retrivedata = arrayList.get(position);


        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid                                   =  new View(mContext);
            grid                                   =  inflater.inflate(R.layout.adapter_dispatch_selectstore_productlist, null);
            final TextView itemName                = (TextView) grid.findViewById(R.id.itemName);
            final TextView itemCode                = (TextView) grid.findViewById(R.id.itemCode);
            final TextView quantity                = (TextView) grid.findViewById(R.id.quanatity);
            final TextView cost                    = (TextView) grid.findViewById(R.id.cost);



            itemName.setText(""+retrivedata.getITEMDESC()+" ("+retrivedata.getSTANDARDUOM()+")");
            itemCode.setText(""+retrivedata.getITEM());
            quantity.setText("Quantity : "+retrivedata.getSOH());
            cost.setText("Cost : "+retrivedata.getCost());


        } else {
            grid = (View) convertView;
        }



        return grid;
    }


}
