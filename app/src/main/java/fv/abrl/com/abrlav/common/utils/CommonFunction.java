package fv.abrl.com.abrlav.common.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import fv.abrl.com.abrlav.R;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by Hudson on 7/20/2018.
 */

public class CommonFunction {

    public static boolean isNetworkAvailable(Context cts) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) cts.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
