package fv.abrl.com.abrlav.dispatch.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.utils.CommonFunction;
import fv.abrl.com.abrlav.dispatch.adapter.DispatchSelectStoreAdapterRv;
import fv.abrl.com.abrlav.dispatch.paracelable.DispatchGenerateToData;
import fv.abrl.com.abrlav.dispatch.paracelable.ToNumberDataParcelable;
import fv.abrl.com.abrlav.dispatch.paracelable.ToNumberListParcelable;
import fv.abrl.com.abrlav.grn.adapter.CollectionCenterListAdapter;
import fv.abrl.com.abrlav.grn.paracelable.GetCollectionCenterDataParacelable;
import fv.abrl.com.abrlav.grn.paracelable.GetCollectionCenterListParcelable;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.home.fragment.HomeFragment;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreDataParacable;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreListParacelable;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/12/2018.
 */

public class SelectStoreFragment extends Fragment {




    @BindView(R.id.dispatchGenerateOtp)
    TextView dispatchGenerateOtp;
    @BindView(R.id.dispatchCollectionCentername)
    TextView dispatchCollectionCentername;

    @BindView(R.id.storelistlv)
    IndexFastScrollRecyclerView storelistlv;

    @BindView(R.id.spinnerlist)
    Spinner spinnerlist;

    @BindView(R.id.selectAllDispatchTv)
    TextView selectAllDispatchTv;

    @BindView(R.id.clearAllDispatchTV)
    TextView clearAllDispatchTV;

    @BindView(R.id.DispatchCancel)
    TextView DispatchCancel;

    @BindView(R.id.searchDispatch)
    EditText searchDispatch;

    @BindView(R.id.clearSearchText)
    ImageView clearSearchText;

    private Unbinder unbinder;
    private Fragment fragment;
    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    @BindView(R.id.indicator)
    AVLoadingIndicatorView indicator;

    public SelectStoreFragment() {
        // Required empty public constructor
    }

    ArrayList<PickingStoreListParacelable> storelist;
    ArrayList<GetCollectionCenterListParcelable> collectioncenterlist = new ArrayList<GetCollectionCenterListParcelable>();
    ArrayList<PickingStoreListParacelable>selectedStorelist;


    DispatchSelectStoreAdapterRv mAdapter;

    public  static ArrayList<ToNumberListParcelable> toNumberlist = new ArrayList<ToNumberListParcelable>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        sharedPref              = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                  = sharedPref.edit();



        storelist               = new ArrayList<PickingStoreListParacelable>();
        selectedStorelist       = new ArrayList<PickingStoreListParacelable>();
        try{
            ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                    .getColor(R.color.bottombar)));

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "Select Store" + "</font>"));
            HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
            if (Build.VERSION.SDK_INT >= 21) {
                getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        toNumberlist.clear();
        selectedStorelist.clear();

        View view       =  inflater.inflate(R.layout.fragment_dispatch_store_select, container, false);
        unbinder        = ButterKnife.bind(this,view);


        mAdapter                =       new DispatchSelectStoreAdapterRv(getActivity(), storelist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        storelistlv.setLayoutManager(mLayoutManager);
        storelistlv.setItemAnimator(new DefaultItemAnimator());

        storelistlv.setIndexTextSize(12);
        storelistlv.setIndexBarColor("#ffffff");
        storelistlv.setIndexBarCornerRadius(0);
        storelistlv.setIndexbarMargin(0);
        storelistlv.setIndexbarWidth(40);
        storelistlv.setPreviewPadding(0);
        storelistlv.setIndexBarTextColor("#818181");
        storelistlv.setIndexBarVisibility(true);
        storelistlv.setIndexbarHighLateTextColor("#ed6c09");
        storelistlv.setIndexBarHighLateTextVisibility(true);
        storelistlv.setAdapter(mAdapter);

        try{
            pickstoreList();
        }catch (Exception e){
            e.printStackTrace();
        }


        try{
            getCollectionCenterlist();
        }catch (Exception e){
            e.printStackTrace();
        }

        spinnerlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                editor.putString("collectioncentername",collectioncenterlist.get(position).getCCNAME()).apply();
                editor.putString("collectioncentercode",""+collectioncenterlist.get(position).getCC()).apply();
                editor.putString("collectioncenterpostion",""+position).apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        Log.e("ccocde",  ""+sharedPref.getString("collectioncentercode","default"));

        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        try{
            if(!sharedPref.getString("collectioncentername","default").equals("default")){
                dispatchCollectionCentername.setText(""+sharedPref.getString("collectioncentername","default"));
            }else{
                dispatchCollectionCentername.setText("Collection Center Name");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        DispatchCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new HomeFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer, fragment);
                transaction.commit();
            }
        });



        selectAllDispatchTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{

                for(int i=0;i <storelist.size();i++) {

                                    storelist.get(i).setCheckeditem(1);


                }
               mAdapter.notifyDataSetChanged();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        clearAllDispatchTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    for(int i=0;i <storelist.size();i++) {

                        storelist.get(i).setCheckeditem(0);


                    }
                    mAdapter.notifyDataSetChanged();
                }catch (Exception e){
                    e.printStackTrace();
                }


            }
        });

        dispatchGenerateOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                for(int i=0;i <storelist.size();i++) {
                    if (storelist.get(i).getCheckeditem() == 1) {
                        PickingStoreListParacelable s1 =
                                new PickingStoreListParacelable(storelist.get(i).getSTORENAME(),
                                storelist.get(i).getSTORE(),
                                storelist.get(i).getCheckeditem(),
                                storelist.get(i).getStoreQunantity(),
                                storelist.get(i).getItemdesc(),
                                storelist.get(i).getiTEM(),
                                sharedPref.getString("collectioncentername","default"),
                                Integer.parseInt(sharedPref.getString("collectioncentercode","default")),
                                "Completed");
                                selectedStorelist.add(s1);
                    }
                }

                    if (selectedStorelist.size() > 0) {
                        if(CommonFunction.isNetworkAvailable(getActivity())) {
                            showDialog(getActivity(),"TO will be generated, Do you Wish to continue ?");
                        }else{
                            Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                        }


                    } else {

                        Toast.makeText(getActivity(),"Please select at least one item",Toast.LENGTH_SHORT).show();
                    }

                }

        });

        clearSearchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchDispatch.setText("");
            }
        });


        searchDispatch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

                mAdapter.getFilter().filter(arg0);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {


                mAdapter.getFilter().filter(arg0);

            }
        });


//        clearSearchText.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                pickbyStoreListSearch.setText("");
//            }
//        });

    }

    private void pickstoreList() {
        Log.e("",""+sharedPref.getString("collectioncentercode","default"));

        indicator.setVisibility(View.VISIBLE);
        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getDispatchstorelist(sharedPref.getString("collectioncentercode","default"),new Callback<PickingStoreDataParacable>()

        {
            @Override
            public void success(PickingStoreDataParacable getccParacables, Response response) {

                if(0 != getccParacables.get$values().size()){

                storelist.addAll(getccParacables.get$values());
                mAdapter.notifyDataSetChanged();
                }else {
                    Toast.makeText(getActivity(),"No Item Found",Toast.LENGTH_SHORT).show();
                }

                indicator.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {

                indicator.setVisibility(View.GONE);

                Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }
    private void getCollectionCenterlist() {



        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getCollectionList(new Callback<GetCollectionCenterDataParacelable>()

        {
            @Override
            public void success(GetCollectionCenterDataParacelable getccParacables, Response response) {

                collectioncenterlist.addAll(getccParacables.get$values());


                spinnerlist.setAdapter(new CollectionCenterListAdapter(getActivity(), collectioncenterlist));

                try{
                    if(!sharedPref.getString("collectioncenterpostion","default").equals("default")){
                        spinnerlist.setSelection(Integer.parseInt(sharedPref.getString("collectioncenterpostion","default")));
                        editor.putString("collectioncentercode",""+collectioncenterlist.get(0).getCC()).apply(); }else{

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }


            }

            @Override
            public void failure(RetrofitError error) {


                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }

    private void generateTo(String storeid) {

        indicator.setVisibility(View.VISIBLE);

        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.generateTo(storeid,new Callback<ToNumberDataParcelable>()

        {
            @Override
            public void success(ToNumberDataParcelable getccParacables, Response response) {

                try{
                    //if(getccParacables.get$values().get(0).getStoreoutput().equals("true")){

                        toNumberlist.addAll(getccParacables.get$values());
                        fragment = new PrintStoreFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.homeContainer, fragment);
                        transaction.commit();

//                    }else {
//                        //Toast.makeText(getActivity(),"No Item Found",Toast.LENGTH_SHORT).show();
//                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                indicator.setVisibility(View.GONE);

            }

            @Override
            public void failure(RetrofitError error) {

                indicator.setVisibility(View.GONE);

                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }


    public void showDialog(Activity activity, String msg){

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_customsubmit);

        TextView text = (TextView) dialog.findViewById(R.id.customDialogMessage);
        text.setText(msg);

        TextView customSubmit = (TextView) dialog.findViewById(R.id.customSubmit);
        ImageView closeDialog = (ImageView) dialog.findViewById(R.id.closeDialog);
        TextView customDraft = (TextView) dialog.findViewById(R.id.customDraft);
        customSubmit.setText("Yes");
        customDraft.setText("No");
        customSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                try{

                    Gson gson = new GsonBuilder().create();
                    JsonArray myCustomArray = gson.toJsonTree(selectedStorelist).getAsJsonArray();
                    generateTo(myCustomArray.toString());


                    // }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        customDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();

    }
}