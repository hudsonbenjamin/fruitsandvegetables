package fv.abrl.com.abrlav.grn.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.utils.CommonFunction;
import fv.abrl.com.abrlav.grn.adapter.GRN_SubmissionAdapterRv;
import fv.abrl.com.abrlav.grn.paracelable.ConfirmGRNDataParacelable;
import fv.abrl.com.abrlav.grn.paracelable.GrnItemParacelable;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.home.fragment.HomeFragment;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/10/2018.
 */

public class GrnSubmissionFragment  extends Fragment {



    @BindView(R.id.summaryViewExpand)
     TextView summaryViewExpand;

    @BindView(R.id.submissionTotalKg)
    TextView submissionTotalKg;

    @BindView(R.id.totalmoney)
    TextView totalmoney;

    @BindView(R.id.VendorName)
    TextView VendorName;

    @BindView(R.id.VendorCode)
    TextView vendorcode;

    @BindView(R.id.collectionName)
    TextView collectionName;

    @BindView(R.id.totalItems)
    TextView totalItems;

    @BindView(R.id.GRNSubmissionSearch)
    EditText GRNSubmissionSearch;

    @BindView(R.id.grnsubmissionSubmit)
     TextView grnsubmissionSubmit;

    @BindView(R.id.clearSearchText)
    ImageView clearSearchText;


    @BindView(R.id.grnSummaryview)
    LinearLayout grnSummaryview;

    @BindView(R.id.cancelGrnSubmission)
    TextView cancelGrnSubmission;

    @BindView(R.id.indicator)
    AVLoadingIndicatorView indicator;


    @BindView(R.id.grnSubmissionItemList)
    IndexFastScrollRecyclerView grnSubmissionItemList;

    private Unbinder unbinder;
    private Fragment fragment;

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    ArrayList<GrnItemParacelable> selectedlist = new ArrayList<GrnItemParacelable>();

    private double displaytotalcount = 0;
    private double displaytotalGrnKg = 0;

    public GrnSubmissionFragment() {
        // Required empty public constructor
    }

    GRN_SubmissionAdapterRv mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        sharedPref                  = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                      = sharedPref.edit();


        try{
            ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                    .getColor(R.color.bottombar)));

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "Submission" + "</font>"));
            HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
            if (Build.VERSION.SDK_INT >= 21) {
                getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        View view =  inflater.inflate(R.layout.fragment_grn_submission, container, false);
        unbinder = ButterKnife.bind(this,view);


        try{
           // submissionTotalKg.setText(""+sharedPref.getString("grnsumofkg","0"));
           // totalmoney.setText(""+sharedPref.getString("grntotalamount","0"));

            collectionName.setText(""+sharedPref.getString("collectioncentername","0"));
            VendorName.setText(""+sharedPref.getString("VendorName","0"));
            vendorcode.setText(""+sharedPref.getString("VendorCode","0"));
            totalItems.setText(": "+GrnItemSelectionFragment.grnarrayselectedlist.size());
        }catch (Exception e){
            e.printStackTrace();
        }

//        try{
//            grnSubmissionItemList.setAdapter(new GrnSubmissionAdapter(getActivity(), GrnItemSelectionFragment.grnarrayselectedlist));
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        mAdapter                =       new GRN_SubmissionAdapterRv(getActivity(), GrnItemSelectionFragment.grnarrayselectedlist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        grnSubmissionItemList.setLayoutManager(mLayoutManager);
        grnSubmissionItemList.setItemAnimator(new DefaultItemAnimator());
        grnSubmissionItemList.setItemViewCacheSize(GrnItemSelectionFragment.grnarrayselectedlist.size());
        grnSubmissionItemList.setIndexTextSize(12);
        grnSubmissionItemList.setIndexBarColor("#ffffff");
        grnSubmissionItemList.setIndexBarCornerRadius(0);
        grnSubmissionItemList.setIndexbarMargin(0);
        grnSubmissionItemList.setIndexbarWidth(40);
        grnSubmissionItemList.setPreviewPadding(0);
        grnSubmissionItemList.setIndexBarTextColor("#818181");
        grnSubmissionItemList.setIndexBarVisibility(true);
        grnSubmissionItemList.setIndexbarHighLateTextColor("#ed6c09");
        grnSubmissionItemList.setIndexBarHighLateTextVisibility(true);
        grnSubmissionItemList.setAdapter(mAdapter);







        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        summaryViewExpand.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                if(grnSummaryview.getVisibility()==View.VISIBLE){
                    summaryViewExpand.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down));
                    grnSummaryview.setVisibility(View.GONE);
                }else {
                    summaryViewExpand.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_up));
                    grnSummaryview.setVisibility(View.VISIBLE);

                    try{

                        displaytotalcount =0;
                        displaytotalGrnKg =0;


                        for(int i =0;i<GrnItemSelectionFragment.grnarrayselectedlist.size();i++){
                            if(GrnItemSelectionFragment.grnarrayselectedlist.get(i).getCheckedItem()== 1){
                                double totalmoney = GrnItemSelectionFragment.grnarrayselectedlist.get(i).getGrnQuantity()*GrnItemSelectionFragment.grnarrayselectedlist.get(i).getGrnCost();
                                displaytotalcount = displaytotalcount + totalmoney;
                                displaytotalGrnKg = displaytotalGrnKg+GrnItemSelectionFragment.grnarrayselectedlist.get(i).getGrnQuantity();
                            }

                        }

                        totalmoney.setText(": "+displaytotalcount);
                        submissionTotalKg.setText(": "+displaytotalGrnKg);
                    }catch (Exception e){
                        e.printStackTrace();
                    }




                }


            }
        });


        clearSearchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GRNSubmissionSearch.setText("");
            }
        });


        grnsubmissionSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    for(int i=0;i<GrnItemSelectionFragment.grnarrayselectedlist.size();i++){
                        if(GrnItemSelectionFragment.grnarrayselectedlist.get(i).getCheckedItem()== 1){


                            GrnItemParacelable s1 = new GrnItemParacelable(

                                    GrnItemSelectionFragment.grnarrayselectedlist.get(0).getITEMDESC(),
                                    GrnItemSelectionFragment.grnarrayselectedlist.get(0).getGrnCost(),
                                    GrnItemSelectionFragment.grnarrayselectedlist.get(0).getGrnQuantity(),
                                    GrnItemSelectionFragment.grnarrayselectedlist.get(0).getMPP(),1,
                                    GrnItemSelectionFragment.grnarrayselectedlist.get(0).getSTANDARDUOM(),
                                    GrnItemSelectionFragment.grnarrayselectedlist.get(0).getITEM(),
                                    sharedPref.getString("invoicenumber","default"),Integer.parseInt(sharedPref.getString("collectioncentercode","default")) ,
                                    sharedPref.getString("VendorName","default"),
                                    Integer.parseInt(sharedPref.getString("VendorCode","default")),
                                    sharedPref.getString("VendorCode","0"),
                                    GrnItemSelectionFragment.grnarrayselectedlist.get(0).getGrnStatus(),
                                    sharedPref.getString("grnnumber","default"));

                            selectedlist.add(s1);
                        }
                    }


                    if(selectedlist.size()>0){

                        if(CommonFunction.isNetworkAvailable(getActivity())) {
                            showDialog(getActivity(),"GRN will be generated, Do you wish to continue ?");
                        }else{
                            Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                        }



                    }else {
                        Toast.makeText(getActivity(),"Please select atleast one item.",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }





            }
        });

        GRNSubmissionSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                try{
                    mAdapter.getFilter().filter(s);

                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onTextChanged(CharSequence s, int arg1, int arg2,
                                      int arg3) {
                try{
                    mAdapter.getFilter().filter(s);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        cancelGrnSubmission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragment = new GrnItemSelectionCancelFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer,fragment);
                transaction.commit();

            }
        });

    }


    private void submitGrnItem(String result) {

        indicator.setVisibility(View.VISIBLE);

        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);
        api.submitGrnItem(result,new Callback<ConfirmGRNDataParacelable>()

        {
            @Override
            public void success(ConfirmGRNDataParacelable getccParacables, Response response) {

                try{
                    if (getccParacables.get$values().get(0).getAssigngrnresult().equals("true")){
                        Toast.makeText(getActivity(),"GRN have been successfully generated.",Toast.LENGTH_SHORT).show();


                        fragment = new GrnPrintFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.homeContainer,fragment);
                        transaction.commit();

                    }else{
                        Toast.makeText(getActivity(),"failed",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){

                    Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

                indicator.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {
                try{
                    indicator.setVisibility(View.GONE);
                    Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                }

                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }

    private void DraftGrnItem(String result) {


        indicator.setVisibility(View.VISIBLE);
        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);




        api.DrfatGrnItem(result,new Callback<ConfirmGRNDataParacelable>()

        {
            @Override
            public void success(ConfirmGRNDataParacelable getccParacables, Response response) {

                try{
                    if (getccParacables.get$values().get(0).getDraftgrn().equals("true")){
                        Toast.makeText(getActivity(),"GRN have been successfully saved.",Toast.LENGTH_SHORT).show();

                        fragment = new HomeFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.homeContainer,fragment);
                        transaction.commit();
                    }else{
                        Toast.makeText(getActivity(),"failed",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                indicator.setVisibility(View.GONE);

            }

            @Override
            public void failure(RetrofitError error) {
                try{
                    indicator.setVisibility(View.GONE);
                    Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                }

                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }


    public void showDialog(Activity activity, String msg){

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_customsubmit);

        TextView text = (TextView) dialog.findViewById(R.id.customDialogMessage);
        text.setText(msg);

        TextView customSubmit  = (TextView) dialog.findViewById(R.id.customSubmit);
        ImageView closeDialog  = (ImageView) dialog.findViewById(R.id.closeDialog);
        TextView customDraft   = (TextView) dialog.findViewById(R.id.customDraft);

        customDraft.setText("Save");
        customSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                try{

                    Gson gson = new GsonBuilder().create();

                    for(int i =0;i<GrnItemSelectionFragment.grnarrayselectedlist.size();i++){

                        if(GrnItemSelectionFragment.grnarrayselectedlist.get(i).getGrnCost()>GrnItemSelectionFragment.grnarrayselectedlist.get(i).getMPP()){
                            GrnItemSelectionFragment.grnarrayselectedlist.get(i).setGrnStatus("Pending");
                        }else{
                            GrnItemSelectionFragment.grnarrayselectedlist.get(i).setGrnStatus("Approved");
                        }
                    }

                    JsonArray myCustomArray = gson.toJsonTree(GrnItemSelectionFragment.grnarrayselectedlist).getAsJsonArray();
                    submitGrnItem(myCustomArray.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        customDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Gson gson = new GsonBuilder().create();


                for(int i =0;i<GrnItemSelectionFragment.grnarrayselectedlist.size();i++){

                    if(GrnItemSelectionFragment.grnarrayselectedlist.get(i).getGrnCost()>GrnItemSelectionFragment.grnarrayselectedlist.get(i).getMPP()){
                        GrnItemSelectionFragment.grnarrayselectedlist.get(i).setGrnStatus("Pending");
                    }else{
                        GrnItemSelectionFragment.grnarrayselectedlist.get(i).setGrnStatus("Approved");
                    }
                }


                JsonArray myCustomArray = gson.toJsonTree(GrnItemSelectionFragment.grnarrayselectedlist).getAsJsonArray();
                DraftGrnItem(myCustomArray.toString());
            }
        });

        dialog.show();

    }
}

