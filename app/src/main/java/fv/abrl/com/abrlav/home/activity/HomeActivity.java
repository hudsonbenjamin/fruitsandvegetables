package fv.abrl.com.abrlav.home.activity;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.grn.fragment.GrnItemSelectionCancelFragment;
import fv.abrl.com.abrlav.grn.fragment.GrnItemSelectionFragment;
import fv.abrl.com.abrlav.grn.fragment.GrnSubmissionFragment;
import fv.abrl.com.abrlav.grn.fragment.GrnVendorSelectionFragment;
import fv.abrl.com.abrlav.home.fragment.HomeFragment;
import fv.abrl.com.abrlav.login.activity.LoginActivity;
import fv.abrl.com.abrlav.mpp.fragment.MppEntryCancelFragment;
import fv.abrl.com.abrlav.mpp.fragment.MppEntryFragment;
import fv.abrl.com.abrlav.mpp.fragment.MppFinalCancelFragment;
import fv.abrl.com.abrlav.mpp.fragment.MppFinalFragment;
import fv.abrl.com.abrlav.notification.fragment.NotificationFragment;
import fv.abrl.com.abrlav.picking.store.fragment.PickByStoreFragmentComplete;
import fv.abrl.com.abrlav.picking.store.fragment.PickbyStoreFragmentSecondCancel;
import io.fabric.sdk.android.Fabric;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , LocationListener{

    private Fragment fragment;
    private BottomNavigationView bottomNavigationView;
    public static Toolbar toolbar;
    protected LocationManager locationManager;


   TextView ProfileUserName;
   TextView userType;

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_home);

        sharedPref                  =  getSharedPreferences("CallHippoSharedPreference", 0);
        editor                      = sharedPref.edit();

         toolbar                    = (Toolbar) findViewById(R.id.toolbar);



        setSupportActionBar(toolbar);
        DrawerLayout drawer             = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle    = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();




        NavigationView navigationView   = (NavigationView) findViewById(R.id.nav_view);

        View headerView = navigationView.getHeaderView(0);

        ProfileUserName                 = (TextView) headerView.findViewById(R.id.ProfileUserName);
        userType                        = (TextView) headerView.findViewById(R.id.userType);

        try{
           ProfileUserName.setText(""+sharedPref.getString("username","default"));
           userType.setText(""+sharedPref.getString("roletype","default"));
        }catch (Exception e){
            e.printStackTrace();
        }
        navigationView.setNavigationItemSelectedListener(this);


        bottomNavigationView            = (BottomNavigationView)findViewById(R.id.bottom_navigation);


        BottomNavigationMenuView menuView = (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
        for (int i = 0; i < menuView.getChildCount(); i++) {
            final View iconView = menuView.getChildAt(i).findViewById(android.support.design.R.id.icon);
            final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
            final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            // set your height here
            layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, displayMetrics);
            // set your width here
            layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, displayMetrics);
            iconView.setLayoutParams(layoutParams);
        }

       // CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) bottomNavigationView.getLayoutParams();
      //  layoutParams.setBehavior(new BottomNavigationViewBehavior());

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                int id = item.getItemId();

                switch(id)
                {
                    case R.id.action_notification:

                        fragment = new NotificationFragment();
                        break;

                    case R.id.action_home:
                        fragment = new HomeFragment();
                        break;
                    case R.id.action_back:

                        Fragment f = getFragmentManager().findFragmentById(R.id.homeContainer);
                        if(f instanceof MppFinalFragment)
                        {
                        fragment = new MppEntryCancelFragment();
                        }else if(f instanceof MppEntryFragment)
                        {
                            fragment = new HomeFragment();

                        }else if(f instanceof GrnItemSelectionFragment)
                        {
                            fragment = new GrnVendorSelectionFragment();
                         }
                         else if(f instanceof GrnSubmissionFragment)
                         { fragment = new GrnItemSelectionCancelFragment();
                         }
                         else if (f instanceof PickByStoreFragmentComplete){
                            fragment = new PickbyStoreFragmentSecondCancel();
                        }
                        else
                        { fragment = new HomeFragment();
                        }
                        break;


                }

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer,fragment);
                transaction.commit();

                return true;
            }
        });

        bottomNavigationView.setSelectedItemId(R.id.action_home);

//        try{
//            getLocation();
//        }catch (Exception e){
//            e.printStackTrace();
//        }

    }

    @Override
    public void onBackPressed() {
        finish();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }




    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        }else if(id == R.id.nav_logout){
            editor.putString("username","default").apply();
            Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }
    void getLocation() {

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) this);
        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {



        getaddress(location.getLatitude(),location.getLongitude());

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }
    private void getaddress(double latitude, double longitude){


        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//            String city = addresses.get(0).getLocality();
            String subLocality = addresses.get(0).getSubLocality();
//            String SubAdminArea = addresses.get(0).getSubAdminArea();
//
//            String state = addresses.get(0).getAdminArea();
//            String country = addresses.get(0).getCountryName();
//            String knownName = addresses.get(0).getFeatureName();



           // Toast.makeText(HomeActivity.this, ""+subLocality, Toast.LENGTH_SHORT).show();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
