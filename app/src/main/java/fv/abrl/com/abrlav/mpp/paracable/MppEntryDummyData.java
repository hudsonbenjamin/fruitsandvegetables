package fv.abrl.com.abrlav.mpp.paracable;

/**
 * Created by Hudson on 7/12/2018.
 */

public class MppEntryDummyData {

    String vegatablename;
    String MarketPrice;
    String Mpp;
    String FinalPrice;
    int checkboxvalue = 0;
    public int getCheckboxvalue() {
        return checkboxvalue;
    }

    public void setCheckboxvalue(int checkboxvalue) {
        this.checkboxvalue = checkboxvalue;
    }



    public MppEntryDummyData(String vegatablename, String MarketPrice, String Mpp, String FinalPrice,int checkvalue) {
        this.vegatablename=vegatablename;
        this.MarketPrice=MarketPrice;
        this.Mpp=Mpp;
        this.FinalPrice=FinalPrice;
        this.checkboxvalue=checkvalue;
    }

    public String getVegatablename() {
        return vegatablename;
    }

    public void setVegatablename(String vegatablename) {
        this.vegatablename = vegatablename;
    }

    public String getMarketPrice() {
        return MarketPrice;
    }

    public void setMarketPrice(String marketPrice) {
        MarketPrice = marketPrice;
    }

    public String getMpp() {
        return Mpp;
    }

    public void setMpp(String mpp) {
        Mpp = mpp;
    }

    public String getFinalPrice() {
        return FinalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        FinalPrice = finalPrice;
    }





}
