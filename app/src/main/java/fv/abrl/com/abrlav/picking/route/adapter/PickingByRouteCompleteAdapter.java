package fv.abrl.com.abrlav.picking.route.adapter;


import android.content.Context;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreListParacelable;

public class PickingByRouteCompleteAdapter extends RecyclerView.Adapter<PickingByRouteCompleteAdapter.MyViewHolder>
        implements Filterable, SectionIndexer {

    private Context mContext;
    private ArrayList<PickingStoreListParacelable> arrayList;
    private ArrayList<PickingStoreListParacelable> filteredarraylist;
    private ArrayList<String> checkboxcount = new ArrayList<String>();
    private ArrayList<Integer> mSectionPositions;

    private PickingStoreListParacelable getItem(int position) {
        return filteredarraylist.get(position);
    }
    //private ValueFilter valueFilter;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CheckBox productID;
        TextView  itemCode;
        ImageView openStoreView;
        RecyclerView innerStoreList;
        LinearLayout grnitemViewDetails,itemViewitembyroute;


        public MyViewHolder(View view) {
            super(view);

            productID               = (CheckBox) view.findViewById(R.id.productID);
            itemCode                = (TextView) view.findViewById(R.id.itemcode);
            innerStoreList          = (RecyclerView) view.findViewById(R.id.innerStoreList);
            grnitemViewDetails      = (LinearLayout) view.findViewById(R.id.grnitemViewDetails);
            openStoreView           = (ImageView) view.findViewById(R.id.openStoreView);
            itemViewitembyroute     = (LinearLayout) view.findViewById(R.id.itemViewitembyroute);



        }
    }


    public PickingByRouteCompleteAdapter(Context c, ArrayList<PickingStoreListParacelable> arrayList) {
        mContext = c;
        this.arrayList = arrayList;
        this.filteredarraylist = arrayList;
        //setHasStableIds(true);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.picking_storelistbyproductselect_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.productID.setText(filteredarraylist.get(position).getItemdesc());

        try{
//            if(filteredarraylist.get(position).getItemdesc().equals(filteredarraylist.get(position-1).getItemdesc())){
//                holder.itemViewitembyroute.setLayoutParams(new LinearLayout.LayoutParams(0,0));
//              holder.itemViewitembyroute.setVisibility(View.GONE);
//            }
//            else {

                holder.itemViewitembyroute.setVisibility(View.VISIBLE);
                GetStorebyItemCompleteMultiLevelAdapter  madapter            =       new GetStorebyItemCompleteMultiLevelAdapter(mContext,  ProductItemListbySelectAdapter.loadWholeinnerlist);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
                holder.innerStoreList.setLayoutManager(mLayoutManager);
                holder.innerStoreList.setItemAnimator(new DefaultItemAnimator());
                holder.innerStoreList.setAdapter(madapter);
                madapter.notifyDataSetChanged();

          //  }
        }catch (Exception e){
            e.printStackTrace();
        }


        holder.openStoreView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.grnitemViewDetails.getVisibility()==View.VISIBLE){
                    holder.grnitemViewDetails.setVisibility(View.GONE);
                    holder.openStoreView.setImageResource(R.drawable.ic_keyboard_arrow_down_orange_24dp);
                }else{
                    holder.grnitemViewDetails.setVisibility(View.VISIBLE);
                    holder.openStoreView.setImageResource(R.drawable.ic_keyboard_arrow_up_orange_35dp);


                }



            }
        });



        holder.productID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (holder.productID.isChecked()) {
                    holder.productID.setChecked(false);

                } else {
                    holder.productID.setChecked(false);
                }

            }

        });

        if(filteredarraylist.get(position).getCheckeditem()==1){
            holder.productID.setChecked(true);
        }else{
            holder.productID.setChecked(false);
        }



    }




    @Override
    public int getItemCount() {

        return filteredarraylist.size();

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                ArrayList<PickingStoreListParacelable> filteredList = new ArrayList<>();
                if (charString.isEmpty()) {
                    filteredarraylist = arrayList;

                } else {

                    for (PickingStoreListParacelable row : arrayList) {
                        if (row.getItemdesc().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredarraylist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredarraylist;
                filterResults.count = filteredarraylist.size();
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                filteredarraylist = (ArrayList<PickingStoreListParacelable>) filterResults.values;
                notifyDataSetChanged();



            }
        };
    }



    @Override
    public int getItemViewType(int position) {
        return position;
    }




    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>(26);
        mSectionPositions = new ArrayList<>(26);
        for (int i = 0, size = filteredarraylist.size(); i < size; i++) {
            String section = String.valueOf(filteredarraylist.get(i).getItemdesc().charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mSectionPositions.get(sectionIndex);
    }


    public long getItemId(int position) {
        return position;
    }




}