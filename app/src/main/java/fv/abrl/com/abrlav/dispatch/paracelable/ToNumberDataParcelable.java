
package fv.abrl.com.abrlav.dispatch.paracelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ToNumberDataParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<ToNumberListParcelable> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<ToNumberListParcelable> get$values() {
        return $values;
    }

    public void set$values(List<ToNumberListParcelable> $values) {
        this.$values = $values;
    }

}
