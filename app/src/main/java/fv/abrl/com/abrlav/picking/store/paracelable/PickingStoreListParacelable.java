
package fv.abrl.com.abrlav.picking.store.paracelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickingStoreListParacelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("STORE")
    @Expose
    private int sTORE;
    @SerializedName("STORE_NAME")
    @Expose
    private String sTORENAME;
    private int Quantity =0;

    @SerializedName("ITEM_DESC")
    @Expose
    private String itemdesc;
    @SerializedName("ITEM")
    @Expose
    private int iTEM;

    private int position;


    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public int getCcode() {
        return ccode;
    }

    public void setCcode(int ccode) {
        this.ccode = ccode;
    }

    @SerializedName("CC_Name")
    @Expose
    String cc;

    @SerializedName("CC_Code")
    @Expose
    int ccode;

    @SerializedName("Status")
    @Expose
    String status ="Completed";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }




    public int getStoreQunantity() {
        return Quantity;
    }

    public void setStoreQunantity(int storeQunantity) {
        Quantity = storeQunantity;
    }




    public int getiTEM() {
        return iTEM;
    }

    public void setiTEM(Integer iTEM) {
        this.iTEM = iTEM;
    }


    public String getItemdesc() {
        return itemdesc;
    }

    public void setItemdesc(String itemdesc) {
        this.itemdesc = itemdesc;
    }




    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }




    public int getCheckeditem() {
        return checkeditem;
    }

    public void setCheckeditem(int checkeditem) {
        this.checkeditem = checkeditem;
    }

    private int checkeditem = 0;


    public PickingStoreListParacelable(String storename, int storecode,int checkvalue,int storeQunantity,String itemdesc,int iTEM,String cc,int c_code,String status) {
        this.sTORE          =   storecode;
        this.sTORENAME      =   storename;
        this.checkeditem    =   checkvalue;
        this.Quantity       =   storeQunantity;
        this.itemdesc       =   itemdesc;
        this.iTEM           =   iTEM;
        this.cc             =   cc;
        this.ccode          =   c_code;
        this.status         =   status;

    }


    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getSTORE() {
        return sTORE;
    }

    public void setSTORE(int sTORE) {
        this.sTORE = sTORE;
    }

    public String getSTORENAME() {
        return sTORENAME;
    }

    public void setSTORENAME(String sTORENAME) {
        this.sTORENAME = sTORENAME;
    }

}
