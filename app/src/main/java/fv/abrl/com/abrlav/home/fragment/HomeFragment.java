package fv.abrl.com.abrlav.home.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.utils.CommonFunction;
import fv.abrl.com.abrlav.dispatch.fragment.SelectStoreFragment;
import fv.abrl.com.abrlav.farmer.fragment.FarmerRegistration;
import fv.abrl.com.abrlav.grn.fragment.GrnVendorSelectionFragment;
import fv.abrl.com.abrlav.grn.paracelable.$value;
import fv.abrl.com.abrlav.grn.paracelable.GetVendorDataParacable;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.home.adapter.ImageAdapterGridView;
import fv.abrl.com.abrlav.mpp.fragment.MppApprovalFragment;
import fv.abrl.com.abrlav.mpp.fragment.MppEntryFragment;
import fv.abrl.com.abrlav.mpp.fragment.MppFinalFragment;
import fv.abrl.com.abrlav.notification.fragment.NotificationFragment;
import fv.abrl.com.abrlav.picking.fragment.PickingMenuFragment;
import fv.abrl.com.abrlav.view.viewhome.fragment.ViewHomeFragment;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static fv.abrl.com.abrlav.R.drawable.menu_icon_white_resize;

/**
 * Created by Hudson on 7/6/2018.
 */

public class HomeFragment extends Fragment {



    @BindView(R.id.gridviewMenu)
     GridView gridviewMenu;


    private Fragment fragment;
    private Unbinder unbinder;
    private View view;
    private int[] imageIDs = {
            R.drawable.mpp, R.drawable.creategrn, R.drawable.picking,
            R.drawable.dispatch, R.drawable.view
    };

    private String[] section = {
            "MPP",
            "GRN",
            "PICKING",
            "DISPATCH",
            "VIEW",

    } ;



    @BindView(R.id.welcomeUser)
    TextView welcomeUser;


    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;


    public HomeFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view                    =  inflater.inflate(R.layout.fragment_home, container, false);
         unbinder                =  ButterKnife.bind(this,view);

        sharedPref               = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                   = sharedPref.edit();


        ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
               .getColor(R.color.colorPrimary)));
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(" ");
        HomeActivity.toolbar.setNavigationIcon(menu_icon_white_resize);
        if (Build.VERSION.SDK_INT >= 21) {
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }



        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        try {
            welcomeUser.setText("Welcome " + sharedPref.getString("username", "username"));
        } catch (Exception e) {
            e.printStackTrace();
        }

//        if(sharedPref.getInt("role",0)==1){
//            gridviewMenu.setAdapter(new ImageAdapterGridView(getActivity(), regionalbuyer, imageIDs));
//        }else if(sharedPref.getInt("role",0)==2){
//            gridviewMenu.setAdapter(new ImageAdapterGridView(getActivity(), regionalbuyer, imageIDs));
//        } else{
        gridviewMenu.setAdapter(new ImageAdapterGridView(getActivity(), section, imageIDs));
        //  }


        gridviewMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent,
                                    View v, int position, long id) {

                if (position == 0) {


                    if (CommonFunction.isNetworkAvailable(getActivity())) {

                        MppEntryFragment.canceldatalist.clear();
                        MppEntryFragment.dataarraylist.clear();
                        MppFinalFragment.addFinalVeglist.clear();
                        MppFinalFragment.MppEntrycanceldatalist.clear();

                        if (sharedPref.getInt("role", 0) == 1) {
                            fragment = new MppEntryFragment();
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            transaction.replace(R.id.homeContainer, fragment, "MPPEntryFragment");
                            transaction.commit();
                        } else if (sharedPref.getInt("role", 0) == 2) {
                            fragment = new MppApprovalFragment();
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            transaction.replace(R.id.homeContainer, fragment, "MPPApprovalFragment");
                            transaction.commit();
                        } else {
                            fragment = new MppEntryFragment();
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            transaction.replace(R.id.homeContainer, fragment, "MPPEntryFragment");
                            transaction.commit();
                        }
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }


                } else if (position == 1) {
                    if (CommonFunction.isNetworkAvailable(getActivity())) {
                        editor.putString("collectioncentername", "default").apply();
                        editor.putString("VendorName", "default").apply();
                        editor.putString("VendorCode", "default").apply();
                        editor.putString("invoicenumber", "default").apply();
                        editor.putString("grnsumofkg", "0").apply();
                        editor.putString("grntotalamount", "0").apply();

                        fragment = new GrnVendorSelectionFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.homeContainer, fragment,"GrnVendorSelectionFragment");
                        transaction.commit();
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                } else if (position == 2) {
                    if (CommonFunction.isNetworkAvailable(getActivity())) {
                        fragment = new PickingMenuFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.homeContainer, fragment,"PickingMenuFragment");

                        transaction.commit();
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                } else if (position == 3) {
                    if (CommonFunction.isNetworkAvailable(getActivity())) {
                        fragment = new SelectStoreFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.homeContainer, fragment,"SelectStoreFragment");
                        transaction.commit();
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                } else if (position == 4) {
                    if (CommonFunction.isNetworkAvailable(getActivity())) {
                        fragment = new ViewHomeFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.homeContainer, fragment,"ViewHomeFragment");
                        transaction.commit();
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Grid Item " + (position + 1) + " Selected", Toast.LENGTH_LONG).show();
                }

            }
        });


    }



}