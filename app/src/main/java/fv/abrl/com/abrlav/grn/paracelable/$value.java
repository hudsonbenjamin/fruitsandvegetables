
package fv.abrl.com.abrlav.grn.paracelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class $value {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("SUPPLIER")
    @Expose
    private int sUPPLIER;
    @SerializedName("SUP_NAME")
    @Expose
    private String sUPNAME;


    public $value(int suppilercode, String suppilername) {
        this.sUPPLIER =suppilercode;
        this.sUPNAME = suppilername;

    }



    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getSUPPLIER() {
        return sUPPLIER;
    }

    public void setSUPPLIER(int sUPPLIER) {
        this.sUPPLIER = sUPPLIER;
    }

    public String getSUPNAME() {
        return sUPNAME;
    }

    public void setSUPNAME(String sUPNAME) {
        this.sUPNAME = sUPNAME;
    }

//    public String toString(){
//        return "id : " + get$id() + "\nsupperliercode : " + getSUPPLIER() + "\nsuppliername : " + getSUPNAME();
//    }

}
