
package fv.abrl.com.abrlav.grn.paracelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GRNSubmitParacable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("assigngrnresult")
    @Expose
    private Assigngrnresult assigngrnresult;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public Assigngrnresult getAssigngrnresult() {
        return assigngrnresult;
    }

    public void setAssigngrnresult(Assigngrnresult assigngrnresult) {
        this.assigngrnresult = assigngrnresult;
    }

}
