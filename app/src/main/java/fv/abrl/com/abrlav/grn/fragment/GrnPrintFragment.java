package fv.abrl.com.abrlav.grn.fragment;




import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.wang.avi.AVLoadingIndicatorView;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.utils.CommonFunction;
import fv.abrl.com.abrlav.common.utils.FileDownloader;
import fv.abrl.com.abrlav.dispatch.fragment.PrintStoreFragment;
import fv.abrl.com.abrlav.dispatch.paracelable.PrintToParcelable;
import fv.abrl.com.abrlav.grn.paracelable.ConfirmGRNDataParacelable;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/10/2018.
 */

public class GrnPrintFragment extends Fragment {


    @BindView(R.id.grnInvoiceCode)
    TextView grnInvoiceCode;

    @BindView(R.id.VendornamePrint)
    TextView VendornamePrint;

    @BindView(R.id.grnVendorCode)
    TextView grnVendorCode;

    @BindView(R.id.collectionCenter)
    TextView collectionCenter;

    @BindView(R.id.grnItemSelectionSubmit)
    TextView grnItemSelectionSubmit;

    @BindView(R.id.grnnumber)
    TextView grnnumber;

    @BindView(R.id.indicator)
    AVLoadingIndicatorView indicator;



    private Unbinder unbinder;
    private Fragment fragment;

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    public GrnPrintFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        sharedPref                  = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                      = sharedPref.edit();

        try{
            ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                    .getColor(R.color.bottombar)));

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "Print GRN" + "</font>"));
            HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
            if (Build.VERSION.SDK_INT >= 21) {
                getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        View view =  inflater.inflate(R.layout.fragment_grn_print, container, false);
        unbinder = ButterKnife.bind(this,view);
        try{
            collectionCenter.setText(""+sharedPref.getString("collectioncentername","default"));
            VendornamePrint.setText(""+sharedPref.getString("VendorName","default"));
            grnVendorCode.setText(""+sharedPref.getString("VendorCode","default"));
            grnInvoiceCode.setText(""+sharedPref.getString("invoicenumber","default"));
            grnnumber.setText(""+sharedPref.getString("grnnumber","default"));
        }catch (Exception e){
            e.printStackTrace();
        }




        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        grnItemSelectionSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               try{


                   if(CommonFunction.isNetworkAvailable(getActivity())) {

                       generateGRNPDF(
                               Integer.parseInt(sharedPref.getString("collectioncentercode","default")),
                               Integer.parseInt(sharedPref.getString("VendorCode","default")));


                   }else{
                       Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                   }



               }catch (Exception e){
                   e.printStackTrace();
               }

            }
        });



    }

    private void CopyReadPDFFromAssets(Context ctx)
    {
        AssetManager assetManager = ctx.getAssets();

        InputStream in = null;
        OutputStream out = null;
        File file = new File(ctx.getFilesDir(), "grn.pdf");
        try
        {
            in = assetManager.open("grn.pdf");
            out = ctx.openFileOutput(file.getName(), Context.MODE_WORLD_READABLE);

            copyPdfFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e)
        {
            Log.e("exception", e.getMessage());
        }

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(
                Uri.parse("file://" + ctx.getFilesDir() + "/grn.pdf"),
                "application/pdf");

        startActivity(intent);
    }

    private void copyPdfFile(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1)
        {
            out.write(buffer, 0, read);
        }
    }



    public void viewPDF()
    {
        indicator.setVisibility(View.GONE);
        // -> filename = maven.pdf

       // if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/FNV/" + sharedPref.getString("grnnumber","default")+".pdf");
        Uri path = FileProvider.getUriForFile(getActivity(), "fv.abrl.com.abrlav.provider", pdfFile);
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        pdfIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(pdfIntent);

          }



    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "FNV");
            folder.mkdir();
            File pdfFile = new File(folder,   sharedPref.getString("grnnumber","default")+".pdf");

            try{
                pdfFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);
            return null;

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try{

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "Download PDf successfully", Toast.LENGTH_SHORT).show();
                        viewPDF();
                    }
                }, 5000);

            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }




    private void generateGRNPDF(int cc,int vendorcode) {

        indicator.setVisibility(View.VISIBLE);

        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);

        api.generateGRNPDF(cc,
                vendorcode,
                sharedPref.getString("VendorName","default"),
                sharedPref.getString("VendorCode","default"),
                sharedPref.getString("grnnumber","default"),
                sharedPref.getString("invoicenumber","default"),
                new Callback<PrintToParcelable>()

        {
            @Override
            public void success(PrintToParcelable getccParacables, Response response) {
                Toast.makeText(getActivity(),"success",Toast.LENGTH_SHORT).show();
                indicator.setVisibility(View.VISIBLE);
                new DownloadFile().execute((ApiConstants.pdfurl+"/"+getccParacables.get$values().get(0).getURL()).replace(" ","%20"));

            }

            @Override
            public void failure(RetrofitError error) {
                indicator.setVisibility(View.GONE);
                Toast.makeText(getActivity(),"failed"+error.getMessage(),Toast.LENGTH_SHORT).show();



            }
        });

    }



}
