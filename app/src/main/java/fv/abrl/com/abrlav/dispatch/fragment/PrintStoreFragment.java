package fv.abrl.com.abrlav.dispatch.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.utils.CommonFunction;
import fv.abrl.com.abrlav.common.utils.FileDownloader;
import fv.abrl.com.abrlav.common.utils.RecyclerItemClickListener;
import fv.abrl.com.abrlav.dispatch.adapter.DispatchPrintScreenAdapter;
import fv.abrl.com.abrlav.dispatch.adapter.PrintStoreAdapter;
import fv.abrl.com.abrlav.dispatch.paracelable.PrintToParcelable;
import fv.abrl.com.abrlav.dispatch.paracelable.ToNumberListParcelable;
import fv.abrl.com.abrlav.grn.paracelable.ConfirmGRNDataParacelable;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreListParacelable;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/12/2018.
 */

public class PrintStoreFragment extends Fragment {

    @BindView(R.id.storelistlv)
    ListView storelistlv;

    @BindView(R.id.dispatchItemSelectionSubmit)
    TextView dispatchItemSelectionSubmit;

    @BindView(R.id.dispatchprintcollectionname)
    TextView dispatchprintcollectionname;




    private Unbinder unbinder;
    private Fragment fragment;

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    @BindView(R.id.indicator)

    AVLoadingIndicatorView indicator;



    public PrintStoreFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        sharedPref              = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                  = sharedPref.edit();


        try{
            ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                    .getColor(R.color.bottombar)));

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "Print TO" + "</font>"));
            HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
            if (Build.VERSION.SDK_INT >= 21) {
                getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        View view =  inflater.inflate(R.layout.fragment_dispatch_print, container, false);
        unbinder = ButterKnife.bind(this,view);

        try{
            storelistlv.setAdapter(new PrintStoreAdapter(getActivity(), SelectStoreFragment.toNumberlist));
        }catch (Exception e){
            e.printStackTrace();
        }



        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        dispatchItemSelectionSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                try{
//
//                    showPrintList(getActivity());
//            }catch (Exception e){
//                e.printStackTrace();
//            }

                if(CommonFunction.isNetworkAvailable(getActivity())) {


                    showPrintList(getActivity(),SelectStoreFragment.toNumberlist);
                }else{
                    Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                }


//                fragment = new HomeFragment();
//                FragmentTransaction transaction = getFragmentManager().beginTransaction();
//                transaction.replace(R.id.homeContainer, fragment);
//                transaction.commit();
            }
        });

        try{
            if(!sharedPref.getString("collectioncentername","default").equals("default")){
                dispatchprintcollectionname.setText(""+sharedPref.getString("collectioncentername","default"));
            }else{
                dispatchprintcollectionname.setText("Collection Center Name");
            }
        }catch (Exception e){
            e.printStackTrace();
        }



    }



    private void CopyReadPDFFromAssets(Context ctx)
    {
        AssetManager assetManager = ctx.getAssets();

        InputStream in = null;
        OutputStream out = null;
        File file = new File(ctx.getFilesDir(), "tocc.pdf");
        try
        {
            in = assetManager.open("tocc.pdf");
            out = ctx.openFileOutput(file.getName(), Context.MODE_WORLD_READABLE);

            copyPdfFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e)
        {
            Log.e("exception", e.getMessage());
        }

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(
                Uri.parse("file://" + ctx.getFilesDir() + "/tocc.pdf"),
                "application/pdf");

        startActivity(intent);
    }

    private void copyPdfFile(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1)
        {
            out.write(buffer, 0, read);
        }
    }




    public void downloadPDF(int store)
    {



    }

    public void viewPDF()
    {

        indicator.setVisibility(View.GONE);
        try{
            File pdfFile = new File(Environment.getExternalStorageDirectory() + "/FNV/" + sharedPref.getString("tonumber","default")+".pdf");  // -> filename = maven.pdf
            Uri path = FileProvider.getUriForFile(getActivity(), "fv.abrl.com.abrlav.provider", pdfFile);
            Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
            pdfIntent.setDataAndType(path, "application/pdf");
            pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            pdfIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(pdfIntent);
        }catch(ActivityNotFoundException e){
            Toast.makeText(getActivity(), "No Application available to view PDF", Toast.LENGTH_SHORT).show();
        }
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "FNV");
            folder.mkdir();

            File pdfFile = new File(folder, sharedPref.getString("tonumber","default")+".pdf");

            try{
                pdfFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);
            return null;

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getActivity(), "Download PDf successfully", Toast.LENGTH_SHORT).show();
                    viewPDF();
                }
            }, 5000);




            Log.d("Download complete", "----------");
        }
    }


    public void showPrintList(Activity activity,ArrayList<ToNumberListParcelable> lolo){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.showprintlist);

        RecyclerView printselect = (RecyclerView) dialog.findViewById(R.id.printselect);




        DispatchPrintScreenAdapter mAdapter                =       new DispatchPrintScreenAdapter(getActivity(), lolo);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        printselect.setLayoutManager(mLayoutManager);
        printselect.setItemAnimator(new DefaultItemAnimator());
        printselect.setAdapter(mAdapter);

        dialog.show();



        printselect.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {


                        try{
                            generateToPdf(DispatchPrintScreenAdapter.filteredarraylist.get(position).getStore());
                            editor.putString("tonumber",""+DispatchPrintScreenAdapter.filteredarraylist.get(position).getToNumber()).apply();
                            dialog.cancel();

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                })
        );


    }


    private void generateToPdf(int store) {

        indicator.setVisibility(View.VISIBLE);


        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);





        api.generateToPDF(store,Integer.parseInt(sharedPref.getString("collectioncentercode","default")),
                new Callback<PrintToParcelable>()

        {
            @Override
            public void success(PrintToParcelable getccParacables, Response response) {
                Toast.makeText(getActivity(),"success",Toast.LENGTH_SHORT).show();

                new DownloadFile().execute((ApiConstants.pdfurl+"/"+getccParacables.get$values().get(0).getURL()).replace(" ","%20"));

            }

            @Override
            public void failure(RetrofitError error) {
                indicator.setVisibility(View.GONE);
                Toast.makeText(getActivity(),"failed"+error.getMessage(),Toast.LENGTH_SHORT).show();



            }
        });

    }

}


