
package fv.abrl.com.abrlav.view.mpp.parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MPPViewDataParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<MPPViewDataItemParcelable> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<MPPViewDataItemParcelable> get$values() {
        return $values;
    }

    public void set$values(List<MPPViewDataItemParcelable> $values) {
        this.$values = $values;
    }

}
