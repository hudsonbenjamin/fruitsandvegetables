
package fv.abrl.com.abrlav.dispatch.paracelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ToNumberListParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("generateto")
    @Expose
    private Object generateto;
    @SerializedName("To_Number")
    @Expose
    private Integer toNumber;
    @SerializedName("STORE_NAME")
    @Expose
    private String sTORENAME;

    public Integer getStore() {
        return store;
    }

    public void setStore(Integer store) {
        this.store = store;
    }

    @SerializedName("Store")
    @Expose
    private Integer store;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public Object getGenerateto() {
        return generateto;
    }

    public void setGenerateto(Object generateto) {
        this.generateto = generateto;
    }

    public Integer getToNumber() {
        return toNumber;
    }

    public void setToNumber(Integer toNumber) {
        this.toNumber = toNumber;
    }

    public String getSTORENAME() {
        return sTORENAME;
    }

    public void setSTORENAME(String sTORENAME) {
        this.sTORENAME = sTORENAME;
    }

}
