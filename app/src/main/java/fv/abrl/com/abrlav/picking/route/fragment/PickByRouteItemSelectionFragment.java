package fv.abrl.com.abrlav.picking.route.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.utils.CustomLinearLayoutManager;
import fv.abrl.com.abrlav.common.utils.RecyclerItemClickListener;
import fv.abrl.com.abrlav.grn.adapter.VendorNameListAdapter;
import fv.abrl.com.abrlav.grn.fragment.GrnItemSelectionFragment;
import fv.abrl.com.abrlav.grn.fragment.GrnVendorSelectionFragment;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.home.fragment.HomeFragment;
import fv.abrl.com.abrlav.mpp.adapter.MPPItemListAdapter;
import fv.abrl.com.abrlav.mpp.paracable.MPPApprovalItemDataParcelable;
import fv.abrl.com.abrlav.picking.fragment.PickingMenuFragment;
import fv.abrl.com.abrlav.picking.route.adapter.GetStoreByItemMultiLevelAdapter;
import fv.abrl.com.abrlav.picking.route.adapter.ProductItemListbySelectAdapter;
import fv.abrl.com.abrlav.picking.route.parcelable.PickByRouteSubmitResultParcelable;
import fv.abrl.com.abrlav.picking.route.parcelable.PickingRouteDataParcelable;
import fv.abrl.com.abrlav.picking.route.parcelable.PickingRouteItemDataParcelable;
import fv.abrl.com.abrlav.picking.route.parcelable.PickingRouteProductItemParcelable;
import fv.abrl.com.abrlav.picking.store.fragment.PickbyStoreFragmentSecond;
import fv.abrl.com.abrlav.picking.store.paracelable.PickByStoreresult;
import fv.abrl.com.abrlav.picking.store.paracelable.PickbyStoreSubmitParacelable;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreDataParacable;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreListParacelable;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static fv.abrl.com.abrlav.R.color.colorPrimary;
import static fv.abrl.com.abrlav.R.color.greycolor;

/**
 * Created by Hudson on 7/14/2018.
 */

public class PickByRouteItemSelectionFragment extends Fragment {



    @BindView(R.id.pickbyrouteitemselectionSubmit)
    TextView pickbyrouteitemselectionSubmit;

    @BindView(R.id.SelectedRoute)
    TextView SelectedRoute;

    @BindView(R.id.productItemSearch)
    EditText productItemSearch;

    @BindView(R.id.selectItemHeader)
    TextView selectItemHeader;

    @BindView(R.id.selectItemComplete)
    TextView selectItemComplete;

    @BindView(R.id.routeItemCollectionCenter)
    TextView routeItemCollectionCenter;

    @BindView(R.id.routeItemCancel)
    TextView routeItemCancel;
    
    @BindView(R.id.pickbyrouteItemlist)
    IndexFastScrollRecyclerView pickbyrouteItemlist;

    @BindView(R.id.clearSearchText)
    ImageView clearSearchText;
    
    private Unbinder unbinder;
    Fragment fragment;

    @BindView(R.id.indicator)
    AVLoadingIndicatorView indicator;

    RecyclerView.LayoutManager mLayoutManager;

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    ProductItemListbySelectAdapter mAdapter;

   public static ArrayList<PickingRouteProductItemParcelable> arraylist = new ArrayList<PickingRouteProductItemParcelable>();
   public static ArrayList<Integer> itemarraycode = new ArrayList<Integer>();

    ArrayList<PickingStoreListParacelable> innerlist = new ArrayList<PickingStoreListParacelable>();
    ArrayList<PickingStoreListParacelable> inneraddlisttowholelist = new ArrayList<PickingStoreListParacelable>();
    ArrayList<PickingStoreListParacelable> serverhitarraylist = new ArrayList<PickingStoreListParacelable>();
    ArrayList<PickingStoreListParacelable> secondaryviewarraylist = new ArrayList<PickingStoreListParacelable>();

    public PickByRouteItemSelectionFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        sharedPref    = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor        = sharedPref.edit();

            try{
        ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.bottombar)));
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "Pick By Route" + "</font>"));

        HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
        if (Build.VERSION.SDK_INT >= 21) {
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
        }
            }
        catch (Exception e){
                e.printStackTrace();
            }

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pickbyrouteitemselection, container, false);
        unbinder = ButterKnife.bind(this, view);

        try{
            SelectedRoute.setText(""+sharedPref.getString("RouteName","default"));
            getProductListbyRoute(Integer.parseInt(sharedPref.getString("RouteCode","default")));
        }catch (Exception e){

        }

        pickbyrouteItemlist     = (IndexFastScrollRecyclerView) view.findViewById(R.id.pickbyrouteItemlist);
        mAdapter                =   new ProductItemListbySelectAdapter(getActivity(), arraylist);
        mLayoutManager          =   new LinearLayoutManager (getActivity());
        pickbyrouteItemlist.setLayoutManager(mLayoutManager);
        pickbyrouteItemlist.setItemAnimator(new DefaultItemAnimator());
        pickbyrouteItemlist.setItemViewCacheSize(arraylist.size());
        pickbyrouteItemlist.setIndexTextSize(12);
        pickbyrouteItemlist.setIndexBarColor("#ffffff");
        pickbyrouteItemlist.setIndexBarCornerRadius(0);
        pickbyrouteItemlist.setIndexbarMargin(0);
        pickbyrouteItemlist.setIndexbarWidth(40);
        pickbyrouteItemlist.setPreviewPadding(0);
        pickbyrouteItemlist.setIndexBarTextColor("#818181");
        pickbyrouteItemlist.setIndexBarVisibility(true);
        pickbyrouteItemlist.setIndexbarHighLateTextColor("#ed6c09");
        pickbyrouteItemlist.setIndexBarHighLateTextVisibility(true);
        pickbyrouteItemlist.setAdapter(mAdapter);



        try{
            routeItemCollectionCenter.setText(""+sharedPref.getString("collectioncentername","default"));
        }catch (Exception e){
            routeItemCollectionCenter.setText("Collection Center Name");
        }

        
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pickbyrouteitemselectionSubmit.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {

               if(ProductItemListbySelectAdapter.loadWholeinnerlist.size()>0) {
                   showDialog(getActivity(),"Do you wish to continue?");
                }
                else{
                   Toast.makeText(getActivity(),"Please select any one Item",Toast.LENGTH_SHORT).show();
               }

            }
        });
        productItemSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try{

                    mAdapter.getFilter().filter(s);

                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                try{

                    mAdapter.getFilter().filter(s);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        clearSearchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                productItemSearch.setText("");
            }
        });

        routeItemCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new PickingMenuFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer,fragment);
                transaction.commit();
            }
        });

        pickbyrouteItemlist.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {

                        innerlist.clear();
                        secondaryviewarraylist.clear();
                        if(GetStoreByItemMultiLevelAdapter.itemCodeList.contains(ProductItemListbySelectAdapter.filteredarraylist.get(position).getITEM())){

                            for(int i=0;i<ProductItemListbySelectAdapter.loadWholeinnerlist.size();i++){

                                if(ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getiTEM()==ProductItemListbySelectAdapter.filteredarraylist.get(position).getITEM()){
                                    PickingStoreListParacelable s = new PickingStoreListParacelable(
                                            ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getSTORENAME(),
                                            ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getSTORE(),
                                            ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getCheckeditem(),
                                            ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getStoreQunantity(),
                                            ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getItemdesc(),
                                            ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getiTEM(),
                                            ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getCc(),
                                            ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getCcode(),
                                            ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getStatus());
                                    secondaryviewarraylist.add(s);
                                }
                            }

                            popSecondView(getActivity(),position,secondaryviewarraylist);

                      }else{
                            getStoreListbyProduct(ProductItemListbySelectAdapter.filteredarraylist.get(position).getITEM(),position,ProductItemListbySelectAdapter.filteredarraylist.get(position).getITEMDESC());

                        }

                        mAdapter.notifyDataSetChanged();

                    }
                })
        );
    }

    private void getProductListbyRoute(int routecode) {


        indicator.setVisibility(View.VISIBLE);
        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getItemByRoute(routecode,new Callback<PickingRouteItemDataParcelable>()

        {
            @Override
            public void success(PickingRouteItemDataParcelable getccParacables, Response response) {

                try{
                    if(0 != getccParacables.get$values().size()){
                        arraylist.addAll(getccParacables.get$values());
                        itemarraycode.clear();
                        for(int i=0;i<arraylist.size();i++){
                            itemarraycode.add(arraylist.get(i).getITEM());
                        }
                        mAdapter.notifyDataSetChanged();



                    }else{

                        Toast.makeText(getActivity(),"No Item Found",Toast.LENGTH_SHORT).show();
                    }
                    indicator.setVisibility(View.GONE);
                }catch (Exception e){
                    e.printStackTrace();

                }







            }

            @Override
            public void failure(RetrofitError error) {

                indicator.setVisibility(View.GONE);
                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }

    private void submitPickbyRoute(String data) {



        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.pickbyrouteSubmit(data,new Callback<PickByRouteSubmitResultParcelable>()

        {
            @Override
            public void success(PickByRouteSubmitResultParcelable getccParacables, Response response) {

                try{
                    if(getccParacables.get$values().get(0).getSubmitpickbyroute().equals("true")){
                        ProductItemListbySelectAdapter.loadWholeinnerlist.clear();
                        GetStoreByItemMultiLevelAdapter.itemCodeList.clear();
                        Toast.makeText(getActivity(),"Item has been submitted Successfully",Toast.LENGTH_SHORT).show();
                        fragment = new HomeFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.homeContainer,fragment);
                        transaction.commit();
                    }
                    else{

                        Toast.makeText(getActivity(),"No Item Found",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();

                }

            }

            @Override
            public void failure(RetrofitError error) {


                Log.e("server","---"+error.getMessage());



            }
        });

    }
    public void showDialog(Activity activity, String msg){


        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_customsubmit);

        TextView text = (TextView) dialog.findViewById(R.id.customDialogMessage);
        text.setText(msg);

        TextView customSubmit = (TextView) dialog.findViewById(R.id.customSubmit);
        ImageView closeDialog = (ImageView) dialog.findViewById(R.id.closeDialog);
        TextView customDraft = (TextView) dialog.findViewById(R.id.customDraft);

        customDraft.setText("Cancel");
        customSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                try{
                    serverhitarraylist.clear();

                    for(int i =0;i<ProductItemListbySelectAdapter.loadWholeinnerlist.size();i++){

                        if(ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getStoreQunantity()!=0){


                            PickingStoreListParacelable s = new PickingStoreListParacelable(
                                    ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getSTORENAME(),
                                    ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getSTORE(),
                                    ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getCheckeditem(),
                                    ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getStoreQunantity(),
                                    ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getItemdesc(),
                                    ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getiTEM(),
                                    ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getCc(),
                                    ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getCcode(),
                                    ProductItemListbySelectAdapter.loadWholeinnerlist.get(i).getStatus());
                            serverhitarraylist.add(s);
                       }

                    }


                    Gson gson                =  new GsonBuilder().create();
                    JsonArray myCustomArray  =  gson.toJsonTree(serverhitarraylist).getAsJsonArray();
                    submitPickbyRoute(myCustomArray.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        customDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();

    }

    public void popupview(Activity activity, final int position){

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_eachitem);

        RecyclerView recyclerView                           =       (RecyclerView) dialog.findViewById(R.id.databindeachitem);
        TextView customSubmit                               =       (TextView) dialog.findViewById(R.id.customSubmit);
        TextView dataHeader                                 =       (TextView) dialog.findViewById(R.id.dataHeader);

        dataHeader.setText(ProductItemListbySelectAdapter.filteredarraylist.get(position).getITEMDESC());

        GetStoreByItemMultiLevelAdapter madapter            =       new GetStoreByItemMultiLevelAdapter(getActivity(), innerlist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setItemViewCacheSize(innerlist.size());
        recyclerView.setAdapter(madapter);
        madapter.notifyDataSetChanged();
        dialog.show();

        customSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();

                ArrayList<Integer> checkcount = new ArrayList<Integer>();
                inneraddlisttowholelist.clear();
                for(int i=0;i<GetStoreByItemMultiLevelAdapter.filteredarraylist.size();i++){

//                    if(GetStoreByItemMultiLevelAdapter.filteredarraylist.get(i).getStoreQunantity()!=0){
                        PickingStoreListParacelable s = new PickingStoreListParacelable(
                                GetStoreByItemMultiLevelAdapter.filteredarraylist.get(i).getSTORENAME(),
                                GetStoreByItemMultiLevelAdapter.filteredarraylist.get(i).getSTORE(),
                                GetStoreByItemMultiLevelAdapter.filteredarraylist.get(i).getCheckeditem(),
                                GetStoreByItemMultiLevelAdapter.filteredarraylist.get(i).getStoreQunantity(),
                                GetStoreByItemMultiLevelAdapter.filteredarraylist.get(i).getItemdesc(),
                                GetStoreByItemMultiLevelAdapter.filteredarraylist.get(i).getiTEM(),
                                GetStoreByItemMultiLevelAdapter.filteredarraylist.get(i).getCc(),
                                GetStoreByItemMultiLevelAdapter.filteredarraylist.get(i).getCcode(),
                                GetStoreByItemMultiLevelAdapter.filteredarraylist.get(i).getStatus());
                        inneraddlisttowholelist.add(s);

                    if(GetStoreByItemMultiLevelAdapter.filteredarraylist.get(i).getStoreQunantity()!=0){

                        checkcount.add(GetStoreByItemMultiLevelAdapter.filteredarraylist.get(i).getiTEM());

                   }

                }

                if(checkcount.size()>0){
                    checkcount.clear();
                    ProductItemListbySelectAdapter.loadWholeinnerlist.addAll(inneraddlisttowholelist);
                    ProductItemListbySelectAdapter.filteredarraylist.get(position).setCheckedValue(1);
                    mAdapter.notifyDataSetChanged();

                }



            }
        });

    }

    private void getStoreListbyProduct(final int itemcode, final int position,final String desc) {

        indicator.setVisibility(View.VISIBLE);

        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.pickinggetStorelistbyItemSelect(itemcode,new Callback<PickingStoreDataParacable>()

        {
            @Override
            public void success(PickingStoreDataParacable getccParacables, Response response) {
                innerlist.clear();
                innerlist.addAll(getccParacables.get$values());

                for(int i =0;i<innerlist.size();i++){
                    innerlist.get(i).setiTEM(itemcode);
                    innerlist.get(i).setPosition(i);
                    innerlist.get(i).setItemdesc(desc);
                }
                popupview(getActivity(),position);
                indicator.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {

                indicator.setVisibility(View.GONE);
                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }


    public void popSecondView(Activity activity, final int position, final ArrayList<PickingStoreListParacelable> secondaryviewarraylist){



        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_eachitem);

        RecyclerView recyclerView                           =       (RecyclerView) dialog.findViewById(R.id.databindeachitem);
        TextView customSubmit                               =       (TextView) dialog.findViewById(R.id.customSubmit);
        TextView dataHeader                                 =       (TextView) dialog.findViewById(R.id.dataHeader);

        dataHeader.setText(ProductItemListbySelectAdapter.filteredarraylist.get(position).getITEMDESC());

        GetStoreByItemMultiLevelAdapter madapter            =       new GetStoreByItemMultiLevelAdapter(getActivity(), secondaryviewarraylist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setItemViewCacheSize(secondaryviewarraylist.size());
        recyclerView.setAdapter(madapter);
        madapter.notifyDataSetChanged();
        dialog.show();

        customSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


        ArrayList<Integer> checkcount = new ArrayList<Integer>();
                try{
                    for(int i=0;i<secondaryviewarraylist.size();i++){


                            if(secondaryviewarraylist.get(i).getSTORE()== ProductItemListbySelectAdapter.loadWholeinnerlist.get(position).getSTORE()){

                            ProductItemListbySelectAdapter.loadWholeinnerlist.get(position).setStoreQunantity(secondaryviewarraylist.get(i).getStoreQunantity());
                                if(secondaryviewarraylist.get(i).getStoreQunantity()!=0){

                                    checkcount.add(secondaryviewarraylist.get(i).getiTEM());

                                }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

        if(checkcount.size()>0){
            checkcount.clear();
            ProductItemListbySelectAdapter.filteredarraylist.get(position).setCheckedValue(1);
            mAdapter.notifyDataSetChanged();

        }


                dialog.cancel();

                }






        });

    }
}