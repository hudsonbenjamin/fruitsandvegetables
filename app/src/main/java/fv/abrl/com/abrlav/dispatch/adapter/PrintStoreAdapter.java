package fv.abrl.com.abrlav.dispatch.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.dispatch.paracelable.ToNumberListParcelable;
import fv.abrl.com.abrlav.grn.paracelable.$value;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreListParacelable;

/**
 * Created by Hudson on 7/17/2018.
 */

public class PrintStoreAdapter extends BaseAdapter {
    private Context mContext;
    ArrayList<ToNumberListParcelable> realarrayList;




    public PrintStoreAdapter(Context c, ArrayList<ToNumberListParcelable> storelist ) {
        mContext = c;
        this.realarrayList = storelist;



    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return realarrayList.size();
    }

    @Override
    public ToNumberListParcelable getItem(int position) {
        // TODO Auto-generated method stub
        return realarrayList.get(position);

    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub

        return position;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub


        Random rand = new Random();
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);
            grid = inflater.inflate(R.layout.adapter_printstoredata, null);


            final TextView product_name = (TextView)  grid.findViewById(R.id.storenameCode);
            final TextView storeToNumber = (TextView)  grid.findViewById(R.id.storeToNumber);

            try{
                //String id = String.format("%04d", rand.nextInt(10000));

                product_name.setText(""+realarrayList.get(position).getSTORENAME());
                storeToNumber.setText(""+realarrayList.get(position).getToNumber());

            }catch (Exception e){
                e.printStackTrace();
            }





        } else {
            grid = (View) convertView;
        }



        return grid;
    }



}
