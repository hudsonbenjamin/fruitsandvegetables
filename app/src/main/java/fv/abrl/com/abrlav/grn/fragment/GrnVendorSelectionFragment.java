package fv.abrl.com.abrlav.grn.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.utils.CommonFunction;
import fv.abrl.com.abrlav.grn.adapter.CollectionCenterListAdapter;
import fv.abrl.com.abrlav.grn.paracelable.GetCollectionCenterDataParacelable;
import fv.abrl.com.abrlav.grn.paracelable.GetCollectionCenterListParcelable;
import fv.abrl.com.abrlav.grn.paracelable.VendorDataSubmitParacelable;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.home.fragment.HomeFragment;
import fv.abrl.com.abrlav.login.activity.LoginActivity;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/10/2018.
 */

public class GrnVendorSelectionFragment extends Fragment {

    @BindView(R.id.grnVendorSelectionSubmit)
     TextView grnVendorSelectionSubmit;

    @BindView(R.id.selectCollectionCenter)
    TextView selectCollectionCenter;

    @BindView(R.id.VendorCode)
    TextView VendorCode;

    @BindView(R.id.venderName)
    TextView venderName;

    @BindView(R.id.GRN_VendorSelectionCancel)
    TextView GRN_VendorSelectionCancel;

    @BindView(R.id.invoicenumber)
    EditText invoicenumber;

    @BindView(R.id.spinnerlist)
    Spinner spinnerlist;

    private Unbinder unbinder;
    private Fragment fragment;


    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    ArrayList<GetCollectionCenterListParcelable> collectioncenterlist = new ArrayList<GetCollectionCenterListParcelable>();




    public GrnVendorSelectionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        sharedPref    = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor        = sharedPref.edit();

        try{
            ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                    .getColor(R.color.bottombar)));

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "Vendor Selection" + "</font>"));
            HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
            if (Build.VERSION.SDK_INT >= 21) {
                getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        View view =  inflater.inflate(R.layout.fragment_grn_vendor_selection, container, false);
        unbinder = ButterKnife.bind(this,view);

       try{
           if(sharedPref.getString("VendorName","default").equals("default")){
               venderName.setText("Vendor Name");
           }else{

               venderName.setText(""+sharedPref.getString("VendorName","default"));
           }
           if(sharedPref.getString("VendorCode","default").equals("default")){
               VendorCode.setText("Vendor Code");
           }else {
               VendorCode.setText(""+sharedPref.getString("VendorCode","default"));
           }

           if(sharedPref.getString("invoicenumber","default").equals("default")){
               invoicenumber.setText("");
           }else{
               invoicenumber.setText(""+sharedPref.getString("invoicenumber","default"));
           }
       }catch (Exception e){
           e.printStackTrace();
       }


       try{
           getCollectionCenterlist();
       }catch (Exception e){
           e.printStackTrace();
       }


        spinnerlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                editor.putString("collectioncentername",collectioncenterlist.get(position).getCCNAME()).apply();
                editor.putString("collectioncenterpostion",""+position).apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        try{
            if(!sharedPref.getString("collectioncenterpostion","default").equals("default")){
                spinnerlist.setSelection(Integer.parseInt(sharedPref.getString("collectioncenterpostion","default")));
            }else{

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        GRN_VendorSelectionCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new HomeFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer,fragment);
                transaction.commit();
            }
        });


        grnVendorSelectionSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if(!invoicenumber.getText().toString().isEmpty()&&!selectCollectionCenter.getText().toString().equals("Collection Center Name")&&!venderName.getText().toString().equals("Vendor Name")){


                    editor.putString("invoicenumber",""+invoicenumber.getText().toString()).apply();

                    if(CommonFunction.isNetworkAvailable(getActivity())) {

                        submitVendordetails(Integer.parseInt(sharedPref.getString("VendorCode", "default")),
                                sharedPref.getString("VendorName", "default").toString(),
                                invoicenumber.getText().toString());


                    }else{
                        Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                    }

                }else{
                    Toast.makeText(getActivity(),"Please enter Invoice number and select Vendor Name",Toast.LENGTH_SHORT).show();
                }



            }
        });

        selectCollectionCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final Dialog dialog = new Dialog(getActivity());
                // Include dialog.xml file
                dialog.setContentView(R.layout.dialog);
                // Set dialog title
                // set values for custom dialog components - text, image and button
                final ListView lv       =  (ListView) dialog.findViewById(R.id.CollectionCenterList);
                lv.setAdapter(new CollectionCenterListAdapter(getActivity(), collectioncenterlist));


                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        try{


                            editor.putString("collectioncentername",""+collectioncenterlist.get(position).getCCNAME()).apply();
                            selectCollectionCenter.setText(""+collectioncenterlist.get(position).getCCNAME());
                            dialog.dismiss();

                        }catch (Exception e){
                            e.printStackTrace();
                        }




                    }
                });


                dialog.show();






            }
        });



        venderName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new GrnVendorNameSelectionFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer,fragment);
                transaction.commit();


            }
        });





    }



    private void getCollectionCenterlist() {



        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getCollectionList(new Callback<GetCollectionCenterDataParacelable>()

        {
            @Override
            public void success(GetCollectionCenterDataParacelable getccParacables, Response response) {




                collectioncenterlist.addAll(getccParacables.get$values());


                spinnerlist.setAdapter(new CollectionCenterListAdapter(getActivity(), collectioncenterlist));

                try{
                    if(!sharedPref.getString("collectioncenterpostion","default").equals("default")){
                        spinnerlist.setSelection(Integer.parseInt(sharedPref.getString("collectioncenterpostion","default")));
                        editor.putString("collectioncentercode",""+collectioncenterlist.get(0).getCC()).apply();
                    }else{

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }


            }

            @Override
            public void failure(RetrofitError error) {


                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }
    private void submitVendordetails(int suppliercode,String name,String invoice) {



        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.submitVendorDetails(sharedPref.getString("collectioncentercode","default"),suppliercode,name,invoice,new Callback<VendorDataSubmitParacelable>()

        {
            @Override
            public void success(VendorDataSubmitParacelable getccParacables, Response response) {


                try{
                    if(getccParacables.get$values().get(0).getAssigninvoicegrnresult().equals("true")){

                        editor.putString("grnnumber",getccParacables.get$values().get(0).getGrnnumber()).apply();
                        fragment = new GrnItemSelectionFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.homeContainer,fragment,"GrnItemSelectionFragment");
                        transaction.commit();

                    }else{
                        Toast.makeText(getActivity(),"Failed to Submit",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }


            }

            @Override
            public void failure(RetrofitError error) {
                try{
                    Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                }






            }
        });

    }


}