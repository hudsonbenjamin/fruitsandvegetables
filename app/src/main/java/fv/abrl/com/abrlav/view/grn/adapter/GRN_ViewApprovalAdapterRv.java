package fv.abrl.com.abrlav.view.grn.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.grn.paracelable.GrnItemParacelable;
import fv.abrl.com.abrlav.view.grn.parcelable.GRNViewListPendingApprovalParcelable;

public class GRN_ViewApprovalAdapterRv extends RecyclerView.Adapter<GRN_ViewApprovalAdapterRv.MyViewHolder>
         {
    private Context mContext;

    ArrayList<GRNViewListPendingApprovalParcelable> arrayList;
    ArrayList<GRNViewListPendingApprovalParcelable> filteredarraylist;
    private ArrayList<Integer> mSectionPositions;

    private GRNViewListPendingApprovalParcelable getItem(int position) {
        return filteredarraylist.get(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CheckBox finalentryCheckBox;
        TextView grnitemViewExpand;
        TextView itemCode;
        LinearLayout grnitemViewDetails;
        TextView grnmarketprice,grnmpp,grn_cost;


        public MyViewHolder(View grid) {
            super(grid);

            finalentryCheckBox       = (CheckBox) grid.findViewById(R.id.grnitemselectionCB);
            grnitemViewExpand        = (TextView)  grid.findViewById(R.id.grnitemViewExpand);
            itemCode                 = (TextView)  grid.findViewById(R.id.itemCode);
            grnmarketprice           = (TextView)  grid.findViewById(R.id.grnmarketprice);
            grnitemViewDetails       = (LinearLayout) grid.findViewById(R.id.grnitemViewDetails);
            grnmpp                   = (TextView) grid.findViewById(R.id.grnmpp);
            grn_cost                 = (TextView) grid.findViewById(R.id.grn_cost);


        }

    }


    public GRN_ViewApprovalAdapterRv(Context c, ArrayList<GRNViewListPendingApprovalParcelable> arrayList ) {
        mContext = c;
        this.arrayList = arrayList;
        this.filteredarraylist = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_grn_viewapproval, parent, false);



        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.grnitemViewExpand.setVisibility(View.INVISIBLE);
        holder.grnitemViewDetails.setVisibility(View.VISIBLE);
        holder.finalentryCheckBox.setText(""+filteredarraylist.get(position).getITEMDESC()+" ("+filteredarraylist.get(position).getSTANDARDUOM()+")");
        holder.grnmpp.setText(""+filteredarraylist.get(position).getMPP());
        holder.grnmarketprice.setText(""+filteredarraylist.get(position).getMarketPrice());
        holder.grn_cost.setText(""+filteredarraylist.get(position).getGRNCost());


        holder.finalentryCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    if (holder.finalentryCheckBox.isChecked()) {
                        filteredarraylist.get(position).setItemchecked(1);
                        holder.finalentryCheckBox.setChecked(true);
                    } else {
                        filteredarraylist.get(position).setItemchecked(0);
                        holder.finalentryCheckBox.setChecked(false);

                    }




            }

        });




  
    }
    @Override
    public int getItemCount() {
        return filteredarraylist.size();
    }




    @Override
    public int getItemViewType(int position) {
        return position;
    }




    public long getItemId(int position) {
        return position;
    }



}