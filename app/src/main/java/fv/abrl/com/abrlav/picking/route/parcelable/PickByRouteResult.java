
package fv.abrl.com.abrlav.picking.route.parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickByRouteResult {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("submitpickbyroute")
    @Expose
    private String submitpickbyroute;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getSubmitpickbyroute() {
        return submitpickbyroute;
    }

    public void setSubmitpickbyroute(String submitpickbyroute) {
        this.submitpickbyroute = submitpickbyroute;
    }

}
