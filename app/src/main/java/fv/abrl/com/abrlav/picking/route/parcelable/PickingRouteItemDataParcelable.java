
package fv.abrl.com.abrlav.picking.route.parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickingRouteItemDataParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<PickingRouteProductItemParcelable> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<PickingRouteProductItemParcelable> get$values() {
        return $values;
    }

    public void set$values(List<PickingRouteProductItemParcelable> $values) {
        this.$values = $values;
    }

}
