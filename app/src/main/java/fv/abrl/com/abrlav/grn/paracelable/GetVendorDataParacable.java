
package fv.abrl.com.abrlav.grn.paracelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;




public class GetVendorDataParacable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<$value> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<$value> get$values() {
        return $values;
    }

    public void set$values(List<$value> $values) {
        this.$values = $values;
    }

}
