package fv.abrl.com.abrlav.view.grn.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.grn.adapter.VendorNameListAdapter;
import fv.abrl.com.abrlav.grn.paracelable.GrnItemParacelable;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.picking.route.parcelable.PickByRouteSubmitResultParcelable;
import fv.abrl.com.abrlav.view.grn.adapter.GRN_ViewApprovalAdapterRv;
import fv.abrl.com.abrlav.view.grn.parcelable.GRNViewListPendingApprovalParcelable;
import fv.abrl.com.abrlav.view.grn.parcelable.GRNViewPendingApprovalParcelable;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GRN_ApprovalFragment extends Fragment {






    private Fragment fragment;
    private Unbinder unbinder;
    private View view;

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    ArrayList<GRNViewListPendingApprovalParcelable> pendingapprovallist = new ArrayList<GRNViewListPendingApprovalParcelable>();
    ArrayList<GRNViewListPendingApprovalParcelable> sendarraylist = new ArrayList<GRNViewListPendingApprovalParcelable>();

    public GRN_ApprovalFragment() {

        // Required empty public constructor

    }

    GRN_ViewApprovalAdapterRv mAdapter;

    @BindView(R.id.grnapprovallist)
    RecyclerView grnapprovallist;

    @BindView(R.id.grnviewPendingSubmit)
    TextView grnviewPendingSubmit;

    @BindView(R.id.mppfinalfragmentReject)
    TextView mppfinalfragmentReject;

    @BindView(R.id.approvalCancel)
    TextView approvalCancel;


    @BindView(R.id.indicator)
    AVLoadingIndicatorView indicator;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view                    =  inflater.inflate(R.layout.fragment_grn_approval_pendingitems, container, false);
        unbinder                =  ButterKnife.bind(this,view);

        sharedPref               = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                   = sharedPref.edit();



        try{
            ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                    .getColor(R.color.bottombar)));

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "GRN Approval" + "</font>"));

            HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);

            if (Build.VERSION.SDK_INT >= 21) {
                getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
            }


        }catch (Exception e){
            e.printStackTrace();
        }


        try{
            getGRNPendingList(sharedPref.getInt("passgrn_number",0));
        }catch (Exception e){
            e.printStackTrace();
        }


        mAdapter                =       new GRN_ViewApprovalAdapterRv(getActivity(), pendingapprovallist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        grnapprovallist.setLayoutManager(mLayoutManager);
        grnapprovallist.setItemAnimator(new DefaultItemAnimator());
        grnapprovallist.setAdapter(mAdapter);




        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        grnviewPendingSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("datasize","--"+pendingapprovallist.size());
                sendarraylist.clear();
                for(int i=0;i<pendingapprovallist.size();i++){

                    if(pendingapprovallist.get(i).getItemchecked()==1)
                    {
                        Log.e("getdata","--"+pendingapprovallist.get(i).getITEMDESC());

                      GRNViewListPendingApprovalParcelable s1 = new


                              GRNViewListPendingApprovalParcelable(
                              pendingapprovallist.get(i).getITEMDESC(),
                              pendingapprovallist.get(i).getITEM(),
                              pendingapprovallist.get(i).getCCCode(),
                              sharedPref.getInt("passgrn_number",0),
                              "Approved");
                               sendarraylist.add(s1);
                    }

                }
                Gson gson = new GsonBuilder().create();
                JsonArray myCustomArray = gson.toJsonTree(sendarraylist).getAsJsonArray();
                sendingapprovallist(myCustomArray.toString());


            }
        });

        approvalCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new GRN_ViewFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer,fragment);
                transaction.commit();
            }
        });


        mppfinalfragmentReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("datasize","--"+pendingapprovallist.size());
                sendarraylist.clear();
                for(int i=0;i<pendingapprovallist.size();i++){

                    if(pendingapprovallist.get(i).getItemchecked()==1)
                    {
                        Log.e("getdata","--"+pendingapprovallist.get(i).getITEMDESC());

                        GRNViewListPendingApprovalParcelable s1 = new


                                GRNViewListPendingApprovalParcelable(
                                pendingapprovallist.get(i).getITEMDESC(),
                                pendingapprovallist.get(i).getITEM(),
                                pendingapprovallist.get(i).getCCCode(),
                                sharedPref.getInt("passgrn_number",0),
                                "Rejected");
                        sendarraylist.add(s1);
                    }

                }
                Gson gson = new GsonBuilder().create();
                JsonArray myCustomArray = gson.toJsonTree(sendarraylist).getAsJsonArray();
                sendingapprovallist(myCustomArray.toString());


            }
        });

    }


    private void getGRNPendingList(int code) {

        indicator.setVisibility(View.VISIBLE);
        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);
        api.getGRNApprovalItem(code,new Callback<GRNViewPendingApprovalParcelable>()

        {
            @Override
            public void success(GRNViewPendingApprovalParcelable getccParacables, Response response) {

                try{
                    if(0 != getccParacables.get$values().size()){

                        pendingapprovallist.addAll(getccParacables.get$values());


                        mAdapter.notifyDataSetChanged();


                    }else{

                        Toast.makeText(getActivity(),"No Item Found",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();

                }
                indicator.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {

                indicator.setVisibility(View.GONE);
                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }

    private void sendingapprovallist(String result) {

        indicator.setVisibility(View.VISIBLE);
        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);
        api.sendgrnapproval(result,new Callback<GRNViewPendingApprovalParcelable>()

        {
            @Override
            public void success(GRNViewPendingApprovalParcelable getccParacables, Response response) {

                Log.e("response","--"+getccParacables.get$values());
                pendingapprovallist.clear();
                pendingapprovallist.addAll(getccParacables.get$values());
                mAdapter.notifyDataSetChanged();
                indicator.setVisibility(View.GONE);

                if(pendingapprovallist.size()==0){
                    fragment = new GRN_ViewFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.homeContainer,fragment);
                    transaction.commit();
                }

            }

            @Override
            public void failure(RetrofitError error) {

                indicator.setVisibility(View.GONE);
                Log.e("server","---"+error.getMessage());
            }
        });

    }

}