
package fv.abrl.com.abrlav.picking.store.paracelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import fv.abrl.com.abrlav.grn.paracelable.$value;

public class PickByStoreCompleteDataParacelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<PickByStoreCompleteItemParacelable> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<PickByStoreCompleteItemParacelable> get$values() {
        return $values;
    }

    public void set$values(List<PickByStoreCompleteItemParacelable> $values) {
        this.$values = $values;
    }

}
