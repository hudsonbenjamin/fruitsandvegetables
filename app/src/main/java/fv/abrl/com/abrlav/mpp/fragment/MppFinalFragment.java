package fv.abrl.com.abrlav.mpp.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.home.fragment.HomeFragment;
import fv.abrl.com.abrlav.mpp.adapter.MppFinalMppAdapterRv;
import fv.abrl.com.abrlav.mpp.paracable.MPPEntrySubmitItemParcelable;
import fv.abrl.com.abrlav.mpp.paracable.MPPFinalSubmitResponseDataParcelable;
import fv.abrl.com.abrlav.mpp.paracable.MPPItemListParacelable;
import fv.abrl.com.abrlav.notification.fragment.NotificationFragment;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/9/2018.
 */

public class MppFinalFragment extends Fragment {


    @BindView(R.id.mppfinalfragmentSubmit)
    TextView mppFinalSubmit;

    @BindView(R.id.LineCount)
    TextView LineCount;

    @BindView(R.id.finalmppcancel)
    TextView finalmppcancel;

    @BindView(R.id.selectCollectionCenter)
    TextView selectCollectionCenter;


    @BindView(R.id.mppfinalfragmentdataTab)
    IndexFastScrollRecyclerView mppfinalListView;

    private Unbinder unbinder;
    private Fragment fragment;

    @BindView(R.id.indicator)
    AVLoadingIndicatorView indicator;

    @BindView(R.id.finalmppfilter)
    EditText finalmppfilter;

    @BindView(R.id.clearSearchText)
    ImageView clearSearchText;

    MppFinalMppAdapterRv mAdapter;

    public static ArrayList<MPPEntrySubmitItemParcelable> addFinalVeglist  =   new ArrayList<MPPEntrySubmitItemParcelable>();
    public static ArrayList<MPPItemListParacelable> MppEntrycanceldatalist =   new ArrayList<MPPItemListParacelable>();


    RecyclerView.LayoutManager mLayoutManager;
    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    public MppFinalFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        sharedPref              = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                  = sharedPref.edit();
        MppEntrycanceldatalist  = MppEntryFragment.canceldatalist;
        View view               =  inflater.inflate(R.layout.fragment_mpp_final, container, false);
        unbinder                = ButterKnife.bind(this,view);

        try{

          ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                   .getColor(R.color.bottombar)));

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "Final MPP Entry" + "</font>"));
            HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
            if (Build.VERSION.SDK_INT >= 21) {
                getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
            }
             }catch (Exception e){
            e.printStackTrace();
        }

//        try{
//          //  LineCount.setText("Lines : "+MppEntryFragment.addVeglist.size());
//        }catch (Exception e){
//          e.printStackTrace();
//        }






        mAdapter                = new MppFinalMppAdapterRv(getActivity(), MppEntryFragment.dataarraylist);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mppfinalListView.setLayoutManager(mLayoutManager);
        mppfinalListView.setItemAnimator(new DefaultItemAnimator());
        mppfinalListView.setItemViewCacheSize(MppEntryFragment.dataarraylist.size());
        mppfinalListView.setIndexTextSize(12);
        mppfinalListView.setIndexBarColor("#ffffff");
        mppfinalListView.setIndexBarCornerRadius(0);
        mppfinalListView.setIndexbarMargin(0);
        mppfinalListView.setIndexbarWidth(40);
        mppfinalListView.setPreviewPadding(0);
        mppfinalListView.setIndexBarTextColor("#818181");
        mppfinalListView.setIndexBarVisibility(true);
        mppfinalListView.setIndexbarHighLateTextColor("#ed6c09");
        mppfinalListView.setIndexBarHighLateTextVisibility(true);
        mppfinalListView.setAdapter(mAdapter);


       // mppfinalListView.setAdapter(new MppFinalMppAdapter(getActivity(), MppEntryFragment.addVeglist));

        finalmppcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addFinalVeglist.clear();
                MppEntrycanceldatalist.clear();
                MppEntrycanceldatalist.clear();


                fragment = new HomeFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer,fragment);
                transaction.commit();
            }
        });


        try{
            if(!sharedPref.getString("collectioncentername","default").equals("default")){
                selectCollectionCenter.setText(""+sharedPref.getString("collectioncentername","default"));
            }else{
                selectCollectionCenter.setText("Collection Center Name");
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mppFinalSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addFinalVeglist.clear();
                for(int i=0;i < MppEntryFragment.dataarraylist.size();i++){


                        MPPEntrySubmitItemParcelable s1=new MPPEntrySubmitItemParcelable(
                                MppEntryFragment.dataarraylist.get(i).getITEM(),
                                MppEntryFragment.dataarraylist.get(i).getITEMDESC(),
                                MppEntryFragment.dataarraylist.get(i).getMarketprice(),
                                MppEntryFragment.dataarraylist.get(i).getCheckboxvalue(),
                                MppEntryFragment.dataarraylist.get(i).getFinalMPP(),
                                MppEntryFragment.dataarraylist.get(i).getMPP(),
                    MppEntryFragment.dataarraylist.get(i).getCCCode(), MppEntryFragment.dataarraylist.get(i).getMPPID());
                        addFinalVeglist.add(s1);


                }

               if(addFinalVeglist.size()>0){

                   showDialog(getActivity(),"Do you wish to continue");

              }

            }
        });

        finalmppfilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.getFilter().filter(s);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });






        clearSearchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finalmppfilter.setText("");
            }
        });





    }


    private void submitFinalMPP(String result) {


        indicator.setVisibility(View.VISIBLE);
        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);



        api.submitFinalMPP(result,new Callback<MPPFinalSubmitResponseDataParcelable>()

        {
            @Override
            public void success(MPPFinalSubmitResponseDataParcelable getparcelable, Response response) {
                indicator.setVisibility(View.GONE);

                if (getparcelable.get$values().get(0).getFinalmppsubmit().equals("true")){
                    Toast.makeText(getActivity(),"Final MPP submitted successfully",Toast.LENGTH_SHORT).show();

                    fragment = new HomeFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.homeContainer,fragment);

                    transaction.commit();
                }else{
                    try{
                        Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }



            }

            @Override
            public void failure(RetrofitError error) {
                indicator.setVisibility(View.GONE);
                try{
                    Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

    }

    public void showDialog(Activity activity, String msg){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_customsubmit);

        TextView text = (TextView) dialog.findViewById(R.id.customDialogMessage);
        text.setText(msg);

        TextView customSubmit = (TextView) dialog.findViewById(R.id.customSubmit);
        ImageView closeDialog = (ImageView) dialog.findViewById(R.id.closeDialog);
        TextView customDraft = (TextView) dialog.findViewById(R.id.customDraft);
        customDraft.setText("Save");
        customSubmit.setText("Submit");


        customSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Gson gson = new GsonBuilder().create();
                JsonArray myCustomArray = gson.toJsonTree(addFinalVeglist).getAsJsonArray();
                submitFinalMPP(myCustomArray.toString()) ;
            }
        });

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        customDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Gson gson = new GsonBuilder().create();
                JsonArray myCustomArray = gson.toJsonTree(addFinalVeglist).getAsJsonArray();
                DraftFinalMPP(myCustomArray.toString()) ;


            }
        });

        dialog.show();

    }

    private void DraftFinalMPP(String result) {


        indicator.setVisibility(View.VISIBLE);
        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);



        api.draftFinalMPP(result,new Callback<MPPFinalSubmitResponseDataParcelable>()

        {
            @Override
            public void success(MPPFinalSubmitResponseDataParcelable getparcelable, Response response) {
                indicator.setVisibility(View.GONE);

                if (getparcelable.get$values().get(0).getFinalmppdraft().equals("true")){
                    Toast.makeText(getActivity(),"Final MPP saved successfully",Toast.LENGTH_SHORT).show();

                    fragment = new HomeFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.homeContainer,fragment);

                    transaction.commit();
                }else{
                    try{
                        Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }



            }

            @Override
            public void failure(RetrofitError error) {
                indicator.setVisibility(View.GONE);
                try{
                    Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

    }
}