package fv.abrl.com.abrlav.mpp.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.home.fragment.HomeFragment;
import fv.abrl.com.abrlav.mpp.adapter.MPPApprovalAdapterRv;
import fv.abrl.com.abrlav.mpp.paracable.MPPApprovalItemDataParcelable;
import fv.abrl.com.abrlav.mpp.paracable.MPPApprovalItemListParcelable;
import fv.abrl.com.abrlav.mpp.paracable.MPPEntrySubmitItemParcelable;
import fv.abrl.com.abrlav.notification.fragment.NotificationFragment;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/9/2018.
 */

public class MppApprovalFragment extends Fragment {


    private Unbinder unbinder;
    private Fragment fragment;


    public static TextView lineCount;

    @BindView(R.id.mppfinalfragmentSubmit)
    TextView mppfinalfragmentSubmit;



    @BindView(R.id.mppfinalfragmentdataTab)
    IndexFastScrollRecyclerView recycler_view;

    @BindView(R.id.mppApprovalSelectAll)
    TextView mppApprovalSelectAll;

    @BindView(R.id.mppApprovalClearAll)
    TextView mppApprovalClearAll;

    @BindView(R.id.selectCollectionCenter)
    TextView selectCollectionCenter;

    @BindView(R.id.mppfinalfragmentReject)
    TextView mppfinalfragmentReject;

    @BindView(R.id.approvalFilter)
    EditText approvalFilter;

    @BindView(R.id.clearSearchText)
    ImageView clearSearchText;

    @BindView(R.id.approvalCancel)
    TextView approvalCancel;

    @BindView(R.id.indicator)
    AVLoadingIndicatorView indicator;

    @BindView(R.id.nodata)
    LinearLayout nodata;

    @BindView(R.id.mppfinalfragmentactionView)
    LinearLayout mppfinalfragmentactionView;

    private  ArrayList<MPPApprovalItemListParcelable> mppapprovallist        =  new ArrayList<MPPApprovalItemListParcelable>();
    private ArrayList<MPPApprovalItemListParcelable> newlistmppapprovallist  = new ArrayList<MPPApprovalItemListParcelable>();

    public static ArrayList<MPPEntrySubmitItemParcelable> cancelFinalList  =   new ArrayList<MPPEntrySubmitItemParcelable>();

    MPPApprovalAdapterRv mAdapter;

    public MppApprovalFragment() {
        // Required empty public constructor
    }

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    RecyclerView.LayoutManager mLayoutManager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        sharedPref          =           getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor              =           sharedPref.edit();

        cancelFinalList     = MppFinalFragment.addFinalVeglist;

            try{
                ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                        .getColor(R.color.bottombar)));
                ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "MPP Approval" + "</font>"));

                HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
                if (Build.VERSION.SDK_INT >= 21) {
                    getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
                }
            }catch (Exception e){
                e.printStackTrace();
            }


        // Inflate the layout for this fragment
        View view               =  inflater.inflate(R.layout.fragment_mpp_approval, container, false);
        unbinder                =  ButterKnife.bind(this,view);

        lineCount               = (TextView ) view.findViewById(R.id.lineCount);

        try{
            if(!sharedPref.getString("collectioncentername","default").equals("default")){
                selectCollectionCenter.setText(""+sharedPref.getString("collectioncentername","default"));
            }else{
                selectCollectionCenter.setText("Collection Center Name");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        recycler_view.setHasFixedSize(true);
        mAdapter                =   new MPPApprovalAdapterRv(getActivity(), mppapprovallist);

        mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);

        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.setIndexTextSize(12);
        recycler_view.setIndexBarColor("#ffffff");
        recycler_view.setIndexBarCornerRadius(0);
        recycler_view.setIndexbarMargin(0);
        recycler_view.setIndexbarWidth(40);
        recycler_view.setPreviewPadding(0);
        recycler_view.setIndexBarTextColor("#818181");
        recycler_view.setIndexBarVisibility(true);
        recycler_view.setIndexbarHighLateTextColor("#ed6c09");
        recycler_view.setIndexBarHighLateTextVisibility(true);
        recycler_view.setAdapter(mAdapter);

        try{
            getApprovalitemlist(getActivity());
        }catch (Exception e){
            e.printStackTrace();
        }



        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mppfinalfragmentSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newlistmppapprovallist.clear();
                for(int i=0;i <mppapprovallist.size();i++){


                    if(mppapprovallist.get(i).getCheckbox()== 1){
                        MPPApprovalItemListParcelable s1=new MPPApprovalItemListParcelable(
                                mppapprovallist.get(i).getCC(),
                                1,
                                mppapprovallist.get(i).getMPPID(),
                                mppapprovallist.get(i).getFinalMPP(),
                                mppapprovallist.get(i).getSTANDARDUOM(),
                                mppapprovallist.get(i).getITEM(),
                                mppapprovallist.get(i).getITEMDESC(),
                                mppapprovallist.get(i).getMarketprice(),
                                mppapprovallist.get(i).getCheckbox(),
                                mppapprovallist.get(i).getComments());
                        newlistmppapprovallist.add(s1);



                    }else{

                    }
//                    if (mppapprovallist.get(i).getCheckbox()==1){
//                        mppapprovallist.remove(i);
//                        mAdapter.notifyItemRemoved(i);
//                        mAdapter.notifyDataSetChanged();
//                    }

                }


                if(newlistmppapprovallist.size()>0){

                    showDialogapproval(getActivity(),"MPP Will get Approved, Do you Wish to continue ?");
                }else{
                    Toast.makeText(getActivity(),"Please select at least one item",Toast.LENGTH_SHORT).show();
                }

            }
        });




        mppfinalfragmentReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                newlistmppapprovallist.clear();
                for(int i=0;i <mppapprovallist.size();i++){
                    if(mppapprovallist.get(i).getCheckbox()== 1){
                        MPPApprovalItemListParcelable s1=new MPPApprovalItemListParcelable(
                                mppapprovallist.get(i).getCC(),
                                1,
                                mppapprovallist.get(i).getMPPID(),
                                mppapprovallist.get(i).getFinalMPP(),
                                mppapprovallist.get(i).getSTANDARDUOM(),
                                mppapprovallist.get(i).getITEM(),
                                mppapprovallist.get(i).getITEMDESC(),
                                mppapprovallist.get(i).getMarketprice(),
                                mppapprovallist.get(i).getCheckbox(),
                                mppapprovallist.get(i).getComments());
                        newlistmppapprovallist.add(s1);



                    }else{

                    }

                }


                if(newlistmppapprovallist.size()>0){

                    showDialogpreReject(getActivity(),"Are you sure wants to reject? ");


                }else{
                    Toast.makeText(getActivity(),"Please select at least one item",Toast.LENGTH_SHORT).show();
                }



            }
        });

        mppApprovalSelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{


                    for(int i=0;i <mppapprovallist.size();i++) {

                        mppapprovallist.get(i).setCheckbox(1);

                    }
                    lineCount.setText("Lines : "+mppapprovallist.size());
                  mAdapter.notifyDataSetChanged();
                }catch (Exception e){
                    mAdapter.notifyDataSetChanged();
                    e.printStackTrace();
                }
            }
        });

        mppApprovalClearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{

                    for(int i=0;i <mppapprovallist.size();i++) {

                        mppapprovallist.get(i).setCheckbox(0);

                    }

                    lineCount.setText("Lines : 0 ");
                    mAdapter.notifyDataSetChanged();
                }catch (Exception e){
                    mAdapter.notifyDataSetChanged();
                    e.printStackTrace();
                }
            }
        });

        approvalFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try{
                    mAdapter.getFilter().filter(s);

                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                try{
                    mAdapter.getFilter().filter(s);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        clearSearchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                approvalFilter.setText("");
            }
        });

        approvalCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new HomeFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer,fragment);
                transaction.commit();
            }
        });








    }

    private void getApprovalitemlist(final Activity activity) {



        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getmppapprovalitem(sharedPref.getString("collectioncentercode","default"),"1",new Callback<MPPApprovalItemDataParcelable>()

        {
            @Override
            public void success(MPPApprovalItemDataParcelable getparcelable, Response response) {
               if(0 != getparcelable.get$values().size()||!getparcelable.get$values().isEmpty()) {
                   mppapprovallist.addAll(getparcelable.get$values());
                   mAdapter.notifyDataSetChanged();
                }else{

                   recycler_view.setVisibility(View.GONE);
                   nodata.setVisibility(View.VISIBLE);
                   mppfinalfragmentactionView.setVisibility(View.INVISIBLE);
                   Toast.makeText(activity,"No Item Found",Toast.LENGTH_SHORT).show();
               }


            }

            @Override
            public void failure(RetrofitError error) {

                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }

    public void showDialogapproval(Activity activity, String msg){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_customsubmit);

        TextView text = (TextView) dialog.findViewById(R.id.customDialogMessage);
        text.setText(msg);

        TextView customSubmit = (TextView) dialog.findViewById(R.id.customSubmit);
        ImageView closeDialog = (ImageView) dialog.findViewById(R.id.closeDialog);
        TextView customDraft = (TextView) dialog.findViewById(R.id.customDraft);
        customDraft.setText("No");
        customSubmit.setText("Yes");


        customSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try{



                    Gson gson = new GsonBuilder().create();
                    JsonArray myCustomArray = gson.toJsonTree(newlistmppapprovallist).getAsJsonArray();
                    submitforHodApproval(myCustomArray.toString()) ;
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        customDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();


            }
        });

        dialog.show();

    }

    public void showDialogRejection(Activity activity, String msg){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_mpp_rejection);


        TextView customSubmit = (TextView) dialog.findViewById(R.id.Submit);
        ImageView closeDialog = (ImageView) dialog.findViewById(R.id.closeDialog);
        final EditText customDialogMessage = (EditText) dialog.findViewById(R.id.customDialogMessage);
        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });


        customSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                newlistmppapprovallist.clear();
                for(int i=0;i <mppapprovallist.size();i++){
                    if(mppapprovallist.get(i).getCheckbox()== 1){
                        MPPApprovalItemListParcelable s1=new MPPApprovalItemListParcelable(
                                mppapprovallist.get(i).getCC(),
                                1,
                                mppapprovallist.get(i).getMPPID(),
                                mppapprovallist.get(i).getFinalMPP(),
                                mppapprovallist.get(i).getSTANDARDUOM(),
                                mppapprovallist.get(i).getITEM(),
                                mppapprovallist.get(i).getITEMDESC(),
                                mppapprovallist.get(i).getMarketprice(),mppapprovallist.get(i).getCheckbox(),
                                ""+customDialogMessage.getText().toString());
                        newlistmppapprovallist.add(s1);

                    }else{

                    }

                }

                try{
                    Gson gson = new GsonBuilder().create();
                    JsonArray myCustomArray = gson.toJsonTree(newlistmppapprovallist).getAsJsonArray();



                    submitforHodReject(myCustomArray.toString()); ;
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
        dialog.show();


    }

    private void submitforHodApproval(String result) {


        indicator.setVisibility(View.VISIBLE);
        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);

        api.MPPHodApproval(result,new Callback<MPPApprovalItemDataParcelable>()

        {

            @Override
            public void success(MPPApprovalItemDataParcelable getparcelable, Response response) {
                mppapprovallist.clear();
               // mAdapter.notifyDataSetChanged();

                indicator.setVisibility(View.GONE);
                Toast.makeText(getActivity(),"MPP details have been approved successfully.",Toast.LENGTH_SHORT).show();
                mppapprovallist.addAll(getparcelable.get$values());
                mAdapter.notifyDataSetChanged();
                if(mppapprovallist.size()==0){
                    fragment = new HomeFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.homeContainer,fragment);
                    transaction.commit();
                }



            }


//            @Override
//            public void success(MPPHODApprovalDataParcelable getparcelable, Response response) {
//
//
//                if (getparcelable.get$values().get(0).getApprovempp().equals("true")){
//
//
//
//
////                    fragment = new HomeFragment();
////                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
////                    transaction.replace(R.id.homeContainer,fragment);
////                    transaction.commit();
//                }else{
//                    try{
//                        Toast.makeText(getActivity(),"Something went wrong",Toast.LENGTH_SHORT).show();
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
//                }
//
//
//
//            }

            @Override
            public void failure(RetrofitError error) {
                try{
                    indicator.setVisibility(View.GONE);
                    Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

    }

    private void submitforHodReject(String result) {

        indicator.setVisibility(View.VISIBLE);


        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);



        api.MPPHodReject(result,new Callback<MPPApprovalItemDataParcelable>()

        {

            @Override
            public void success(MPPApprovalItemDataParcelable getparcelable, Response response) {
                mppapprovallist.clear();
                // mAdapter.notifyDataSetChanged();
                Toast.makeText(getActivity(),"Rejected.",Toast.LENGTH_SHORT).show();
                mppapprovallist.addAll(getparcelable.get$values());
                mAdapter.notifyDataSetChanged();
                indicator.setVisibility(View.GONE);

                if(mppapprovallist.size()==0){
                    fragment = new HomeFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.homeContainer,fragment);
                    transaction.commit();
                }

            }
//            @Override
//            public void success(MPPHODRejectDataParcelable getparcelable, Response response) {
//
//
//                if (getparcelable.get$values().get(0).getRejectmpp().equals("true")){
//
//                    Toast.makeText(getActivity(),"Rejected.",Toast.LENGTH_SHORT).show();
////                    fragment = new HomeFragment();
////                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
////                    transaction.replace(R.id.homeContainer,fragment);
////                    transaction.commit();
//                }else{
//                    try{
//                        Toast.makeText(getActivity(),"Something went wrong--1",Toast.LENGTH_SHORT).show();
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
//                }
//
//
//
//            }

            @Override
            public void failure(RetrofitError error) {

                try{
                    indicator.setVisibility(View.GONE);
                    Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

    }

    public void showDialogpreReject(final Activity activity, final String msg){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_customsubmit);

        TextView text = (TextView) dialog.findViewById(R.id.customDialogMessage);
        text.setText(msg);

        TextView customSubmit = (TextView) dialog.findViewById(R.id.customSubmit);
        ImageView closeDialog = (ImageView) dialog.findViewById(R.id.closeDialog);
        TextView customDraft = (TextView) dialog.findViewById(R.id.customDraft);
        customDraft.setText("No");
        customSubmit.setText("Yes");


        customSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try{

                    showDialogRejection(activity,msg);

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        customDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();


            }
        });

        dialog.show();

    }
}