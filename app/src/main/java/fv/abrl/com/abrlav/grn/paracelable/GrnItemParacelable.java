
package fv.abrl.com.abrlav.grn.paracelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GrnItemParacelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("ITEM")
    @Expose
    private int iTEM;
    @SerializedName("ITEM_DESC")
    @Expose
    private String iTEMDESC;
    @SerializedName("MPP")
    @Expose
    private int mPP;
    @SerializedName("SUPPLIER")
    @Expose
    private int sUPPLIER;
    @SerializedName("SUP_NAME")
    @Expose
    private String sUPNAME;


    public String getVendor_Code() {
        return Vendor_Code;
    }

    public void setVendor_Code(String vendor_Code) {
        Vendor_Code = vendor_Code;
    }

    @SerializedName("Vendor_Code")
    @Expose
    private String Vendor_Code;

    @SerializedName("GRNQuantity")
    @Expose
    private double grnQuantity;

    @SerializedName("GRNCost")
    @Expose
    private double grnCost;

    @SerializedName("STANDARD_UOM")
    @Expose
    private String sTANDARDUOM;

    public String getGrnStatus() {
        return grnStatus;
    }

    public void setGrnStatus(String grnStatus) {
        this.grnStatus = grnStatus;
    }

    @SerializedName("approverstatus")
    @Expose
    private String grnStatus;

    @SerializedName("GRN_Number")
    @Expose
    private String grnnumber;

    @SerializedName("GRN_Invoice_Number")
    @Expose
    private String GRN_Invoice_Number;

    @SerializedName("CC_Code")
    @Expose
    private int CC_Code;


    public String getGrnnumber() {
        return grnnumber;
    }

    public void setGrnnumber(String grnnumber) {
        this.grnnumber = grnnumber;
    }



    public String getGRN_Invoice_Number() {
        return GRN_Invoice_Number;
    }

    public void setGRN_Invoice_Number(String GRN_Invoice_Number) {
        this.GRN_Invoice_Number = GRN_Invoice_Number;
    }

    public int getCC_Code() {
        return CC_Code;
    }

    public void setCC_Code(int CC_Code) {
        this.CC_Code = CC_Code;
    }




    public int getCheckedItem() {
        return checkedItem;
    }

    public void setCheckedItem(int checkedItem) {
        this.checkedItem = checkedItem;
    }

    private int checkedItem = 0;

    public GrnItemParacelable(String iTEMDESC, double grnCost, double grnQuantity, int mPP,int checkedvalue,String standard,int iTEM,String GRN_Invoice_Number,int CC,String supname,int suppliercode,String Vendor_Code,String status,String grnnumber) {
        this.iTEMDESC =iTEMDESC;
        this.grnCost = grnCost;
        this.grnQuantity = grnQuantity;
        this.mPP = mPP;
        this.checkedItem = checkedvalue;
        this.sTANDARDUOM = standard;
        this.iTEM = iTEM;
        this.GRN_Invoice_Number = GRN_Invoice_Number;
        this.CC_Code = CC;
        this.sUPNAME = supname;
        this.sUPPLIER = suppliercode;
        this.Vendor_Code = Vendor_Code;
        this.grnStatus = status;
        this.grnnumber = grnnumber;
    }

    public double getGrnQuantity() {
        return grnQuantity;
    }

    public void setGrnQuantity(double grnQuantity) {
        this.grnQuantity = grnQuantity;
    }

    public double getGrnCost() {
        return grnCost;
    }

    public void setGrnCost(double grnCost) {
        this.grnCost = grnCost;
    }





    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getITEM() {
        return iTEM;
    }

    public void setITEM(int iTEM) {
        this.iTEM = iTEM;
    }

    public String getITEMDESC() {
        return iTEMDESC;
    }

    public void setITEMDESC(String iTEMDESC) {
        this.iTEMDESC = iTEMDESC;
    }

    public int getMPP() {
        return mPP;
    }

    public void setMPP(int mPP) {
        this.mPP = mPP;
    }

    public int getSUPPLIER() {
        return sUPPLIER;
    }

    public void setSUPPLIER(int sUPPLIER) {
        this.sUPPLIER = sUPPLIER;
    }

    public String getSUPNAME() {
        return sUPNAME;
    }

    public void setSUPNAME(String sUPNAME) {
        this.sUPNAME = sUPNAME;
    }


    public String getSTANDARDUOM() {
        return sTANDARDUOM;
    }

    public void setSTANDARDUOM(String sTANDARDUOM) {
        this.sTANDARDUOM = sTANDARDUOM;
    }

}
