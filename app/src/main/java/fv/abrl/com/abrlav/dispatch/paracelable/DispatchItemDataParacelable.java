
package fv.abrl.com.abrlav.dispatch.paracelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import fv.abrl.com.abrlav.grn.paracelable.$value;

public class DispatchItemDataParacelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<DispatchItemListParacelable> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<DispatchItemListParacelable> get$values() {
        return $values;
    }

    public void set$values(List<DispatchItemListParacelable> $values) {
        this.$values = $values;
    }

}
