package fv.abrl.com.abrlav.dispatch.adapter;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.dispatch.paracelable.DispatchItemDataParacelable;
import fv.abrl.com.abrlav.dispatch.paracelable.DispatchItemListParacelable;
import fv.abrl.com.abrlav.grn.adapter.VendorNameListAdapter;
import fv.abrl.com.abrlav.grn.paracelable.$value;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreListParacelable;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DispatchSelectStoreAdapterRv extends RecyclerView.Adapter<DispatchSelectStoreAdapterRv.MyViewHolder>
        implements Filterable, SectionIndexer {
    private Context mContext;
    ArrayList<PickingStoreListParacelable> arrayList;
    ArrayList<PickingStoreListParacelable> filteredarraylist;
    ArrayList<DispatchItemListParacelable> innerlist = new ArrayList<DispatchItemListParacelable>();
    private ArrayList<Integer> mSectionPositions;

public class MyViewHolder extends RecyclerView.ViewHolder {

    CheckBox dispatchstorename;
    TextView grnitemViewExpand;
    LinearLayout grnitemViewDetails;
    ListView productItemList;

    public MyViewHolder(View view) {
        super(view);

        dispatchstorename       = (CheckBox) view.findViewById(R.id.dispatchstorename);
        grnitemViewExpand       = (TextView) view.findViewById(R.id.grnitemViewExpand);
        grnitemViewDetails      = (LinearLayout) view.findViewById(R.id.grnitemViewDetails);
        productItemList         = (ListView) view.findViewById(R.id.productItemList);


    }

}


    public DispatchSelectStoreAdapterRv(Context c, ArrayList<PickingStoreListParacelable> arrayList) {
        mContext = c;
        this.arrayList = arrayList;
        this.filteredarraylist = arrayList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_dispatch_selectstore, parent, false);



        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.dispatchstorename.setText(filteredarraylist.get(position).getSTORENAME());


        holder.grnitemViewExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    if( holder.grnitemViewDetails.getVisibility()==View.VISIBLE){
                        holder.grnitemViewExpand.setBackground(mContext.getResources().getDrawable(R.drawable.arrow_down));
                        holder.grnitemViewDetails.setVisibility(View.GONE);

                    }else {

                        innerlist.clear();
                        pickitemlistbyStoreSelected(mContext, String.valueOf(filteredarraylist.get(position).getSTORE()),holder.productItemList);
                        holder.grnitemViewExpand.setBackground(mContext.getResources().getDrawable(R.drawable.arrow_up));
                        holder.grnitemViewDetails.setVisibility(View.VISIBLE);


                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        if(filteredarraylist.get(position).getCheckeditem()==1){
            holder.dispatchstorename.setChecked(true);
        }else{
            holder.dispatchstorename.setChecked(false);
        }


        holder.dispatchstorename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.dispatchstorename.isChecked()) {
                    filteredarraylist.get(position).setCheckeditem(1);
                    holder.dispatchstorename.setChecked(true);


                } else {
                    filteredarraylist.get(position).setCheckeditem(0);
                    holder.dispatchstorename.setChecked(false);
                }
            }

        });



    }

    @Override
    public int getItemCount() {
        return filteredarraylist.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredarraylist = arrayList;
                } else {
                    ArrayList<PickingStoreListParacelable> filteredList = new ArrayList<>();
                    for (PickingStoreListParacelable row : arrayList) {
                        if (row.getSTORENAME().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredarraylist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredarraylist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredarraylist = (ArrayList<PickingStoreListParacelable>) filterResults.values;

                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

//    public void setClickListener(ItemClickListener itemClickListener) {
//        this.clickListener = itemClickListener;
//    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>(26);
        mSectionPositions = new ArrayList<>(26);
        for (int i = 0, size = filteredarraylist.size(); i < size; i++) {
            String section = String.valueOf(filteredarraylist.get(i).getSTORENAME().charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mSectionPositions.get(sectionIndex);
    }

    private void pickitemlistbyStoreSelected(final Context ctx, String SelectedStore, final ListView productItemList) {



        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getstoreinnerproductlist(SelectedStore,new Callback<DispatchItemDataParacelable>()

        {
            @Override
            public void success(DispatchItemDataParacelable getccParacables, Response response) {


                if(0 != getccParacables.get$values().size()){

                    innerlist.addAll(getccParacables.get$values());
                    productItemList.setAdapter(new DispatchMultilevelItemListAdapter(ctx, innerlist));

                }else{
                    Toast.makeText(ctx,"No Item Found",Toast.LENGTH_SHORT).show();
                }



            }

            @Override
            public void failure(RetrofitError error) {


                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }
}
