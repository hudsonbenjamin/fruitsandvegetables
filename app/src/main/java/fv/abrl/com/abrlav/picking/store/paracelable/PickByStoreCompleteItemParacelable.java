
package fv.abrl.com.abrlav.picking.store.paracelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickByStoreCompleteItemParacelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("submitpickbystore")
    @Expose
    private String submitPickByStoreresultvalue;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getSubmitPickByStoreresultvalue() {
        return submitPickByStoreresultvalue;
    }

    public void setSubmitPickByStoreresultvalue(String submitPickByStoreresultvalue) {
        this.submitPickByStoreresultvalue = submitPickByStoreresultvalue;
    }

}
