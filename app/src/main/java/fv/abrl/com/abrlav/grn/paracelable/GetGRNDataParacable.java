
package fv.abrl.com.abrlav.grn.paracelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetGRNDataParacable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<GrnItemParacelable> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<GrnItemParacelable> get$values() {
        return $values;
    }

    public void set$values(List<GrnItemParacelable> $values) {
        this.$values = $values;
    }

}
