package fv.abrl.com.abrlav.view.mpp.fragment;

import android.app.Fragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.view.grn.adapter.GRN_ViewAdapter;
import fv.abrl.com.abrlav.view.grn.parcelable.GRNViewApprovalParcelable;
import fv.abrl.com.abrlav.view.mpp.adapter.MPPViewsCompletedAdapterRv;
import fv.abrl.com.abrlav.view.mpp.adapter.MppViewAdapterRv;
import fv.abrl.com.abrlav.view.mpp.parcelable.MPPViewDataItemParcelable;
import fv.abrl.com.abrlav.view.mpp.parcelable.MPPViewDataParcelable;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MppViewFragment extends Fragment {






    private Fragment fragment;
    private Unbinder unbinder;
    private View view;


    @BindView(R.id.statusSpinnerList)
    Spinner statusSpinnerList;

    @BindView(R.id.dateSpinnerList)
    Spinner dateSpinnerList;

    @BindView(R.id.mppviewList)
    RecyclerView mppviewList;


    MPPViewsCompletedAdapterRv mAdapter;



    public MppViewFragment() {
        // Required empty public constructor
    }


    ArrayList<MPPViewDataItemParcelable> viewarraylist = new ArrayList<MPPViewDataItemParcelable>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view                    =  inflater.inflate(R.layout.fragment_mpp_view, container, false);
        unbinder                =  ButterKnife.bind(this,view);



        try{

            ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                    .getColor(R.color.bottombar)));
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "MPP View" + "</font>"));

            HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
            if (Build.VERSION.SDK_INT >= 21) {
                getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
            }


        }catch (Exception e){
            e.printStackTrace();
        }





        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();

        categories.add("Approved");
        categories.add("Rejected");
        categories.add("Pending");

        List<String> datelist = new ArrayList<String>();
        datelist.add("Today");
        datelist.add("Yesterday");


        // Creating adapter for spinner
        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statusSpinnerList.setAdapter(categoryAdapter);

        ArrayAdapter<String> dateadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datelist);
        dateadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dateSpinnerList.setAdapter(dateadapter);



        statusSpinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {


                String item = parentView.getItemAtPosition(position).toString();
               try{
                   viewarraylist.clear();
               }catch (Exception ed){
                   ed.printStackTrace();
               }

               try{
                   getGRnView(item);
               }catch (Exception e){
                   e.printStackTrace();
               }




            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        dateSpinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String item = parentView.getItemAtPosition(position).toString();

                // Toast.makeText(getActivity(), "Selected: " + item, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

    }


    private void getGRnView(String status) {

        //indicator.setVisibility(View.VISIBLE);

        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getMPPViewList(status,new Callback<MPPViewDataParcelable>()

        {
            @Override
            public void success(MPPViewDataParcelable getccParacables, Response response) {

                try{
                    if(0 != getccParacables.get$values().size()){
                        viewarraylist.addAll(getccParacables.get$values());

                        mAdapter                =       new MPPViewsCompletedAdapterRv(getActivity(), viewarraylist);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        mppviewList.setLayoutManager(mLayoutManager);
                        mppviewList.setItemAnimator(new DefaultItemAnimator());
                        mppviewList.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                    }else{

                        Toast.makeText(getActivity(),"No Item Found",Toast.LENGTH_SHORT).show();
                    }
                   // indicator.setVisibility(View.GONE);

                }catch (Exception e){
                   // indicator.setVisibility(View.GONE);
                    e.printStackTrace();

                }







            }

            @Override
            public void failure(RetrofitError error) {

                //indicator.setVisibility(View.GONE);
                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }

}