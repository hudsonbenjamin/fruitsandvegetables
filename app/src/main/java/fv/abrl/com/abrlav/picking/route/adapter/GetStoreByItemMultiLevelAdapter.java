package fv.abrl.com.abrlav.picking.route.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SectionIndexer;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


import fv.abrl.com.abrlav.R;

import fv.abrl.com.abrlav.picking.route.fragment.PickByRouteItemSelectionFragment;
import fv.abrl.com.abrlav.picking.route.fragment.PickByRouteSelectionFragment;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreListParacelable;

public class GetStoreByItemMultiLevelAdapter extends RecyclerView.Adapter<GetStoreByItemMultiLevelAdapter.MyViewHolder>
         {
    private Context mContext;


    private ArrayList<PickingStoreListParacelable> arrayList;
    public static ArrayList<PickingStoreListParacelable> filteredarraylist;
    private ArrayList<Integer> mSectionPositions;
    public static ArrayList<Integer> itemCodeList = new ArrayList<Integer>();
    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView storeName2;
        EditText storeQunantityEt2;

        public MyViewHolder(View view) {
            super(view);
            storeName2 = (TextView)  view.findViewById(R.id.storeName2);
            storeQunantityEt2  = (EditText)view.findViewById(R.id.storeQunantityEt2);
        }

    }

    public GetStoreByItemMultiLevelAdapter(Context c, ArrayList<PickingStoreListParacelable> arrayList) {
        mContext = c;
        this.arrayList = arrayList;
        this.filteredarraylist = arrayList;



    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.getstorebyitemmultileveladapter, parent, false);



        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.storeName2.setText(""+filteredarraylist.get(position).getSTORENAME());
        if(filteredarraylist.get(position).getStoreQunantity()==0){
            holder.storeQunantityEt2.setText("");
        }else{
            holder.storeQunantityEt2.setText(""+filteredarraylist.get(position).getStoreQunantity());
        }
        holder.storeQunantityEt2.setKeyListener(DigitsKeyListener.getInstance(true,true));
        holder.storeQunantityEt2.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (s.length() > 0) {
                       itemCodeList.add(filteredarraylist.get(position).getiTEM());

                        filteredarraylist.get(position).setStoreQunantity(Integer.parseInt(holder.storeQunantityEt2.getText().toString()));

                    } else {
                        // itemCodeList.remove(filteredarraylist.get(position).getiTEM());
                        filteredarraylist.get(position).setStoreQunantity(0);


                    }

                } catch (Exception e) {
                    e.printStackTrace();

                }


            }
        });

    }

    @Override
    public int getItemCount() {
        return filteredarraylist.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}