package fv.abrl.com.abrlav.view.picking.menu.fragment;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.home.adapter.ImageAdapterGridView;
import fv.abrl.com.abrlav.picking.item.fragment.PickByItemSelectionFragment;
import fv.abrl.com.abrlav.picking.store.fragment.PickByStoreFragment;

/**
 * Created by Hudson on 7/11/2018.
 */

public class PickingViewMenuFragment extends Fragment {


    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    @BindView(R.id.gridviewMenu)
    GridView gridviewMenu;

    @BindView(R.id.welcomeUser)
    TextView welcomeUser;



    Fragment fragment;
    private Unbinder unbinder;

    @BindView(R.id.viewModule)
    TextView viewModule;

    int[] imageIDs = {
            R.drawable.pickbyroute, R.drawable.pickbystore, R.drawable.pick_byitem

    };
    String[] web = {

            "PICK BY ROUTE",
            "PICK BY STORE",
            "PICK BY ITEM"



    } ;





    public PickingViewMenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        sharedPref                  =   getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                      =   sharedPref.edit();
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this,view);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.colorPrimary)));
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Picking View");
        HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_white_resize);
        if (Build.VERSION.SDK_INT >= 21) {
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        viewModule.setVisibility(View.VISIBLE);

        try{
            welcomeUser.setText("Welcome "+sharedPref.getString("username","username"));
        }catch (Exception e){
            e.printStackTrace();
        }


        gridviewMenu.setAdapter(new ImageAdapterGridView(getActivity(), web, imageIDs));

        gridviewMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent,
                                    View v, int position, long id) {
                if(position==0){
                    fragment = new PickingViewFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.homeContainer,fragment);
                    transaction.commit();

                }else if(position==1){
                    fragment = new PickingViewFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.homeContainer,fragment);
                    transaction.commit();
                }else if(position==2){
                    fragment = new PickingViewFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.homeContainer,fragment);
                    transaction.commit();
                }


            }
        });



        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);




    }




}