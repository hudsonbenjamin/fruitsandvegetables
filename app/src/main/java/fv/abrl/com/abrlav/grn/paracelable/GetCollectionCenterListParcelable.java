
package fv.abrl.com.abrlav.grn.paracelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCollectionCenterListParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("CC")
    @Expose
    private int cC;
    @SerializedName("CC_NAME")
    @Expose
    private String cCNAME;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getCC() {
        return cC;
    }

    public void setCC(int cC) {
        this.cC = cC;
    }

    public String getCCNAME() {
        return cCNAME;
    }

    public void setCCNAME(String cCNAME) {
        this.cCNAME = cCNAME;
    }

}
