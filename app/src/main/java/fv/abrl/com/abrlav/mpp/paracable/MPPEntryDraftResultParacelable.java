
package fv.abrl.com.abrlav.mpp.paracable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MPPEntryDraftResultParacelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("draftmpp")
    @Expose
    private String mppdraftvaluesresult;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getMppdraftvaluesresult() {
        return mppdraftvaluesresult;
    }

    public void setMppdraftvaluesresult(String mppdraftvaluesresult) {
        this.mppdraftvaluesresult = mppdraftvaluesresult;
    }

}
