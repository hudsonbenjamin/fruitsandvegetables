
package fv.abrl.com.abrlav.login.parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResultParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("EmpId")
    @Expose
    private Integer empId;
    @SerializedName("Employee_Name")
    @Expose
    private String employeeName;
    @SerializedName("role")
    @Expose
    private Integer role;
    @SerializedName("roletype")
    @Expose
    private String roletype;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public Integer getEmpId() {
        return empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getRoletype() {
        return roletype;
    }

    public void setRoletype(String roletype) {
        this.roletype = roletype;
    }

}
