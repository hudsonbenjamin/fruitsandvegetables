package fv.abrl.com.abrlav.view.mpp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.SectionIndexer;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.mpp.paracable.MPPEntrySubmitItemParcelable;
import fv.abrl.com.abrlav.view.mpp.parcelable.MPPViewDataItemParcelable;


/**
 * Created by Hudson on 7/26/2018.
 */

public class MppViewAdapterRv extends RecyclerView.Adapter<MppViewAdapterRv.MyViewHolder>
        implements Filterable {

    private Context mContext;
    ArrayList<MPPViewDataItemParcelable> arrayList;
    ArrayList<MPPViewDataItemParcelable> filteredarraylist;
    ArrayList<String> checkboxcount = new ArrayList<String>();


    private ArrayList<Integer> mSectionPositions;



    private MPPViewDataItemParcelable getItem(int position) {
        return filteredarraylist.get(position);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView finalentryCheckBox;
        TextView marketPrice;
        TextView Mpp;
        EditText finalMpp;

        public MyViewHolder(View view) {
            super(view);

            finalentryCheckBox  = (TextView) view.findViewById(R.id.finalentryCheckBox);
            marketPrice         = (TextView)  view.findViewById(R.id.marketPrice);
            Mpp                 = (TextView)  view.findViewById(R.id.Mpp);
            finalMpp            = (EditText) view.findViewById(R.id.finalMpp);


        }


    }


    public MppViewAdapterRv(Context c, ArrayList<MPPViewDataItemParcelable> arrayList) {
        mContext = c;
        this.arrayList = arrayList;
        this.filteredarraylist = arrayList;
        //setHasStableIds(true);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_mppfinalentry, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.finalentryCheckBox.setText(""+filteredarraylist.get(position).getITEMDESC()+" ("+filteredarraylist.get(position).getSTANDARDUOM()+")");
//        holder.finalMpp.setText(""+filteredarraylist.get(position).getMarketprice());
//        filteredarraylist.get(position).setFinalMPP(filteredarraylist.get(position).getMPP());//tempory finalMPP value
//        holder.finalMpp.setText(""+filteredarraylist.get(position).getMPP());
//        holder.Mpp.setText(""+filteredarraylist.get(position).getMPP());
//        holder.marketPrice.setText(""+filteredarraylist.get(position).getMarketprice());
//
//        holder.finalMpp.setKeyListener(DigitsKeyListener.getInstance(true,true));
//        holder.finalMpp.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                try{
//                    if(s.length()>0){
//
//                        filteredarraylist.get(position).setFinalMPP(Float.parseFloat(holder.finalMpp.getText().toString()));
//
//                      //  holder.finalentryCheckBox.setTextColor(Color.parseColor("#ed6c09"));
//
//                    }else{
//
//                         //finalentryCheckBox.setChecked(false);
//                    }
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//
//
//            }
//        });


    }


    @Override
    public int getItemCount() {
        return filteredarraylist.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredarraylist = arrayList;
                } else {
                    ArrayList<MPPViewDataItemParcelable> filteredList = new ArrayList<>();
                    for (MPPViewDataItemParcelable row : arrayList) {
                        if (row.getITEMDESC().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredarraylist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredarraylist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredarraylist = (ArrayList<MPPViewDataItemParcelable>) filterResults.values;

                notifyDataSetChanged();
            }
        };
    }



    @Override
    public int getItemViewType(int position) {
        return position;
    }


}