package fv.abrl.com.abrlav.view.mpp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.mpp.fragment.MppApprovalFragment;
import fv.abrl.com.abrlav.mpp.paracable.MPPApprovalItemListParcelable;
import fv.abrl.com.abrlav.view.mpp.parcelable.MPPViewDataItemParcelable;


/**
 * Created by Hudson on 7/30/2018.
 */

public class MPPViewsCompletedAdapterRv extends RecyclerView.Adapter<MPPViewsCompletedAdapterRv.MyViewHolder>
        implements Filterable {

    private Context mContext;
    private ArrayList<MPPViewDataItemParcelable> arrayList;
    private ArrayList<MPPViewDataItemParcelable> filteredarraylist;
    private ArrayList<Integer> mSectionPositions;
    private ArrayList<String> checkboxcount = new ArrayList<String>();



    private MPPViewDataItemParcelable getItem(int position) {
        return filteredarraylist.get(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView ch;
        private TextView yesterdaympp;
        private TextView marketprice;
        private TextView mpp;
        private TextView finalmpp;

        public MyViewHolder(View view) {


            super(view);

            ch                        =   (TextView) view.findViewById(R.id.approvalitemCheckbox);
            yesterdaympp              =   (TextView) view.findViewById(R.id.yesterdayMpp);
            marketprice               =   (TextView) view.findViewById(R.id.approvedMarketPrice);
            mpp                       =   (TextView) view.findViewById(R.id.approvedMPP);
            finalmpp                  =   (TextView) view.findViewById(R.id.approvedFinalMPP);

        }


    }


    public MPPViewsCompletedAdapterRv(Context c, ArrayList<MPPViewDataItemParcelable> arrayList) {
        mContext = c;
        this.arrayList = arrayList;
        this.filteredarraylist = arrayList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_mpp_view_completed, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.ch.setText("" + filteredarraylist.get(position).getITEMDESC() + " (" + filteredarraylist.get(position).getSTANDARDUOM() + ")");
        holder.yesterdaympp.setText("" + filteredarraylist.get(position).getYMPP());
        holder.marketprice.setText("" + filteredarraylist.get(position).getMarketPrice());
        holder.mpp.setText("" + filteredarraylist.get(position).getMPP());
        holder.finalmpp.setText(""+filteredarraylist.get(position).getFMPP());


    }











    @Override
    public int getItemCount() {
        return filteredarraylist.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredarraylist = arrayList;
                } else {
                    ArrayList<MPPViewDataItemParcelable> filteredList = new ArrayList<>();
                    for (MPPViewDataItemParcelable row : arrayList) {
                        if (row.getITEMDESC().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredarraylist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredarraylist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredarraylist = (ArrayList<MPPViewDataItemParcelable>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }



    @Override
    public int getItemViewType(int position) {
        return position;
    }


//    public long getItemId(int position) {
//        return position;
//    }





}
