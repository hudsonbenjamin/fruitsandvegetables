package fv.abrl.com.abrlav.picking.route.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.utils.CustomLinearLayoutManager;
import fv.abrl.com.abrlav.picking.route.fragment.PickByRouteItemSelectionFragment;
import fv.abrl.com.abrlav.picking.route.parcelable.PickingRouteProductItemParcelable;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreDataParacable;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreListParacelable;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ProductItemListbySelectAdapter extends RecyclerView.Adapter<ProductItemListbySelectAdapter.MyViewHolder>
        implements Filterable, SectionIndexer {
    // implements Filterable{

    private Context mContext;


    private ArrayList<PickingRouteProductItemParcelable> arrayList;
    public static ArrayList<PickingRouteProductItemParcelable> filteredarraylist;
    private ArrayList<String> checkboxcount = new ArrayList<String>();
    private ArrayList<Integer> mSectionPositions;
    public static ArrayList<PickingStoreListParacelable> innerlist = new ArrayList<PickingStoreListParacelable>();
    public static ArrayList<PickingStoreListParacelable> loadWholeinnerlist = new ArrayList<PickingStoreListParacelable>();




    GetStoreByItemMultiLevelAdapter madapter;


    private PickingRouteProductItemParcelable getItem(int position) {
        return filteredarraylist.get(position);
    }
    //private ValueFilter valueFilter;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CheckBox productID;
        TextView  itemCode;
        ImageView openStoreView;
        RecyclerView innerStoreList;
        LinearLayout grnitemViewDetails;


        public MyViewHolder(View view) {
            super(view);


            productID               = (CheckBox) view.findViewById(R.id.productID);
            itemCode                = (TextView) view.findViewById(R.id.itemcode);
            innerStoreList          = (RecyclerView) view.findViewById(R.id.innerStoreList);
            grnitemViewDetails      = (LinearLayout) view.findViewById(R.id.grnitemViewDetails);
            openStoreView           = (ImageView) view.findViewById(R.id.openStoreView);


        }
    }


    public ProductItemListbySelectAdapter(Context c, ArrayList<PickingRouteProductItemParcelable> arrayList) {
        mContext = c;
        this.arrayList = arrayList;
        this.filteredarraylist = arrayList;
        //setHasStableIds(true);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.picking_storelistbyproductselect_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


            holder.productID.setText(filteredarraylist.get(position).getITEMDESC());
            holder.itemCode.setText("Item Code : "+filteredarraylist.get(position).getITEM());


            if(filteredarraylist.get(position).getCheckedValue()==1){
                holder.productID.setChecked(true);
            }else{
                holder.productID.setChecked(false);
            }


//            holder.openStoreView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (holder.grnitemViewDetails.getVisibility()==View.VISIBLE){
//
//
//                        holder.grnitemViewDetails.setVisibility(View.GONE);
//                        holder.openStoreView.setImageResource(R.drawable.ic_keyboard_arrow_down_orange_24dp);
//                    }else{
//
//                        holder.grnitemViewDetails.setVisibility(View.VISIBLE);
//                        holder.openStoreView.setImageResource(R.drawable.ic_keyboard_arrow_up_orange_35dp);
//                      if( ProductItemListbySelectAdapter.itemCode.contains(filteredarraylist.get(position).getITEM())){
//
//                          Toast.makeText(mContext,"already present",Toast.LENGTH_SHORT).show();
//
//                      }else{
//                          getStoreListbyProduct(filteredarraylist.get(position).getITEM(),holder.innerStoreList,position,filteredarraylist.get(position).getITEMDESC());
//
//                      }
//
//                    }
//
//
//
//                }
//            });



        holder.productID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    if (holder.productID.isChecked()) {
                        holder.productID.setChecked(false);

                    } else {
                        holder.productID.setChecked(false);
                    }

            }

        });

    }
    @Override
    public int getItemCount() {

        return filteredarraylist.size();

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                ArrayList<PickingRouteProductItemParcelable> filteredList = new ArrayList<>();
                if (charString.isEmpty()) {
                    filteredarraylist = arrayList;

                } else {

                    for (PickingRouteProductItemParcelable row : arrayList) {
                        if (row.getITEMDESC().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredarraylist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredarraylist;
                filterResults.count = filteredarraylist.size();
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                filteredarraylist = (ArrayList<PickingRouteProductItemParcelable>) filterResults.values;
                notifyDataSetChanged();



            }
        };
    }



    @Override
    public int getItemViewType(int position) {
        return position;
    }




    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>(26);
        mSectionPositions = new ArrayList<>(26);
        for (int i = 0, size = filteredarraylist.size(); i < size; i++) {
            String section = String.valueOf(filteredarraylist.get(i).getITEMDESC().charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mSectionPositions.get(sectionIndex);
    }


    public long getItemId(int position) {
        return position;
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void getStoreListbyProduct(final int itemcode, final RecyclerView innerStoreList,int position,final String itemdesc) {



        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.pickinggetStorelistbyItemSelect(itemcode,new Callback<PickingStoreDataParacable>()

        {
            @Override
            public void success(PickingStoreDataParacable getccParacables, Response response) {

                try{
                    if(0 != getccParacables.get$values().size()){
                        loadWholeinnerlist.addAll(innerlist);
                        innerlist.clear();
                        innerlist.addAll(getccParacables.get$values());

                        for(int i =0;i<innerlist.size();i++){
                            innerlist.get(i).setiTEM(itemcode);
                            innerlist.get(i).setPosition(i);
                            innerlist.get(i).setItemdesc(itemdesc);
                        }

                        madapter            =       new GetStoreByItemMultiLevelAdapter(mContext, innerlist);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
                        innerStoreList.setLayoutManager(mLayoutManager);
                        innerStoreList.setItemAnimator(new DefaultItemAnimator());
                        innerStoreList.setAdapter(madapter);
                        madapter.notifyDataSetChanged();

                    }else{


                    }
                }catch (Exception e){
                    e.printStackTrace();

                }







            }

            @Override
            public void failure(RetrofitError error) {


                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }
}