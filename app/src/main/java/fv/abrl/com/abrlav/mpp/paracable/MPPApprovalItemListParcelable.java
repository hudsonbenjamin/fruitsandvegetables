
package fv.abrl.com.abrlav.mpp.paracable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MPPApprovalItemListParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("CC")
    @Expose
    private int cC;
    @SerializedName("Role")
    @Expose
    private int role = 1;
    @SerializedName("MPPID")
    @Expose
    private int mPPID;
    @SerializedName("ITEM")
    @Expose
    private int iTEM;
    @SerializedName("ITEM_DESC")
    @Expose
    private String iTEMDESC;
    @SerializedName("STANDARD_UOM")
    @Expose
    private String sTANDARDUOM;
    @SerializedName("MPP")
    @Expose
    private float mPP;
    @SerializedName("Marketprice")
    @Expose
    private float marketprice;
    @SerializedName("FinalMPP")
    @Expose
    private float finalMPP;
    @SerializedName("YMPP")
    @Expose
    private float yMPP;

    private int checkbox = 0;

    public String getComments() {
        return Comments;
    }

    public void setComments(String comments) {
        this.Comments = comments;
    }

    private String Comments;


    public MPPApprovalItemListParcelable(int cc, int role, int mppid, float finalMPP, String standarduom, int item, String itemdesc, float marketprice, int checkbox,String comments) {

        this.cC = cc;
        this.role = role;
        this.mPPID = mppid;
        this.finalMPP = finalMPP;
        this.sTANDARDUOM = standarduom;
        this.iTEM = item;
        this.iTEMDESC = itemdesc;
        this.marketprice = marketprice;
        this.checkbox = checkbox;
        this.Comments = comments;


    }

    public int getCheckbox() {
        return checkbox;
    }

    public void setCheckbox(int checkbox) {
        this.checkbox = checkbox;
    }




    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getCC() {
        return cC;
    }

    public void setCC(int cC) {
        this.cC = cC;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public int getMPPID() {
        return mPPID;
    }

    public void setMPPID(int mPPID) {
        this.mPPID = mPPID;
    }

    public int getITEM() {
        return iTEM;
    }

    public void setITEM(int iTEM) {
        this.iTEM = iTEM;
    }

    public String getITEMDESC() {
        return iTEMDESC;
    }

    public void setITEMDESC(String iTEMDESC) {
        this.iTEMDESC = iTEMDESC;
    }

    public String getSTANDARDUOM() {
        return sTANDARDUOM;
    }

    public void setSTANDARDUOM(String sTANDARDUOM) {
        this.sTANDARDUOM = sTANDARDUOM;
    }

    public float getMPP() {
        return mPP;
    }

    public void setMPP(float mPP) {
        this.mPP = mPP;
    }

    public float getMarketprice() {
        return marketprice;
    }

    public void setMarketprice(float marketprice) {
        this.marketprice = marketprice;
    }

    public float getFinalMPP() {
        return finalMPP;
    }

    public void setFinalMPP(float finalMPP) {
        this.finalMPP = finalMPP;
    }

    public float getYMPP() {
        return yMPP;
    }

    public void setYMPP(float yMPP) {
        this.yMPP = yMPP;
    }

}
