
package fv.abrl.com.abrlav.view.grn.parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GRNApprovalItemListParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("GRN_Invoice_Number")
    @Expose
    private String gRNInvoiceNumber;
    @SerializedName("SUPPLIER")
    @Expose
    private Integer sUPPLIER;
    @SerializedName("GRN_Number")
    @Expose
    private Integer gRNNumber;


    @SerializedName("SUP_NAME")
    @Expose
    private String sUPNAME;


    public String getPdf_Path() {
        return pdf_Path;
    }

    public void setPdf_Path(String pdf_Path) {
        this.pdf_Path = pdf_Path;
    }

    @SerializedName("PDF_path")
    @Expose
    private String pdf_Path;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getGRNInvoiceNumber() {
        return gRNInvoiceNumber;
    }

    public void setGRNInvoiceNumber(String gRNInvoiceNumber) {
        this.gRNInvoiceNumber = gRNInvoiceNumber;
    }

    public Integer getSUPPLIER() {
        return sUPPLIER;
    }

    public void setSUPPLIER(Integer sUPPLIER) {
        this.sUPPLIER = sUPPLIER;
    }

    public Integer getGRNNumber() {
        return gRNNumber;
    }

    public void setGRNNumber(Integer gRNNumber) {
        this.gRNNumber = gRNNumber;
    }

    public String getSUPNAME() {
        return sUPNAME;
    }

    public void setSUPNAME(String sUPNAME) {
        this.sUPNAME = sUPNAME;
    }

}
