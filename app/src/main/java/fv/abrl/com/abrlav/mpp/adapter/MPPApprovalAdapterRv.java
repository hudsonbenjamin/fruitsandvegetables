package fv.abrl.com.abrlav.mpp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.mpp.fragment.MppApprovalFragment;
import fv.abrl.com.abrlav.mpp.paracable.MPPApprovalItemListParcelable;


/**
 * Created by Hudson on 7/30/2018.
 */

public class MPPApprovalAdapterRv extends RecyclerView.Adapter<MPPApprovalAdapterRv.MyViewHolder>
        implements Filterable,SectionIndexer {

    private Context mContext;
    private ArrayList<MPPApprovalItemListParcelable> arrayList;
    private ArrayList<MPPApprovalItemListParcelable> filteredarraylist;
    private ArrayList<Integer> mSectionPositions;
    private ArrayList<String> checkboxcount = new ArrayList<String>();



    private MPPApprovalItemListParcelable getItem(int position) {
        return filteredarraylist.get(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private CheckBox ch;
        private TextView yesterdaympp;
        private TextView marketprice;
        private TextView mpp;
        private EditText finalmpp;

        public MyViewHolder(View view) {


            super(view);

            ch                        =   (CheckBox) view.findViewById(R.id.approvalitemCheckbox);
            yesterdaympp              =   (TextView) view.findViewById(R.id.yesterdayMpp);
            marketprice               =   (TextView) view.findViewById(R.id.approvedMarketPrice);
            mpp                       =   (TextView) view.findViewById(R.id.approvedMPP);
            finalmpp                  =   (EditText) view.findViewById(R.id.approvedFinalMPP);

        }


    }


    public MPPApprovalAdapterRv(Context c, ArrayList<MPPApprovalItemListParcelable> arrayList) {
        mContext = c;
        this.arrayList = arrayList;
        this.filteredarraylist = arrayList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_mpp_approval, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
       // MPPApprovalItemListParcelable retrivedata = filteredarraylist.get(position);


       holder.ch.setText(""+filteredarraylist.get(position).getITEMDESC()+" ("+filteredarraylist.get(position).getSTANDARDUOM()+")");
       holder.yesterdaympp.setText(""+filteredarraylist.get(position).getYMPP());
       holder.marketprice.setText(""+filteredarraylist.get(position).getMarketprice());
       holder.mpp.setText(""+filteredarraylist.get(position).getMPP());
       holder.finalmpp.setText(""+filteredarraylist.get(position).getFinalMPP());

       try{
           if(filteredarraylist.get(position).getCheckbox()==1){
               holder.ch.setChecked(true);
           }else{
               holder.ch.setChecked(false);
           }
       }catch (Exception e){
           e.printStackTrace();
       }





        holder.finalmpp.setKeyListener(DigitsKeyListener.getInstance(true,true));
        holder.finalmpp.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                    if(s.length()>0){


                        filteredarraylist.get(position).setFinalMPP(Float.parseFloat(holder.finalmpp.getText().toString()));
                      //  holder.ch.setTextColor(Color.parseColor("#ed6c09"));


                    }else{
                        filteredarraylist.get(position).setFinalMPP(0);



                    }



            }
        });


        holder.ch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if(!holder.finalmpp.getText().toString().isEmpty()){
                    if(holder.ch.isChecked())
                    {

                        checkboxvaluereturn();
                        filteredarraylist.get(position).setCheckbox(1);
                        filteredarraylist.get(position).setFinalMPP(Float.parseFloat(holder.finalmpp.getText().toString()));

                    }
                    else
                    {
                       checkboxvaluereturn();
                      //  holder.finalmpp.setText("");
                        holder.ch.setChecked(false);
                        filteredarraylist.get(position).setCheckbox(0);

                    }
                }else{
                    filteredarraylist.get(position).setCheckbox(0);
                   holder.ch.setChecked(false);
                    Toast.makeText(mContext,"Please Enter Final MPP",Toast.LENGTH_SHORT).show();
                }

            }
        });


        holder.ch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (!holder.finalmpp.getText().toString().isEmpty()) {
                    if (buttonView.isChecked()) {
                        filteredarraylist.get(position).setCheckbox(1);
                        filteredarraylist.get(position).setFinalMPP(Float.parseFloat(holder.finalmpp.getText().toString()));
                        holder.ch.setChecked(true);
                    } else {
                        filteredarraylist.get(position).setCheckbox(0);
                        holder.ch.setChecked(false);
                    }
                } else {
                    filteredarraylist.get(position).setCheckbox(0);
                    holder.ch.setChecked(false);
                    Toast.makeText(mContext, "Please Enter Final MPP", Toast.LENGTH_SHORT).show();
                }
            }

        });








    }


    @Override
    public int getItemCount() {
        return filteredarraylist.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredarraylist = arrayList;
                } else {
                    ArrayList<MPPApprovalItemListParcelable> filteredList = new ArrayList<>();
                    for (MPPApprovalItemListParcelable row : arrayList) {
                        if (row.getITEMDESC().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredarraylist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredarraylist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredarraylist = (ArrayList<MPPApprovalItemListParcelable>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }



    @Override
    public int getItemViewType(int position) {
        return position;
    }


//    public long getItemId(int position) {
//        return position;
//    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>(26);
        mSectionPositions = new ArrayList<>(26);
        for (int i = 0, size = filteredarraylist.size(); i < size; i++) {
            String section = String.valueOf(filteredarraylist.get(i).getITEMDESC().charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mSectionPositions.get(sectionIndex);
    }

    private void checkboxvaluereturn(){
        try{
            checkboxcount.clear();
            for(int i=0;i<filteredarraylist.size();i++){
                if(filteredarraylist.get(i).getCheckbox()==1){

                    checkboxcount.add(filteredarraylist.get(i).getITEMDESC());
                }


            }
           MppApprovalFragment.lineCount.setText("Lines : "+checkboxcount.size());
        }catch (Exception e){
            e.printStackTrace();
        }

    }



}
