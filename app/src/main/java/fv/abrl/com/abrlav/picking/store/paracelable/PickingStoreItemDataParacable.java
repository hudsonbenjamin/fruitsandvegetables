
package fv.abrl.com.abrlav.picking.store.paracelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import fv.abrl.com.abrlav.grn.paracelable.*;

public class PickingStoreItemDataParacable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<PickingStoreItemListParacelable> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<PickingStoreItemListParacelable> get$values() {
        return $values;
    }

    public void set$values(List<PickingStoreItemListParacelable> $values) {
        this.$values = $values;
    }

}
