
package fv.abrl.com.abrlav.picking.route.parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickingRouteProductItemParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("ITEM")
    @Expose
    private Integer iTEM;
    @SerializedName("ITEM_DESC")
    @Expose
    private String iTEMDESC;

    public int getCheckedValue() {
        return checkedValue;
    }

    public void setCheckedValue(int checkedValue) {
        this.checkedValue = checkedValue;
    }

    private int checkedValue =0;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public Integer getITEM() {
        return iTEM;
    }

    public void setITEM(Integer iTEM) {
        this.iTEM = iTEM;
    }

    public String getITEMDESC() {
        return iTEMDESC;
    }

    public void setITEMDESC(String iTEMDESC) {
        this.iTEMDESC = iTEMDESC;
    }

}
