package fv.abrl.com.abrlav.farmer.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;



import java.util.ArrayList;


import butterknife.BindView;
import butterknife.OnClick;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FarmerRegistration extends Fragment {

//    private ArrayList<DataParcelable> itemlist = new ArrayList<DataParcelable>();
//    private TextView txtcomplete;
//
//    TextView txt1,txt2,txt3, vendorlabel,txtcheck,txtcc;
//    EditText edtvendorcode;
//    LinearLayout vendorlay;
//    RelativeLayout vendorlayout;
//    CheckBox chkVendor;
//    //MaterialBetterSpinner Spinner_collectioncenter;
//    Spinner Spinner_collectioncenter;
//    ArrayAdapter<String> spinneradapter ;
//    String[] collectionlist = {"Collection center 1", "Collection center2"};
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {
//         super.onCreateView(inflater, container, savedInstanceState);
//
//         final View view = inflater.inflate(R.layout.fragment_farmer_reg1,container,false);
//         chkVendor = view.findViewById(R.id.checkvendor);
//         vendorlayout = view.findViewById(R.id.vendorcodelay);
//         vendorlabel = view.findViewById(R.id.vendolabel);
//         edtvendorcode = view.findViewById(R.id.edtvendorcode);
//         txtcheck = view.findViewById(R.id.check);
//         txtcc = view.findViewById(R.id.txtcc);
//        Spinner_collectioncenter = view.findViewById(R.id.spinnerlist);
//         spinneradapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_dropdown_item_1line,collectionlist);
//        Spinner_collectioncenter.setAdapter(spinneradapter);
//
//       /* Spinner_collectioncenter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//            }
//        });*/
//       txtcc.setOnClickListener(new View.OnClickListener() {
//           @Override
//           public void onClick(View view) {
//               Spinner_collectioncenter.performClick();
//           }
//       });
//
//        Spinner_collectioncenter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                String s = adapterView.getSelectedItem().toString();
//                txtcc.setText(s);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
//
//        chkVendor.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(chkVendor.isChecked())
//                {
//                    vendorlabel.setText("Enter Vendor Code");
//                    vendorlabel.setVisibility(View.VISIBLE);
//                    vendorlayout.setVisibility(View.VISIBLE);
//                    //Toast.makeText(getActivity(),"Selected", Toast.LENGTH_LONG).show();
//                }
//                else
//                {
//                    vendorlabel.setText("");
//                    vendorlabel.setVisibility(View.GONE);
//                    vendorlayout.setVisibility(View.GONE);
//                    //Toast.makeText(getActivity(),"unSelected", Toast.LENGTH_LONG).show();
//                }
//            }
//        });
//
//        txtcheck.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                // Do Service process.....
//            }
//        });
//
//
//        /*final RadioGroup RG = (RadioGroup)view.findViewById(R.id.option);
//        vendorlay = (LinearLayout)view.findViewById(R.id.vendorlay);
//
//        RG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup radioGroup, int i) {
//                int id = RG.getCheckedRadioButtonId();
//                Log.e("option", "----"+id);
//                Log.e("button", "----"+i);
//                vendorlay.setVisibility(View.VISIBLE);
//                TextView txtlabel = view.findViewById(R.id.txtinput);
//                if(i==2)
//                {
//
//                    txtlabel.setText("Vendor Code");
//                }
//                else if(i==3)
//                {
//
//                    txtlabel.setText("Registered Mobile Number");
//                }
//                else
//                {
//                    vendorlay.setVisibility(View.GONE);
//                }
//            }
//        });
//*/
//
//
//        /*TableLayout TL = (TableLayout) view.findViewById(R.id.viewhistory);
//        TL.setStretchAllColumns(true);
//
//        for(int i=1;i<=3;i++)
//        {
//            TableRow row= new TableRow(getActivity());
//            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
//            row.setLayoutParams(lp);
//
//            txt1 = new TextView(getActivity());
//            txt2 = new TextView(getActivity());
//            txt3 = new TextView(getActivity());
//
//            txt1.setText("text"+String.valueOf(i));
//            txt2.setText("text"+String.valueOf(i));
//            txt3.setText("text"+String.valueOf(i));
//            //txt1.setGravity(View.FOCUS_LEFT);
//            txt3.setGravity(View.FOCUS_RIGHT);
//            row.addView(txt1);
//            row.addView(txt2);
//            row.addView(txt3);
//
//            TL.addView(row);
//        }
//*/
//
//
//        /*view.findViewById( R.id.fmrcomplete).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                //https://developer.android.com/guide/topics/ui/dialogs
//                final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
//                LayoutInflater inflater = getActivity().getLayoutInflater();
//
//                dialog.setView(inflater.inflate(R.layout.dialog_farmer,container,false))
//                        .setPositiveButton("yes", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//
//                    }
//                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialog.create().dismiss();
//                    }
//                });
//
//
//                dialog.create().show();
//
//
//
//            }
//        });*/
//
//        //getAPIData();
//        return view;
//
//
//    }
//
//    private void getAPIData() {
//
//
//        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
//        iHttpApiCall api = adapter.create(iHttpApiCall.class);
//
//        api.getMppItem("430", new Callback<DataListParcelable>() {
//            @Override
//            public void success(DataListParcelable dataListParcelable, Response response) {
//
//              //  itemlist = new ArrayList<DataParcelable>();
//                itemlist.addAll(dataListParcelable.getItemcollection());
//                Log.e("data","--"+itemlist.size());
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                error.printStackTrace();
//            }
//        });
//    }


}
