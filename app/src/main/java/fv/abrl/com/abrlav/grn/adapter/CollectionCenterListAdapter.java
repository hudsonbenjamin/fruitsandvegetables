package fv.abrl.com.abrlav.grn.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.grn.paracelable.$value;
import fv.abrl.com.abrlav.grn.paracelable.GetCollectionCenterListParcelable;

/**
 * Created by Hudson on 7/18/2018.
 */

public class CollectionCenterListAdapter extends BaseAdapter {
    private Context mContext;

    ArrayList<GetCollectionCenterListParcelable> realarrayList;


    public CollectionCenterListAdapter(Context c, ArrayList<GetCollectionCenterListParcelable> vendornamelist ) {
        mContext = c;
        this.realarrayList = vendornamelist;

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return realarrayList.size();
    }

    @Override
    public GetCollectionCenterListParcelable getItem(int position) {
        // TODO Auto-generated method stub
        return realarrayList.get(position);

    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub

        return position;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        final GetCollectionCenterListParcelable retrivedata = realarrayList.get(position);
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);
            grid = inflater.inflate(R.layout.list_item, null);


            final TextView product_name = (TextView)  grid.findViewById(R.id.product_name);

            product_name.setText(""+retrivedata.getCCNAME());




        } else {
            grid = (View) convertView;
        }



        return grid;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }






}