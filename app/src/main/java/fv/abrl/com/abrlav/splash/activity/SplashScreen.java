package fv.abrl.com.abrlav.splash.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.home.activity.SplashActivity;
import fv.abrl.com.abrlav.login.activity.LoginActivity;

/**
 * Created by Hudson on 7/6/2018.
 */

public class SplashScreen extends Activity {

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        sharedPref = getSharedPreferences("CallHippoSharedPreference", 0);
        editor = sharedPref.edit();

        if (sharedPref.getString("username", "default").equals("default")) {

            Intent i = new Intent(SplashScreen.this, LoginActivity.class);
            startActivity(i);

        }else{
            Intent i = new Intent(SplashScreen.this, HomeActivity.class);
            startActivity(i);
      }

    }
}
