
package fv.abrl.com.abrlav.grn.paracelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VendorItemSubmitParacelable {

    @SerializedName("$id")
    @Expose
    private String $id;

    @SerializedName("submitinvoicevalues")
    @Expose
    private String assigninvoicegrnresult;


    @SerializedName("grnnumber")
    @Expose
    private String grnnumber;


    public String getGrnnumber() {
        return grnnumber;
    }

    public void setGrnnumber(String grnnumber) {
        this.grnnumber = grnnumber;
    }





    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getAssigninvoicegrnresult() {
        return assigninvoicegrnresult;
    }

    public void setAssigninvoicegrnresult(String assigninvoicegrnresult) {
        this.assigninvoicegrnresult = assigninvoicegrnresult;
    }

}
