package fv.abrl.com.abrlav.picking.store.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.wang.avi.AVLoadingIndicatorView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.utils.CommonFunction;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.home.fragment.HomeFragment;
import fv.abrl.com.abrlav.picking.fragment.PickingMenuFragment;
import fv.abrl.com.abrlav.picking.store.adapter.PickbyStoreItemCompleteAdapterRv;
import fv.abrl.com.abrlav.picking.store.paracelable.PickByStoreCompleteDataParacelable;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/14/2018.
 */

public class PickByStoreFragmentComplete extends Fragment {



    @BindView(R.id.pickbystorecompleteSubmit)
    TextView pickbystorecompleteSubmit;

    @BindView(R.id.selectedItemProductlist)
    RecyclerView selectedItemProductlist;

    @BindView(R.id.storenameCode)
    TextView storenameCode;

    @BindView(R.id.collectionCenterName)
    TextView collectionCenterName;

    @BindView(R.id.pickbystoreComplete)
    TextView pickbystoreComplete;

    @BindView(R.id.clearSearchText)
    ImageView clearSearchText;

    @BindView(R.id.indicator)
    AVLoadingIndicatorView indicator;

    @BindView(R.id.pickingbyRouteFinalSearch)
    EditText pickingbyRouteFinalSearch;

    private Unbinder unbinder;
    Fragment fragment;

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    PickbyStoreItemCompleteAdapterRv mAdapter;

    public PickByStoreFragmentComplete() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        sharedPref              = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                  = sharedPref.edit();

        try{
            ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                    .getColor(R.color.bottombar)));
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "Pick By Store" + "</font>"));

            HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
            if (Build.VERSION.SDK_INT >= 21) {
                getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
            }
        }catch (Exception e){
            e.printStackTrace();
        }



        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pickbystorefragmentcomplete, container, false);
        unbinder = ButterKnife.bind(this, view);

        try{

            mAdapter                                    =       new PickbyStoreItemCompleteAdapterRv(getActivity(), PickbyStoreFragmentSecond.selectedItem);
            RecyclerView.LayoutManager mLayoutManager   =       new LinearLayoutManager(getActivity());
            selectedItemProductlist.setLayoutManager(mLayoutManager);
            selectedItemProductlist.setItemAnimator(new DefaultItemAnimator());
            selectedItemProductlist.setAdapter(mAdapter);


        }catch (Exception e){
            e.printStackTrace();
        }

        try{
            if (!sharedPref.getString("Storename","default").equals("default")){
                storenameCode.setText(""+sharedPref.getString("Storename","default")+" - "+sharedPref.getString("storecode","default"));
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pickbystorecompleteSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CommonFunction.isNetworkAvailable(getActivity())) {
                    showDialog(getActivity(),"Do you wish to continue?");
                }else{
                    Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                }

            }
        });

        try{
            if(!sharedPref.getString("collectioncentername","default").equals("default")){
                collectionCenterName.setText(""+sharedPref.getString("collectioncentername","default"));
            }else{
                collectionCenterName.setText("Collection Center Name");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        pickbystoreComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new PickingMenuFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer,fragment);
                transaction.commit();
            }
        });

        clearSearchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickingbyRouteFinalSearch.setText("");
            }
        });

        pickingbyRouteFinalSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

                mAdapter.getFilter().filter(arg0);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {


                mAdapter.getFilter().filter(arg0);

            }
        });

    }
    private void submitbystore(String result) {



        indicator.setVisibility(View.VISIBLE);


        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);

        api.submitpickbystore(result,new Callback<PickByStoreCompleteDataParacelable>()

        {
            @Override
            public void success(PickByStoreCompleteDataParacelable getccParacables, Response response) {

                try{
                    if (getccParacables.get$values().get(0).getSubmitPickByStoreresultvalue().equals("true")){
                        Toast.makeText(getActivity(),"Successfully Submitted!",Toast.LENGTH_SHORT).show();
                        fragment = new HomeFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.homeContainer,fragment);
                        transaction.commit();
                    }else{
                        Toast.makeText(getActivity(),"failed",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    Log.e("error","--"+e.getMessage());
                    Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                indicator.setVisibility(View.GONE);

            }

            @Override
            public void failure(RetrofitError error) {
                indicator.setVisibility(View.GONE);
                Log.e("error-1","--"+error.getMessage());
                Toast.makeText(getActivity(),"No Connectivity",Toast.LENGTH_SHORT).show();
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }

    public void showDialog(Activity activity, String msg){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_customsubmit);

        TextView text = (TextView) dialog.findViewById(R.id.customDialogMessage);
        text.setText(msg);

        TextView customSubmit = (TextView) dialog.findViewById(R.id.customSubmit);
        ImageView closeDialog = (ImageView) dialog.findViewById(R.id.closeDialog);
        TextView customDraft = (TextView) dialog.findViewById(R.id.customDraft);

        customDraft.setText("Cancel");

        customSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                Gson gson = new GsonBuilder().create();
                JsonArray myCustomArray = gson.toJsonTree(PickbyStoreFragmentSecond.selectedItem).getAsJsonArray();
                submitbystore(myCustomArray.toString());

            }
        });

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        customDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }



}