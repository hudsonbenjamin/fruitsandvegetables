package fv.abrl.com.abrlav.common.indexscroller;

import java.util.HashMap;

public interface FastScrollRecyclerViewInterface {
    public HashMap<String,Integer> getMapIndex();
}
