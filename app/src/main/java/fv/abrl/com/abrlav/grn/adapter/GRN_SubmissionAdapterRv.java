package fv.abrl.com.abrlav.grn.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.grn.paracelable.GrnItemParacelable;

public class GRN_SubmissionAdapterRv extends RecyclerView.Adapter<GRN_SubmissionAdapterRv.MyViewHolder>
        implements Filterable, SectionIndexer {
    private Context mContext;
    
    ArrayList<GrnItemParacelable> arrayList;
    ArrayList<GrnItemParacelable> filteredarraylist;
    private ArrayList<Integer> mSectionPositions;

    private GrnItemParacelable getItem(int position) {
        return filteredarraylist.get(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView finalentryCheckBox;
        TextView grnitemViewExpand;
        TextView itemCode;
        TextView Mpp;
        LinearLayout grnitemViewDetails;
        TextView grnquantity;
        TextView grnCost;
        
        
        public MyViewHolder(View grid) {
            super(grid);

            finalentryCheckBox       = (TextView) grid.findViewById(R.id.grnitemselectionCB);
            grnitemViewExpand       = (TextView)  grid.findViewById(R.id.grnitemViewExpand);
            itemCode                 = (TextView)  grid.findViewById(R.id.itemCode);
            Mpp                     = (TextView)  grid.findViewById(R.id.grnItemSelectionMpp);
            grnitemViewDetails      = (LinearLayout) grid.findViewById(R.id.grnitemViewDetails);
            grnquantity             = (TextView) grid.findViewById(R.id.grnquantity);
            grnCost                  = (TextView) grid.findViewById(R.id.grnCost);


        }

    }


    public GRN_SubmissionAdapterRv(Context c, ArrayList<GrnItemParacelable> arrayList ) {
        mContext = c;
        this.arrayList = arrayList;
        this.filteredarraylist = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_grn_submission, parent, false);



        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.grnitemViewExpand.setVisibility(View.INVISIBLE);
        holder.grnitemViewDetails.setVisibility(View.VISIBLE);



        holder.finalentryCheckBox.setText(""+filteredarraylist.get(position).getITEMDESC()+" ("+filteredarraylist.get(position).getSTANDARDUOM()+")");
        holder.Mpp.setText(""+filteredarraylist.get(position).getMPP());
        holder.grnquantity.setText(""+filteredarraylist.get(position).getGrnQuantity());
        holder.grnCost.setText(""+filteredarraylist.get(position).getGrnCost());
        holder.itemCode.setText("Item Code : "+filteredarraylist.get(position).getITEM());

//        holder.grnquantity.setKeyListener(DigitsKeyListener.getInstance(true,true));
//        holder.grnquantity.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                try{
//                    if(s.length()>0&&holder.grnCost.getText().toString().trim().length() > 0){
//                        filteredarraylist.get(position).setCheckedItem(1);
//                        filteredarraylist.get(position).setGrnQuantity(Integer.valueOf(""+holder.grnquantity.getText().toString()));
//
//
//                    }else{
//                        filteredarraylist.get(position).setCheckedItem(0);
//                        filteredarraylist.get(position).setGrnQuantity(filteredarraylist.get(position).getGrnQuantity());
//
//                    }
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//
//
//
//            }
//        });
//        holder.grnCost.setKeyListener(DigitsKeyListener.getInstance(true,true));
//        holder.grnCost.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                try{
//                    if(s.length()>0&&holder.grnquantity.getText().toString().trim().length() > 0){
//                        filteredarraylist.get(position).setCheckedItem(1);
//                        filteredarraylist.get(position).setGrnCost(Integer.valueOf(""+holder.grnCost.getText().toString()));
//
//                    }else{
//                        filteredarraylist.get(position).setCheckedItem(0);
//                        filteredarraylist.get(position).setGrnCost(filteredarraylist.get(position).getGrnCost());
//
//                    }
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//
//
//            }
//        });
  
    }
    @Override
    public int getItemCount() {
        return filteredarraylist.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredarraylist = arrayList;
                } else {
                    ArrayList<GrnItemParacelable> filteredList = new ArrayList<>();
                    for (GrnItemParacelable row : arrayList) {
                        if (row.getITEMDESC().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredarraylist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredarraylist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredarraylist = (ArrayList<GrnItemParacelable>) filterResults.values;

                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>(26);
        mSectionPositions = new ArrayList<>(26);
        for (int i = 0, size = filteredarraylist.size(); i < size; i++) {
            String section = String.valueOf(filteredarraylist.get(i).getITEMDESC().charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mSectionPositions.get(sectionIndex);
    }

    public long getItemId(int position) {
        return position;
    }



}