
package fv.abrl.com.abrlav.login.parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginDataItemParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("EmpId")
    @Expose
    private Integer empId;
    @SerializedName("Employee_Name")
    @Expose
    private String employeeName;
    @SerializedName("role")
    @Expose
    private Integer role;
    @SerializedName("MODULE_ID")
    @Expose
    private String mODULEID;
    @SerializedName("ROLE_NAME")
    @Expose
    private String rOLENAME;
    @SerializedName("INITIATOR")
    @Expose
    private String iNITIATOR;
    @SerializedName("APPROVER")
    @Expose
    private String aPPROVER;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public Integer getEmpId() {
        return empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getMODULEID() {
        return mODULEID;
    }

    public void setMODULEID(String mODULEID) {
        this.mODULEID = mODULEID;
    }

    public String getROLENAME() {
        return rOLENAME;
    }

    public void setROLENAME(String rOLENAME) {
        this.rOLENAME = rOLENAME;
    }

    public String getINITIATOR() {
        return iNITIATOR;
    }

    public void setINITIATOR(String iNITIATOR) {
        this.iNITIATOR = iNITIATOR;
    }

    public String getAPPROVER() {
        return aPPROVER;
    }

    public void setAPPROVER(String aPPROVER) {
        this.aPPROVER = aPPROVER;
    }

}
