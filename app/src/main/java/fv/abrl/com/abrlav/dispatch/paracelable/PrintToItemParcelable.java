
package fv.abrl.com.abrlav.dispatch.paracelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrintToItemParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("URL")
    @Expose
    private String uRL;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getURL() {
        return uRL;
    }

    public void setURL(String uRL) {
        this.uRL = uRL;
    }

}
