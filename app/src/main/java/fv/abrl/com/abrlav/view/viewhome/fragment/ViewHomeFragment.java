package fv.abrl.com.abrlav.view.viewhome.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.utils.CommonFunction;
import fv.abrl.com.abrlav.dispatch.fragment.SelectStoreFragment;
import fv.abrl.com.abrlav.grn.fragment.GrnVendorSelectionFragment;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.home.adapter.ImageAdapterGridView;
import fv.abrl.com.abrlav.home.fragment.HomeFragment;
import fv.abrl.com.abrlav.mpp.fragment.MppApprovalFragment;
import fv.abrl.com.abrlav.mpp.fragment.MppEntryFragment;
import fv.abrl.com.abrlav.picking.fragment.PickingMenuFragment;
import fv.abrl.com.abrlav.view.dispatch.fragment.Dispatch_ViewFragment;
import fv.abrl.com.abrlav.view.grn.fragment.GRN_ViewFragment;
import fv.abrl.com.abrlav.view.mpp.fragment.MppViewFragment;
import fv.abrl.com.abrlav.view.picking.menu.fragment.PickingViewMenuFragment;

import static fv.abrl.com.abrlav.R.drawable.menu_icon_white_resize;

/**
 * Created by Hudson on 7/6/2018.
 */

public class ViewHomeFragment extends Fragment {



    @BindView(R.id.gridviewMenu)
     GridView gridviewMenu;

    @BindView(R.id.viewModule)
    TextView viewModule;


    private Fragment fragment;
    private Unbinder unbinder;
    private View view;
    private int[] imageIDs = {
            R.drawable.mpp,
            R.drawable.creategrn,
            R.drawable.dispatch
    };

    private String[] section = {
            "MPP",
            "GRN",
            "DISPATCH",

    } ;



    @BindView(R.id.welcomeUser)
    TextView welcomeUser;


    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;


    public ViewHomeFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view                    =  inflater.inflate(R.layout.fragment_home, container, false);
         unbinder                =  ButterKnife.bind(this,view);

        sharedPref               = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                   = sharedPref.edit();

        viewModule.setVisibility(View.VISIBLE);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
               .getColor(R.color.colorPrimary)));
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(" ");
        HomeActivity.toolbar.setNavigationIcon(menu_icon_white_resize);
        if (Build.VERSION.SDK_INT >= 21) {
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        try{
            welcomeUser.setText("Welcome "+sharedPref.getString("username","username"));
        }catch (Exception e){
            e.printStackTrace();
        }


        gridviewMenu.setAdapter(new ImageAdapterGridView(getActivity(), section, imageIDs));




        gridviewMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent,
                                    View v, int position, long id) {

                if(position ==0){
                    fragment = new MppViewFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.homeContainer,fragment);
                    transaction.commit();
                }else if(position==1){
                    fragment = new GRN_ViewFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.homeContainer,fragment);
                    transaction.commit();
                }else if(position ==2){
                    fragment = new Dispatch_ViewFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.homeContainer,fragment);
                    transaction.commit();
                }


            }
        });

            }



}

