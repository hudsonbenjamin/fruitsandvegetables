package fv.abrl.com.abrlav.picking.store.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreItemListParacelable;

public class PickbyStoreItemCompleteAdapterRv  extends RecyclerView.Adapter<PickbyStoreItemCompleteAdapterRv.MyViewHolder> implements Filterable {

    private Context mContext;
    ArrayList<PickingStoreItemListParacelable> realarrayList;
    ArrayList<PickingStoreItemListParacelable> arrayList;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView productname,Vendornamequantity;

        public MyViewHolder(View grid) {
            super(grid);

            productname             =   (TextView)  grid.findViewById(R.id.productname);
             Vendornamequantity     =   (TextView)  grid.findViewById(R.id.Vendornamequantity);


        }

    }

    public PickbyStoreItemCompleteAdapterRv(Context c, ArrayList<PickingStoreItemListParacelable> productlist ) {
        mContext = c;
        this.realarrayList = productlist;
        this.arrayList          =   productlist;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_pickbystorefragmentcomplete, parent, false);


        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.productname.setText(""+realarrayList.get(position).getITEMDESC()+" ("+realarrayList.get(position).getSTANDARDUOM()+")");
        holder.Vendornamequantity.setText("Quantity : "+realarrayList.get(position).getQuantity());
    }

    @Override
    public int getItemCount() {
        return realarrayList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    realarrayList = arrayList;
                } else {
                    ArrayList<PickingStoreItemListParacelable> filteredList = new ArrayList<>();
                    for (PickingStoreItemListParacelable row : arrayList) {
                        if (row.getITEMDESC().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    realarrayList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = realarrayList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                realarrayList = (ArrayList<PickingStoreItemListParacelable>) filterResults.values;

                notifyDataSetChanged();
            }
        };
    }
}
