package fv.abrl.com.abrlav.common.interfaces;

import java.util.Map;

import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.dispatch.paracelable.DispatchGenerateToData;
import fv.abrl.com.abrlav.dispatch.paracelable.DispatchItemDataParacelable;
import fv.abrl.com.abrlav.dispatch.paracelable.PrintToParcelable;
import fv.abrl.com.abrlav.dispatch.paracelable.ToNumberDataParcelable;
import fv.abrl.com.abrlav.grn.paracelable.ConfirmGRNDataParacelable;
import fv.abrl.com.abrlav.grn.paracelable.GetCollectionCenterDataParacelable;
import fv.abrl.com.abrlav.grn.paracelable.GetGRNDataParacable;
import fv.abrl.com.abrlav.grn.paracelable.GetVendorDataParacable;
import fv.abrl.com.abrlav.grn.paracelable.VendorDataSubmitParacelable;
import fv.abrl.com.abrlav.login.parcelable.LoginDataModuleParcelable;
import fv.abrl.com.abrlav.login.parcelable.LoginDataparcelable;
import fv.abrl.com.abrlav.mpp.paracable.MPPApprovalItemDataParcelable;
import fv.abrl.com.abrlav.mpp.paracable.MPPEntryDraftDataParcelable;
import fv.abrl.com.abrlav.mpp.paracable.MPPEntrySubmitDataParcelable;
import fv.abrl.com.abrlav.mpp.paracable.MPPFinalSubmitResponseDataParcelable;
import fv.abrl.com.abrlav.mpp.paracable.MPPHODApprovalDataParcelable;
import fv.abrl.com.abrlav.mpp.paracable.MPPHODRejectDataParcelable;
import fv.abrl.com.abrlav.mpp.paracable.MPPItemDataParcelable;
import fv.abrl.com.abrlav.picking.route.parcelable.PickByRouteSubmitResultParcelable;
import fv.abrl.com.abrlav.picking.route.parcelable.PickingRouteDataParcelable;
import fv.abrl.com.abrlav.picking.route.parcelable.PickingRouteItemDataParcelable;
import fv.abrl.com.abrlav.picking.store.paracelable.PickByStoreCompleteDataParacelable;
import fv.abrl.com.abrlav.picking.store.paracelable.PickByStoreresult;
import fv.abrl.com.abrlav.picking.store.paracelable.PickbyStoreSubmitParacelable;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreDataParacable;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreItemDataParacable;
import fv.abrl.com.abrlav.view.dispatch.parcelable.DispatchInnerViewParcelable;
import fv.abrl.com.abrlav.view.dispatch.parcelable.DispatchViewStoresParcelable;
import fv.abrl.com.abrlav.view.grn.parcelable.GRNViewApprovalParcelable;
import fv.abrl.com.abrlav.view.grn.parcelable.GRNViewPendingApprovalParcelable;
import fv.abrl.com.abrlav.view.mpp.parcelable.MPPViewDataParcelable;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Query;
import retrofit.http.QueryMap;

/**
 * Created by Hudson on 7/10/2018.
 */

public interface iHttpApiCall {


    @Headers("Content-Type:application/json")
    @GET("/GRN_GetVendorsbyCC")
    void getVendorlist(@Query("CC") String id, Callback<GetVendorDataParacable> response);

    @Headers("Content-Type:application/json")
    @GET("/Common_GetCC")
    void getCollectionList(Callback<GetCollectionCenterDataParacelable> response);

    @Headers("Content-Type:application/json")
    @GET("/GRN_GetItemsbyVendor")
    void getitembyvendor(@Query("Vendor") String vendorcode, Callback<GetGRNDataParacable> response);

    @Headers("Content-Type:application/json")
    @GET("/Picking_GetStoresbyCC")
    void getstorelist(@Query("CC") String vendorcode, Callback<PickingStoreDataParacable> response);

    @Headers("Content-Type:application/json")
    @GET("/Dispatch_GetStoresbyCC")
    void getDispatchstorelist(@Query("CC") String vendorcode, Callback<PickingStoreDataParacable> response);

    @Headers("Content-Type:application/json")
    @GET("/Picking_GetRoutebyCC")
    void getRouteList(@Query("CC") String vendorcode, Callback<PickingRouteDataParcelable> response);

    @Headers("Content-Type:application/json")
    @GET("/Picking_GetItemsbyStore")
    void getproductlist(@Query("store") String vendorcode, Callback<PickingStoreItemDataParacable> response);

    @Headers("Content-Type:application/json")
    @GET("/Dispatch_GetItemsbyStore")
    void getstoreinnerproductlist(@Query("store") String store, Callback<DispatchItemDataParacelable> response);


    @FormUrlEncoded
    @POST("/GRN_SubmitGRN")
    void submitGrnItem(@Field("data") String data, Callback<ConfirmGRNDataParacelable> response);

    @FormUrlEncoded
    @POST("/GRN_DraftGRN")
    void DrfatGrnItem(@Field("data") String data, Callback<ConfirmGRNDataParacelable> response);

    @Headers("Content-Type:application/json")
    @GET("/GRN_SubmitInvoice")
    void submitVendorDetails(@Query("CC") String cc,@Query("Supplier") int Suppliercode,@Query("Sup_name") String SupName,@Query("invoiceno") String invoiceno, Callback<VendorDataSubmitParacelable> response);

    @FormUrlEncoded
    @POST("/Picking_SubmitPickbyStore")
    void submitpickbystore(@Field("data") String data,Callback<PickByStoreCompleteDataParacelable> response);

    @Headers("Content-Type:application/json")
    @GET("/MPP_FinalApprovalItems")
    void getmppapprovalitem(@Query("CC") String CC,@Query("role") String role,Callback<MPPApprovalItemDataParcelable> response);

    @FormUrlEncoded
    @POST("/Dispatch_GenerateTo")
    void generateTo(@Field("data") String CC, Callback<ToNumberDataParcelable> response);

    @Headers("Content-Type:application/json")
    @GET("/PrintGRN")
    void downloadgrnpdf(@Query("currentusercc") String currentusercc, Callback<DispatchGenerateToData> response);

    @Headers("Content-Type:application/json")
    @GET("/MPP_GetItemsbyCC")
    void getMppItemList(@Query("CC") String CC, Callback<MPPItemDataParcelable> response);

    @FormUrlEncoded
    @POST("/MPP_DraftMPP")
    void draftEntryMPP(@Field("data") String data, Callback<MPPEntryDraftDataParcelable> response);

    @FormUrlEncoded
    @POST("/MPP_SubmitMPP")
    void submitEntryMPP(@Field("data") String data, Callback<MPPEntrySubmitDataParcelable> response);

    @FormUrlEncoded
    @POST("/MPP_FinalMPPSubmit")
    void submitFinalMPP(@Field("data") String data, Callback<MPPFinalSubmitResponseDataParcelable> response);
    @FormUrlEncoded
    @POST("/MPP_FinalMPPDraft")
    void draftFinalMPP(@Field("data") String data, Callback<MPPFinalSubmitResponseDataParcelable> response);


    @FormUrlEncoded
    @POST("/MPP_ApproveMPP")
    void MPPHodApproval(@Field("data") String data, Callback<MPPApprovalItemDataParcelable> response);

    @FormUrlEncoded
    @POST("/MPP_RejectMPP")
    void MPPHodReject(@Field("data") String data, Callback<MPPApprovalItemDataParcelable> response);

    @Headers("Content-Type:application/json")
    @GET("/Common_Login")
    void Login(@Query("username") String username,@Query("password") String password, Callback<LoginDataModuleParcelable> response);


    @Headers("Content-Type:application/json")
    @GET("/Picking_GetItemsbyRoute")
    void getItemByRoute(@Query("Route") int route, Callback<PickingRouteItemDataParcelable> response);

    @Headers("Content-Type:application/json")
    @GET("/Picking_GetStoresbyItem")
    void pickinggetStorelistbyItemSelect(@Query("Item") int route, Callback<PickingStoreDataParacable> response);

    @FormUrlEncoded
    @POST("/Picking_SubmitPickbyRoute")
    void pickbyrouteSubmit(@Field("data") String data, Callback<PickByRouteSubmitResultParcelable> response);


    @Headers("Content-Type:application/json")
    @GET("/View_GRNView")
    void getGRNViewList(@Query("status") String status, Callback<GRNViewApprovalParcelable> response);

    @Headers("Content-Type:application/json")
    @GET("/View_MPPViewbyStatus")
    void getMPPViewList(@Query("status") String status, Callback<MPPViewDataParcelable> response);


    @Headers("Content-Type:application/json")
    @GET("/View_DispatchViewStores")
    void getDispatchViewList(@Query("CC") String CC, Callback<DispatchViewStoresParcelable> response);



    @Headers("Content-Type:application/json")
    @GET("/View_DispatchViewItems")
    void getDispatchViewinnerproductlist(@Query("store") String store, Callback<DispatchInnerViewParcelable> response);


    @Headers("Content-Type:application/json")
    @GET("/View_GRNApprovalPending")
    void getGRNApprovalItem(@Query("GRN_Number") int grnnumber, Callback<GRNViewPendingApprovalParcelable> response);


    @FormUrlEncoded
    @POST("/GRN_ApprovalSubmit")
    void sendgrnapproval(@Field("data") String data, Callback<GRNViewPendingApprovalParcelable> response);


    @Headers("Content-Type:application/json")
    @GET("/Dispatch_PrintDispatch")
    void generateToPDF(@Query("Store1") int Store1,@Query("CC") int ccode,Callback<PrintToParcelable> response);

    @Headers("Content-Type:application/json")
    @GET("/GRN_PrintGRN")
    void generateGRNPDF(@Query("currentusercc") int ccode,@Query("suppliercode") int suppliercode,@Query("Vendor_Name") String Vendor_Name, @Query("Vendor_Code") String Vendor_Code,@Query("GRN_Number") String GRN_Number,@Query("Invoice_Number") String Invoice_Number,Callback<PrintToParcelable> response);


}



