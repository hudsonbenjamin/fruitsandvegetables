
package fv.abrl.com.abrlav.view.grn.parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GRNViewApprovalParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<GRNApprovalItemListParcelable> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<GRNApprovalItemListParcelable> get$values() {
        return $values;
    }

    public void set$values(List<GRNApprovalItemListParcelable> $values) {
        this.$values = $values;
    }

}
