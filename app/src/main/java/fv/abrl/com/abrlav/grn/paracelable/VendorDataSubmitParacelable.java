
package fv.abrl.com.abrlav.grn.paracelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VendorDataSubmitParacelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<VendorItemSubmitParacelable> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<VendorItemSubmitParacelable> get$values() {
        return $values;
    }

    public void set$values(List<VendorItemSubmitParacelable> $values) {
        this.$values = $values;
    }

}
