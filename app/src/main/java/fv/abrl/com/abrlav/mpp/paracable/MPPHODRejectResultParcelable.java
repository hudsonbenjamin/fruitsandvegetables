
package fv.abrl.com.abrlav.mpp.paracable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MPPHODRejectResultParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("rejectmpp")
    @Expose
    private String rejectmpp;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getRejectmpp() {
        return rejectmpp;
    }

    public void setRejectmpp(String rejectmpp) {
        this.rejectmpp = rejectmpp;
    }

}
