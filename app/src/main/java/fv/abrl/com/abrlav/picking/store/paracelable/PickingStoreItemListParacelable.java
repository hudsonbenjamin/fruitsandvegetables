
package fv.abrl.com.abrlav.picking.store.paracelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickingStoreItemListParacelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("ITEM")
    @Expose
    private int iTEM;
    @SerializedName("ITEM_DESC")
    @Expose
    private String iTEMDESC;

    @SerializedName("SOH")
    @Expose
    private int sOH;

    @SerializedName("STANDARD_UOM")
    @Expose
    private String sTANDARDUOM;

    private int checkitem =0;

    @SerializedName("Quantity")
    @Expose
    private int quantity;

    @SerializedName("MPP")
    @Expose
    private int mPP;


    @SerializedName("STORE_NAME")
    @Expose
    private String storename;


    @SerializedName("STORE")
    @Expose
    private String storecode;

    public int getCC_Code() {
        return CC_Code;
    }

    public void setCC_Code(int CC_Code) {
        this.CC_Code = CC_Code;
    }

    @SerializedName("CC_Code")
    @Expose
    private int CC_Code;

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    public int getCheckitem() {
        return checkitem;
    }

    public void setCheckitem(int checkitem) {
        this.checkitem = checkitem;
    }




    public String getStorecode() {
        return storecode;
    }

    public void setStorecode(String storecode) {
        this.storecode = storecode;
    }


    public PickingStoreItemListParacelable( String id,String productname,int itemcode,int productQuantity,int checkitem,String standard,String storecode,String storename,int SOH,int ccode) {
        this.quantity =productQuantity;
        this.iTEMDESC =productname;
        this.iTEM =itemcode;
        this.$id =id;
        this.checkitem = checkitem;
        this.sTANDARDUOM = standard;
        this.storecode = storecode;
        this.storename = storename;
        this.sOH = SOH;
        this.CC_Code = ccode;


    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getMPP() {
        return mPP;
    }

    public void setMPP(int mPP) {
        this.mPP = mPP;
    }




    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getITEM() {
        return iTEM;
    }

    public void setITEM(int iTEM) {
        this.iTEM = iTEM;
    }

    public String getITEMDESC() {
        return iTEMDESC;
    }

    public void setITEMDESC(String iTEMDESC) {
        this.iTEMDESC = iTEMDESC;
    }

    public int getSOH() {
        return sOH;
    }

    public void setSOH(int sOH) {
        this.sOH = sOH;
    }

    public String getSTANDARDUOM() {
        return sTANDARDUOM;
    }

    public void setSTANDARDUOM(String sTANDARDUOM) {
        this.sTANDARDUOM = sTANDARDUOM;
    }

}
