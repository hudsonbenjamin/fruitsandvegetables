package fv.abrl.com.abrlav.grn.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.severapi.Utils;
import fv.abrl.com.abrlav.common.utils.CommonFunction;
import fv.abrl.com.abrlav.grn.adapter.GrnItemSelectionAdapterRv;
import fv.abrl.com.abrlav.grn.paracelable.GetGRNDataParacable;
import fv.abrl.com.abrlav.grn.paracelable.GrnItemParacelable;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.home.fragment.HomeFragment;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/10/2018.
 */

public class GrnItemSelectionFragment extends Fragment {



    @BindView(R.id.summaryViewExpand)
     TextView summaryViewExpand;



    @BindView(R.id.grnItemSelectionSubmit)
     TextView grnItemSelectionSubmit;

    @BindView(R.id.grnSummaryview)
     LinearLayout grnSummaryview;


    private Unbinder unbinder;
    private Fragment fragment;

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    @BindView(R.id.CollectionCenter)
    TextView CollectionCenter;

    @BindView(R.id.vendorName)
    TextView vendorName;

    @BindView(R.id.vendorcode)
    TextView vendorcode;

    @BindView(R.id.totalmoneyGrn)
    TextView totalmoneyGrn;




    @BindView(R.id.grnsumKg)
    TextView grnsumKg;

    @BindView(R.id.grnItemSearch)
    EditText grnItemSearch;

    @BindView(R.id.GrnitemListCancel)
    TextView GrnitemListCancel;

    @BindView(R.id.clearSearchText)

    ImageView  clearSearchText;

    @BindView(R.id.indicator)
    AVLoadingIndicatorView indicator;



    @BindView(R.id.grnitemSelectionListrv)
    IndexFastScrollRecyclerView grnitemSelectionListrv;


    public static ArrayList<GrnItemParacelable> grnarrayselectedlist  = new ArrayList<GrnItemParacelable>();
    public static ArrayList<GrnItemParacelable> grncancearrayselectedlist  = new ArrayList<GrnItemParacelable>();

    ArrayList<GrnItemParacelable>  grnitemarraylist                   = new ArrayList<GrnItemParacelable>();

    GrnItemSelectionAdapterRv  mAdapter;

     private double displaytotalcount = 0.00;
     private double displaytotalGrnKg = 0;

    public GrnItemSelectionFragment() {
        // Required empty public constructor
    }

    public static TextView grntotalItemCount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        sharedPref                  = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                      = sharedPref.edit();

            try{
                ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                        .getColor(R.color.bottombar)));

                ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "Item Selection" + "</font>"));
                HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
                if (Build.VERSION.SDK_INT >= 21) {
                    getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
                }
            }catch (Exception e){
                e.printStackTrace();
            }


        View view =  inflater.inflate(R.layout.fragment_grn_item_selection, container, false);
        unbinder = ButterKnife.bind(this,view);
        grntotalItemCount = (TextView) view.findViewById(R.id.grntotalItemCount);

        try{

            CollectionCenter.setText(""+sharedPref.getString("collectioncentername","0"));
            vendorName.setText(""+sharedPref.getString("VendorName","0"));
            vendorcode.setText(""+sharedPref.getString("VendorCode","0"));
        }catch (Exception e){
            e.printStackTrace();
        }





        mAdapter                =       new GrnItemSelectionAdapterRv(getActivity(), grnitemarraylist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        grnitemSelectionListrv.setLayoutManager(mLayoutManager);
        grnitemSelectionListrv.setItemAnimator(new DefaultItemAnimator());
        grnitemSelectionListrv.setItemViewCacheSize(grnitemarraylist.size());
        grnitemSelectionListrv.setIndexTextSize(12);
        grnitemSelectionListrv.setIndexBarColor("#ffffff");
        grnitemSelectionListrv.setIndexBarCornerRadius(0);
        grnitemSelectionListrv.setIndexbarMargin(0);
        grnitemSelectionListrv.setIndexbarWidth(40);
        grnitemSelectionListrv.setPreviewPadding(0);
        grnitemSelectionListrv.setIndexBarTextColor("#818181");
        grnitemSelectionListrv.setIndexBarVisibility(true);
        grnitemSelectionListrv.setIndexbarHighLateTextColor("#ed6c09");
        grnitemSelectionListrv.setIndexBarHighLateTextVisibility(true);
        grnitemSelectionListrv.setAdapter(mAdapter);


        try{
            vendorlist(sharedPref.getString("VendorCode","0"));
        }catch (Exception e){
            e.printStackTrace();
        }


        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        summaryViewExpand.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {




                if(grnSummaryview.getVisibility()==View.VISIBLE){
                    summaryViewExpand.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down));
                    grnSummaryview.setVisibility(View.GONE);
                }else {



                    try{
                        displaytotalcount =0;
                        displaytotalGrnKg =0;


                        for(int i =0;grnitemarraylist.size()>i;i++){
                            if(grnitemarraylist.get(i).getCheckedItem()== 1){
                                double totalmoney = grnitemarraylist.get(i).getGrnQuantity()*grnitemarraylist.get(i).getGrnCost();
                                displaytotalcount = displaytotalcount + totalmoney;

                                displaytotalGrnKg = displaytotalGrnKg+grnitemarraylist.get(i).getGrnQuantity();
                            }

                        }

                        totalmoneyGrn.setText(": "+displaytotalcount);
                        grnsumKg.setText(": "+displaytotalGrnKg);
                    }catch (Exception e){
                        e.printStackTrace();
                    }


                    summaryViewExpand.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_up));
                    grnSummaryview.setVisibility(View.VISIBLE);
                }


            }
        });

        clearSearchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                grnItemSearch.setText("");
            }
        });

        grnItemSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                try{
                    mAdapter.getFilter().filter(s);

                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onTextChanged(CharSequence s, int arg1, int arg2,
                                      int arg3) {
                try{
                    mAdapter.getFilter().filter(s);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });



        grnItemSelectionSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                grnarrayselectedlist.clear();
                grncancearrayselectedlist.clear();
                try{
                    for(int i=0;i <grnitemarraylist.size();i++){
                        if(grnitemarraylist.get(i).getCheckedItem()== 1){
                            if(grnitemarraylist.get(i).getGrnCost()>grnitemarraylist.get(i).getMPP()){
                                grnitemarraylist.get(i).setGrnStatus("Pending");
                            }else{
                                grnitemarraylist.get(i).setGrnStatus("Submitted");
                            }

                            GrnItemParacelable s1=new GrnItemParacelable(grnitemarraylist.get(i).getITEMDESC(),
                                    grnitemarraylist.get(i).getGrnCost(),
                                    grnitemarraylist.get(i).getGrnQuantity(),
                                    grnitemarraylist.get(i).getMPP(),
                                    grnitemarraylist.get(i).getCheckedItem(),
                                    grnitemarraylist.get(i).getSTANDARDUOM(),
                                    grnitemarraylist.get(i).getITEM(),
                                    sharedPref.getString("invoicenumber","default"),
                                    Integer.parseInt(sharedPref.getString("collectioncentercode","default")),
                                    sharedPref.getString("VendorName","default"),
                                    Integer.parseInt(sharedPref.getString("VendorCode","default")),
                                    ""+sharedPref.getString("VendorCode","0"),
                                    grnitemarraylist.get(i).getGrnStatus(),sharedPref.getString("grnnumber","default"));
                            grnarrayselectedlist.add(s1);
                            grncancearrayselectedlist.add(s1);
                        }else{
                            GrnItemParacelable s1=new GrnItemParacelable(grnitemarraylist.get(i).getITEMDESC(),
                                    grnitemarraylist.get(i).getGrnCost(),
                                    grnitemarraylist.get(i).getGrnQuantity(),
                                    grnitemarraylist.get(i).getMPP(),
                                    grnitemarraylist.get(i).getCheckedItem(),
                                    grnitemarraylist.get(i).getSTANDARDUOM(),
                                    grnitemarraylist.get(i).getITEM(),
                                    sharedPref.getString("invoicenumber","default"),
                                    430,
                                    sharedPref.getString("VendorName","default"),
                                    Integer.parseInt(sharedPref.getString("VendorCode","default")),
                                    ""+sharedPref.getString("VendorCode","0"),
                                    grnitemarraylist.get(i).getGrnStatus(),
                                    sharedPref.getString("grnnumber","default"));
                            grncancearrayselectedlist.add(s1);
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                displaytotalcount =0;
                displaytotalGrnKg =0;
                if(grnarrayselectedlist.size()>0){

                    if(CommonFunction.isNetworkAvailable(getActivity())) {
                        showDialog(getActivity(),"Do you wish to continue?");
                    }else{
                        Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                    }




                }else{
                    Toast.makeText(getActivity(),"Please Select atleast anyone item",Toast.LENGTH_SHORT).show();
                }


           }
       });

        GrnitemListCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragment = new HomeFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer,fragment);
                transaction.commit();
            }
        });

//        grnitemSelectionListrv.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//                View view = getActivity().getCurrentFocus();
//                if (view != null) {
//                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                }
//            }
//        });


    }
    private void vendorlist(String vendorcode) {

        indicator.setVisibility(View.VISIBLE);

        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getitembyvendor(vendorcode,new Callback<GetGRNDataParacable>()

        {
            @Override
            public void success(GetGRNDataParacable getccParacables, Response response) {

               try {
                   if (0 != getccParacables.get$values().size()) {

                       grnitemarraylist.addAll(getccParacables.get$values());

                       mAdapter.notifyDataSetChanged();
                    //   grnitemSelectionList.setAdapter(new GrnItemSelectionAdapter(getActivity(), grnitemarraylist));
                   } else {
                       Toast.makeText(getActivity(), "No Items Found", Toast.LENGTH_SHORT).show();
                   }
               }catch (Exception e){
                   e.printStackTrace();
               }

                indicator.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {

                indicator.setVisibility(View.GONE);
                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }

    public void showDialog(Activity activity, String msg){

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_customsubmit);

        TextView text = (TextView) dialog.findViewById(R.id.customDialogMessage);
        text.setText(msg);

        TextView customSubmit = (TextView) dialog.findViewById(R.id.customSubmit);
        ImageView closeDialog = (ImageView) dialog.findViewById(R.id.closeDialog);
        TextView customDraft = (TextView) dialog.findViewById(R.id.customDraft);

        customDraft.setText("Cancel");
        customSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                try{
                    for(int i =0;grnarrayselectedlist.size()>i;i++){
                        double totalmoney = grnarrayselectedlist.get(i).getGrnCost()*grnarrayselectedlist.get(i).getGrnQuantity();
                        displaytotalcount = displaytotalcount + totalmoney;
                        displaytotalGrnKg = displaytotalGrnKg+grnarrayselectedlist.get(i).getGrnQuantity();
                    }

                    totalmoneyGrn.setText(": "+displaytotalcount);
                    grnsumKg.setText(": "+displaytotalGrnKg);
                }catch (Exception e){
                    e.printStackTrace();
                }

                editor.putString("grnsumofkg",""+grnsumKg.getText().toString()).apply();
                editor.putString("grntotalamount",""+totalmoneyGrn.getText().toString()).apply();

                fragment = new GrnSubmissionFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer,fragment,"GrnSubmissionFragment");
                transaction.commit();

            }
        });

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        customDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();

    }
}
