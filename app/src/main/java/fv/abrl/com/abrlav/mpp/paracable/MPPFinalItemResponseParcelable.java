
package fv.abrl.com.abrlav.mpp.paracable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MPPFinalItemResponseParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("finalmppsubmit")
    @Expose
    private String finalmppsubmit;

    public String getFinalmppdraft() {
        return finalmppdraft;
    }

    public void setFinalmppdraft(String finalmppdraft) {
        this.finalmppdraft = finalmppdraft;
    }

    @SerializedName("finalmppdraft")
    @Expose
    private String finalmppdraft;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getFinalmppsubmit() {
        return finalmppsubmit;
    }

    public void setFinalmppsubmit(String finalmppsubmit) {
        this.finalmppsubmit = finalmppsubmit;
    }

}
