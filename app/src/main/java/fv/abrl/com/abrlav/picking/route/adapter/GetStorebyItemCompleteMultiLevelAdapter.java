package fv.abrl.com.abrlav.picking.route.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.SectionIndexer;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreListParacelable;

public class GetStorebyItemCompleteMultiLevelAdapter extends RecyclerView.Adapter<GetStorebyItemCompleteMultiLevelAdapter.MyViewHolder>
        implements  SectionIndexer {
    private Context mContext;


    private ArrayList<PickingStoreListParacelable> arrayList;
    ArrayList<PickingStoreListParacelable> filteredarraylist;
    private ArrayList<Integer> mSectionPositions;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView storeName2;
        EditText storeQunantityEt2;

        public MyViewHolder(View view) {
            super(view);

            storeName2         =  (TextView)  view.findViewById(R.id.storeName2);
            storeQunantityEt2  =  (EditText)view.findViewById(R.id.storeQunantityEt2);
        }

    }

    public GetStorebyItemCompleteMultiLevelAdapter(Context c, ArrayList<PickingStoreListParacelable> arrayList) {
        mContext = c;
        this.arrayList = arrayList;
        this.filteredarraylist = arrayList;



    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.getstorebyitemmultileveladapter, parent, false);



        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.storeName2.setText(""+filteredarraylist.get(position).getSTORENAME());

        if(filteredarraylist.get(position).getStoreQunantity()==0){

            holder.storeQunantityEt2.setText("");
        }else{

            holder.storeQunantityEt2.setText(""+filteredarraylist.get(position).getStoreQunantity());
        }



    }

    @Override
    public int getItemCount() {
        return filteredarraylist.size();
    }



    @Override
    public int getItemViewType(int position) {
        return position;
    }




    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>(26);
        mSectionPositions = new ArrayList<>(26);
        for (int i = 0, size = filteredarraylist.size(); i < size; i++) {
            String section = String.valueOf(filteredarraylist.get(i).getSTORENAME().charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mSectionPositions.get(sectionIndex);
    }


}
