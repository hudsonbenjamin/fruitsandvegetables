package fv.abrl.com.abrlav.common.interfaces;

import android.view.View;

/**
 * Created by Hudson on 7/20/2018.
 */

public interface ItemClickListener {
    void onClick(View view, int position);
}