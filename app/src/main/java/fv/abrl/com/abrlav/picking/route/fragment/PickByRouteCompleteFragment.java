package fv.abrl.com.abrlav.picking.route.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.home.fragment.HomeFragment;
import fv.abrl.com.abrlav.picking.route.adapter.PickingByRouteCompleteAdapter;
import fv.abrl.com.abrlav.picking.route.adapter.ProductItemListbySelectAdapter;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;

/**
 * Created by Hudson on 7/14/2018.
 */

public class PickByRouteCompleteFragment extends Fragment {


    @BindView(R.id.pickbyroutecompletesubmit)
    TextView pickbyitemcompletesubmit;

    @BindView(R.id.pickbyrouteItemlist)
    IndexFastScrollRecyclerView pickbyrouteItemlist;


    private Unbinder unbinder;
    Fragment fragment;
    PickingByRouteCompleteAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;


    public PickByRouteCompleteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

            try{
        ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.bottombar)));
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "Pick By Route" + "</font>"));

        HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
        if (Build.VERSION.SDK_INT >= 21) {
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
        }
            }
        catch (Exception e){
                e.printStackTrace();
            }

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pickbyroutecomplete, container, false);
        unbinder = ButterKnife.bind(this, view);


        mAdapter                =   new PickingByRouteCompleteAdapter(getActivity(), ProductItemListbySelectAdapter.loadWholeinnerlist);
        mLayoutManager          =   new LinearLayoutManager(getActivity());
        pickbyrouteItemlist.setLayoutManager(mLayoutManager);
        pickbyrouteItemlist.setItemAnimator(new DefaultItemAnimator());
        pickbyrouteItemlist.setItemViewCacheSize(ProductItemListbySelectAdapter.loadWholeinnerlist.size());
        pickbyrouteItemlist.setIndexTextSize(12);
        pickbyrouteItemlist.setIndexBarColor("#ffffff");
        pickbyrouteItemlist.setIndexBarCornerRadius(0);
        pickbyrouteItemlist.setIndexbarMargin(0);
        pickbyrouteItemlist.setIndexbarWidth(40);
        pickbyrouteItemlist.setPreviewPadding(0);
        pickbyrouteItemlist.setIndexBarTextColor("#818181");
        pickbyrouteItemlist.setIndexBarVisibility(true);
        pickbyrouteItemlist.setIndexbarHighLateTextColor("#ed6c09");
        pickbyrouteItemlist.setIndexBarHighLateTextVisibility(true);
        pickbyrouteItemlist.setAdapter(mAdapter);

        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pickbyitemcompletesubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new HomeFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer,fragment);
                transaction.commit();
            }
        });


    }
}