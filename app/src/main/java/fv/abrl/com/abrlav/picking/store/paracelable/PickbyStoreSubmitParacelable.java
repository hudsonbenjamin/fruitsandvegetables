
package fv.abrl.com.abrlav.picking.store.paracelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickbyStoreSubmitParacelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("PickByStoreresult")
    @Expose
    private PickByStoreresult pickByStoreresult;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public PickByStoreresult getPickByStoreresult() {
        return pickByStoreresult;
    }

    public void setPickByStoreresult(PickByStoreresult pickByStoreresult) {
        this.pickByStoreresult = pickByStoreresult;
    }

}
