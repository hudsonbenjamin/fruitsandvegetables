
package fv.abrl.com.abrlav.login.parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginDataparcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<LoginResultParcelable> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<LoginResultParcelable> get$values() {
        return $values;
    }

    public void set$values(List<LoginResultParcelable> $values) {
        this.$values = $values;
    }

}
