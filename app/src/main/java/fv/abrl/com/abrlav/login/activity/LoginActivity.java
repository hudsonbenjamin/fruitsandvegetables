package fv.abrl.com.abrlav.login.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.utils.CommonFunction;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.login.parcelable.LoginDataItemParcelable;
import fv.abrl.com.abrlav.login.parcelable.LoginDataModuleParcelable;
import fv.abrl.com.abrlav.login.parcelable.LoginDataparcelable;
import fv.abrl.com.abrlav.login.parcelable.LoginResultParcelable;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/6/2018.
 */

public class LoginActivity extends Activity {

    @BindView(R.id.signIn)
    TextView signIn;

    @BindView(R.id.userName)
    EditText userName;

    @BindView(R.id.userPassword)
    EditText userPassword;

    @BindView(R.id.indicator)
    AVLoadingIndicatorView indicator;

    private Unbinder unbinder;

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;



    ArrayList<LoginDataItemParcelable> arrayList = new ArrayList<LoginDataItemParcelable>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        unbinder                    =  ButterKnife.bind(this);
        sharedPref                  =  getSharedPreferences("CallHippoSharedPreference", 0);
        editor                      =  sharedPref.edit();



        if(!isStoragePermissionGranted())
        {

        }

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!userName.getText().toString().equals("")&&!userPassword.getText().toString().equals("")){
                    if(CommonFunction.isNetworkAvailable(LoginActivity.this)){
                        login(userName.getText().toString(),userPassword.getText().toString());
                    }else{
                        Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                    }

                }else{
                    Toast.makeText(getApplicationContext(),"All Fields are mandatory!",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void login(String username,String password) {


        indicator.setVisibility(View.VISIBLE);
        final RestAdapter adapter   =   new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api            =   adapter.create(iHttpApiCall.class);


        api.Login(username,password,new Callback<LoginDataModuleParcelable>()

        {
            @Override
            public void success(LoginDataModuleParcelable getccParacables, Response response) {

                try{
                    if(0 != getccParacables.get$values().size()){
                        arrayList.addAll(getccParacables.get$values());
                        for(int i=0;i<arrayList.size();i++){
                            editor.putString("username",getccParacables.get$values().get(i).getEmployeeName()).apply();
                            editor.putString("roletype",getccParacables.get$values().get(i).getROLENAME()).apply();
                            editor.putInt("role",getccParacables.get$values().get(i).getRole()).apply();

                            Log.e("module","--"+getccParacables.get$values().get(i).getMODULEID());
                            if(getccParacables.get$values().get(i).getMODULEID().equals("MPP")){

                            }else if(getccParacables.get$values().get(i).getMODULEID().equals("GRN")){

                            }

                        }

                        Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(i);

                    }else{
                        Toast.makeText(getApplicationContext(),"Invalid Credentials",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                indicator.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {
                indicator.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(),"Login Failed"+error.getMessage(),Toast.LENGTH_SHORT).show();

            }
        });

    }



    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {

            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                return false;
            }
        }
        else {
            return true;
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){


        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        System.exit(0);

    }

}
