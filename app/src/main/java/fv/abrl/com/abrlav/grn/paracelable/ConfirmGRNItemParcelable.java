
package fv.abrl.com.abrlav.grn.paracelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConfirmGRNItemParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;

    @SerializedName("submitgrn")
    @Expose
    private String assigngrnresult;

    @SerializedName("draftgrn")
    @Expose
    private String draftgrn;

    public String getDraftgrn() {
        return draftgrn;
    }

    public void setDraftgrn(String draftgrn) {
        this.draftgrn = draftgrn;
    }



    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getAssigngrnresult() {
        return assigngrnresult;
    }

    public void setAssigngrnresult(String assigngrnresult) {
        this.assigngrnresult = assigngrnresult;
    }

}
