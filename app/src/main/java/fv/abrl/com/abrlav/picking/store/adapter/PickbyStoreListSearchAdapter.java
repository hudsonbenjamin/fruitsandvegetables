package fv.abrl.com.abrlav.picking.store.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.SectionIndexer;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.ItemClickListener;
import fv.abrl.com.abrlav.grn.adapter.VendorNameListAdapter;
import fv.abrl.com.abrlav.grn.paracelable.$value;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreListParacelable;

/**
 * Created by Hudson on 7/20/2018.
 */

public class PickbyStoreListSearchAdapter extends RecyclerView.Adapter<PickbyStoreListSearchAdapter.MyViewHolder>
        implements Filterable, SectionIndexer {
    private Context mContext;

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;
    ArrayList<PickingStoreListParacelable> arrayList;
    public static  ArrayList<PickingStoreListParacelable> filteredarraylist;
    private ItemClickListener clickListener;
    private ArrayList<Integer> mSectionPositions;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView product_name;

        public MyViewHolder(View view) {
            super(view);

            product_name = (TextView)  view.findViewById(R.id.product_name);


        }

    }


    public PickbyStoreListSearchAdapter(Context c, ArrayList<PickingStoreListParacelable> arrayList) {
        mContext = c;
        this.arrayList = arrayList;
        this.filteredarraylist = arrayList;
        //setHasStableIds(true);

        sharedPref              = c.getSharedPreferences("CallHippoSharedPreference", 0);
        editor                  = sharedPref.edit();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);



        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final PickingStoreListParacelable retrivedata = filteredarraylist.get(position);
        holder.product_name.setText(""+retrivedata.getSTORENAME());
    }

    @Override
    public int getItemCount() {
        return filteredarraylist.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredarraylist = arrayList;
                } else {
                    ArrayList<PickingStoreListParacelable> filteredList = new ArrayList<>();
                    for (PickingStoreListParacelable row : arrayList) {
                        if (row.getSTORENAME().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredarraylist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredarraylist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredarraylist = (ArrayList<PickingStoreListParacelable>) filterResults.values;

                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>(26);
        mSectionPositions = new ArrayList<>(26);
        for (int i = 0, size = filteredarraylist.size(); i < size; i++) {
            String section = String.valueOf(filteredarraylist.get(i).getSTORENAME().charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mSectionPositions.get(sectionIndex);
    }
}
