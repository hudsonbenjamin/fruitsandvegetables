
package fv.abrl.com.abrlav.picking.route.parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickingRouteListParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("ROUTE_ID")
    @Expose
    private Integer rOUTEID;
    @SerializedName("ROUTE_NAME")
    @Expose
    private String rOUTENAME;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public Integer getROUTEID() {
        return rOUTEID;
    }

    public void setROUTEID(Integer rOUTEID) {
        this.rOUTEID = rOUTEID;
    }

    public String getROUTENAME() {
        return rOUTENAME;
    }

    public void setROUTENAME(String rOUTENAME) {
        this.rOUTENAME = rOUTENAME;
    }

}
