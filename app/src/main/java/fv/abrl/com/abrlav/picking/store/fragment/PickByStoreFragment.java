package fv.abrl.com.abrlav.picking.store.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.utils.RecyclerItemClickListener;
import fv.abrl.com.abrlav.grn.adapter.CollectionCenterListAdapter;
import fv.abrl.com.abrlav.grn.paracelable.GetCollectionCenterDataParacelable;
import fv.abrl.com.abrlav.grn.paracelable.GetCollectionCenterListParcelable;
import fv.abrl.com.abrlav.home.activity.HomeActivity;

import fv.abrl.com.abrlav.picking.store.adapter.PickbyStoreListSearchAdapter;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreDataParacable;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreListParacelable;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/14/2018.
 */

public class PickByStoreFragment extends Fragment {





    @BindView(R.id.collectioncenterName)
    TextView collectionCenterName;

    @BindView(R.id.spinnerlist)
    Spinner spinnerlist;

    @BindView(R.id.pickbyStoreListSearch)
    EditText pickbyStoreListSearch;

    @BindView(R.id.storelistrv)
    IndexFastScrollRecyclerView storelistrv;

    private Unbinder unbinder;
    Fragment fragment;

    @BindView(R.id.clearSearchText)
    ImageView clearSearchText;

    @BindView(R.id.indicator)
    AVLoadingIndicatorView indicator;


    PickbyStoreListSearchAdapter mAdapter;

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    public PickByStoreFragment() {
        // Required empty public constructor
    }

    ArrayList<PickingStoreListParacelable> storelist;
    ArrayList<GetCollectionCenterListParcelable> collectioncenterlist = new ArrayList<GetCollectionCenterListParcelable>();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        sharedPref              = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                  = sharedPref.edit();

        storelist = new ArrayList<PickingStoreListParacelable>();

        try{
        ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.bottombar)));
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "Pick By Store" + "</font>"));

        HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
        if (Build.VERSION.SDK_INT >= 21) {
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
        }

            }
        catch (Exception e){
                e.printStackTrace();
                }
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pickbystore, container, false);
        unbinder  = ButterKnife.bind(this, view);

        try{
            pickstoreList();
        }catch (Exception e){
            e.printStackTrace();
        }


        try{
            getCollectionCenterlist();
        }catch (Exception e){
            e.printStackTrace();
        }

        mAdapter                =       new PickbyStoreListSearchAdapter(getActivity(), storelist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        storelistrv.setLayoutManager(mLayoutManager);
        storelistrv.setItemAnimator(new DefaultItemAnimator());
        storelistrv.setIndexTextSize(12);
        storelistrv.setIndexBarColor("#ffffff");
        storelistrv.setIndexBarCornerRadius(0);
        storelistrv.setIndexbarMargin(0);
        storelistrv.setIndexbarWidth(40);
        storelistrv.setPreviewPadding(0);
        storelistrv.setIndexBarTextColor("#818181");
        storelistrv.setIndexBarVisibility(true);
        storelistrv.setIndexbarHighLateTextColor("#ed6c09");
        storelistrv.setIndexBarHighLateTextVisibility(true);
        storelistrv.setAdapter(mAdapter);



        return view;

    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        pickbyItemSubmit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                fragment = new PickbyStoreFragmentSecond();
//                FragmentTransaction transaction = getFragmentManager().beginTransaction();
//                transaction.replace(R.id.homeContainer,fragment);
//                transaction.commit();
//            }
//        });

        try{
            if(!sharedPref.getString("collectioncentername","default").equals("default")){
                collectionCenterName.setText(""+sharedPref.getString("collectioncentername","default"));
            }else{
                collectionCenterName.setText("Collection Center Name");
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        spinnerlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                editor.putString("collectioncentername",collectioncenterlist.get(position).getCCNAME()).apply();
                editor.putString("collectioncenterpostion",""+position).apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });




        storelistrv.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        try{
                            editor.putString("Storename",""+PickbyStoreListSearchAdapter.filteredarraylist.get(position).getSTORENAME());
                            editor.putString("storecode",""+PickbyStoreListSearchAdapter.filteredarraylist.get(position).getSTORE()).apply();

                            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                            fragment = new PickbyStoreFragmentSecond();
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            transaction.replace(R.id.homeContainer,fragment);
                            transaction.commit();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                })
        );

        collectionCenterName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Select Center");
                String[] animals = {"Andheri", "Bandra", "Borivoli", "Churchgate", "MumbaiCentral"};
                builder.setItems(animals, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if(which==0){
                            collectionCenterName.setText("Andheri");
                            editor.putString("collectioncentername","Andheri").apply();
                        }else if(which==1){
                            collectionCenterName.setText("Bandra");
                            editor.putString("collectioncentername","Bandra").apply();
                        }else if(which==2){
                            collectionCenterName.setText("Borivoli");
                            editor.putString("collectioncentername","Borivoli").apply();
                        }else if(which==3){
                            collectionCenterName.setText("Churchgate");
                            editor.putString("collectioncentername","Churchgate").apply();
                        }else if(which==4){
                            collectionCenterName.setText("MumbaiCentral");
                            editor.putString("collectioncentername","MumbaiCentral").apply();
                        }else {
                            collectionCenterName.setText("Collection Center Name");
                            editor.putString("collectioncentername","Collection Center Name").apply();
                        }

                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        pickbyStoreListSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

                mAdapter.getFilter().filter(arg0);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {


                mAdapter.getFilter().filter(arg0);

            }
        });


        clearSearchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickbyStoreListSearch.setText("");
            }
        });

    }

    private void pickstoreList() {

        indicator.setVisibility(View.VISIBLE);

        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getstorelist(sharedPref.getString("collectioncentercode","default"),new Callback<PickingStoreDataParacable>()

        {
            @Override
            public void success(PickingStoreDataParacable getccParacables, Response response) {


                if(0 != getccParacables.get$values().size()){
                    storelist.addAll(getccParacables.get$values());

                    mAdapter.notifyDataSetChanged();

                }else {
                    Toast.makeText(getActivity(),"No Item Found",Toast.LENGTH_SHORT).show();
                }
                indicator.setVisibility(View.GONE);

            }

            @Override
            public void failure(RetrofitError error) {

                indicator.setVisibility(View.GONE);
                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }

    private void getCollectionCenterlist() {



        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getCollectionList(new Callback<GetCollectionCenterDataParacelable>()

        {
            @Override
            public void success(GetCollectionCenterDataParacelable getccParacables, Response response) {

                collectioncenterlist.addAll(getccParacables.get$values());


                spinnerlist.setAdapter(new CollectionCenterListAdapter(getActivity(), collectioncenterlist));

                try{
                    if(!sharedPref.getString("collectioncenterpostion","default").equals("default")){
                        spinnerlist.setSelection(Integer.parseInt(sharedPref.getString("collectioncenterpostion","default")));
                        editor.putString("collectioncentercode",""+collectioncenterlist.get(0).getCC()).apply();}else{

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }


            }

            @Override
            public void failure(RetrofitError error) {


                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }
}