
package fv.abrl.com.abrlav.view.dispatch.parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DispatchViewStoresListParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("STORE_NAME")
    @Expose
    private String sTORENAME;
    @SerializedName("Store")
    @Expose
    private Integer store;


    public Integer getTo_Number() {
        return To_Number;
    }

    public void setTo_Number(Integer to_Number) {
        To_Number = to_Number;
    }

    @SerializedName("To_Number")
    @Expose
    private Integer To_Number;

    public String getPdf_path() {
        return pdf_path;
    }

    public void setPdf_path(String pdf_path) {
        this.pdf_path = pdf_path;
    }

    @SerializedName("PDF_path")
    @Expose
    private String pdf_path;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getSTORENAME() {
        return sTORENAME;
    }

    public void setSTORENAME(String sTORENAME) {
        this.sTORENAME = sTORENAME;
    }

    public Integer getStore() {
        return store;
    }

    public void setStore(Integer store) {
        this.store = store;
    }



}
