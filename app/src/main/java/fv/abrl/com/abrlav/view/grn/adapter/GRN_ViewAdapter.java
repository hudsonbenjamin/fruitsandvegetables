package fv.abrl.com.abrlav.view.grn.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.view.grn.parcelable.GRNApprovalItemListParcelable;

public class GRN_ViewAdapter extends RecyclerView.Adapter<GRN_ViewAdapter.MyViewHolder>
        implements Filterable {
private Context mContext;

private  ArrayList<GRNApprovalItemListParcelable> arrayList;
public static  ArrayList<GRNApprovalItemListParcelable> filteredarraylist;
private ArrayList<Integer> mSectionPositions;

public class MyViewHolder extends RecyclerView.ViewHolder {

    TextView invoicenumber,vendorcode,grn_number,vendorname;

    public MyViewHolder(View view) {
        super(view);

        invoicenumber = (TextView) view.findViewById(R.id.invoicenumber);
        vendorcode    = (TextView) view.findViewById(R.id.vendorcode);
        grn_number    = (TextView) view.findViewById(R.id.grn_number);
        vendorname    = (TextView) view.findViewById(R.id.vendornumber);

    }
}

    public GRN_ViewAdapter(Context c, ArrayList<GRNApprovalItemListParcelable> arrayList) {
        mContext = c;
        this.arrayList = arrayList;
        this.filteredarraylist = arrayList;
        //setHasStableIds(true);


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grn_view_adapter, parent, false);



        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

    holder.invoicenumber.setText(""+filteredarraylist.get(position).getGRNInvoiceNumber());
    holder.vendorcode.setText(""+filteredarraylist.get(position).getSUPPLIER());
    holder.grn_number.setText(""+filteredarraylist.get(position).getGRNNumber());
    holder.vendorname.setText(""+filteredarraylist.get(position).getSUPNAME());

    }

    @Override
    public int getItemCount() {
        return filteredarraylist.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredarraylist = arrayList;
                } else {
                    ArrayList<GRNApprovalItemListParcelable> filteredList = new ArrayList<>();
                    for (GRNApprovalItemListParcelable row : arrayList) {
                        if (row.getSUPNAME().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredarraylist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredarraylist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredarraylist = (ArrayList<GRNApprovalItemListParcelable>) filterResults.values;

                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }



}