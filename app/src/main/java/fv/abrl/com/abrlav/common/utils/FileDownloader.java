package fv.abrl.com.abrlav.common.utils;

import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Hudson on 7/23/2018.
 */



public class FileDownloader {
    private static final int  MEGABYTE = 1024 * 1024;

    public static void downloadFile(String fileUrl, File directory){
        try {

            URL url = new URL(fileUrl);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            //urlConnection.setRequestMethod("GET");
            //urlConnection.setDoOutput(true);
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            FileOutputStream fileOutputStream = new FileOutputStream(directory);
            int totalSize = urlConnection.getContentLength();

            byte[] buffer = new byte[MEGABYTE];
            int bufferLength = 0;
            while((bufferLength = inputStream.read(buffer))>0)
            fileOutputStream.write(buffer, 0, bufferLength);
            fileOutputStream.close();
        }

    catch (FileNotFoundException e) {
            Log.e("print-1","--"+e.getMessage());
        e.printStackTrace();
    } catch (MalformedURLException e) {
            Log.e("print-2","--"+e.getMessage());
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
            Log.e("print-3","--"+e.getMessage());
    }
    }}


