
package fv.abrl.com.abrlav.mpp.paracable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MPPItemListParacelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("ITEM")
    @Expose
    private int iTEM;
    @SerializedName("ITEM_DESC")
    @Expose
    private String iTEMDESC;
    @SerializedName("STANDARD_UOM")
    @Expose
    private String sTANDARDUOM;



    private int role = 1;
    private String Marketprice ="0";
    private int checkboxvalue = 0;
    private boolean isSelected;



    public int getCC_Code() {
        return CC_Code;
    }

    public void setCC_Code(int CC_Code) {
        this.CC_Code = CC_Code;
    }

    private int CC_Code ;

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }


    public MPPItemListParacelable(int itemcode ,String itemdesc, String marketprice, int checkboxvalue,String sTANDARDUOM,boolean isSelected,int cccode) {

        this.iTEM = itemcode;
        this.iTEMDESC = itemdesc;
        this.Marketprice = marketprice;
        this.checkboxvalue = checkboxvalue;
        this.sTANDARDUOM = sTANDARDUOM;
        this.isSelected = isSelected;
        this.CC_Code = cccode;
    }

    public String getMarketPrice() {
        return Marketprice;
    }

    public void setMarketPrice(String marketPrice) {
        Marketprice = marketPrice;
    }

    public int getCheckboxvalue() {
        return checkboxvalue;
    }

    public void setCheckboxvalue(int checkboxvalue) {
        this.checkboxvalue = checkboxvalue;
    }



    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getITEM() {
        return iTEM;
    }

    public void setITEM(int iTEM) {
        this.iTEM = iTEM;
    }

    public String getITEMDESC() {
        return iTEMDESC;
    }

    public void setITEMDESC(String iTEMDESC) {
        this.iTEMDESC = iTEMDESC;
    }

    public String getSTANDARDUOM() {
        return sTANDARDUOM;
    }

    public void setSTANDARDUOM(String sTANDARDUOM) {
        this.sTANDARDUOM = sTANDARDUOM;
    }

}
