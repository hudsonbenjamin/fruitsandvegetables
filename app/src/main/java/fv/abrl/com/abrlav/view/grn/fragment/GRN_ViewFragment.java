package fv.abrl.com.abrlav.view.grn.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.utils.FileDownloader;
import fv.abrl.com.abrlav.common.utils.RecyclerItemClickListener;
import fv.abrl.com.abrlav.grn.fragment.GrnItemSelectionCancelFragment;
import fv.abrl.com.abrlav.grn.fragment.GrnPrintFragment;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.view.grn.adapter.GRN_ViewAdapter;
import fv.abrl.com.abrlav.view.grn.parcelable.GRNApprovalItemListParcelable;
import fv.abrl.com.abrlav.view.grn.parcelable.GRNViewApprovalParcelable;
import fv.abrl.com.abrlav.view.grn.parcelable.GRNViewListPendingApprovalParcelable;
import fv.abrl.com.abrlav.view.grn.parcelable.GRNViewPendingApprovalParcelable;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GRN_ViewFragment extends Fragment {






    private Fragment fragment;
    private Unbinder unbinder;
    private View view;

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    @BindView(R.id.statusSpinnerList)
    Spinner statusSpinnerList;

    @BindView(R.id.dateSpinnerList)
    Spinner dateSpinnerList;


    public GRN_ViewFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.grnViewApprovalList)
    RecyclerView grnViewApprovalList;

    @BindView(R.id.clearSearchText)
    ImageView clearSearchText;

    @BindView(R.id.searchItems)
    EditText searchItems;

    @BindView(R.id.indicator)
    AVLoadingIndicatorView indicator;


    GRN_ViewAdapter mAdapter;

    ArrayList<GRNApprovalItemListParcelable> grnviewlist = new ArrayList<GRNApprovalItemListParcelable>();


    String itemselected;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view                    =  inflater.inflate(R.layout.fragment_grn_view, container, false);
        unbinder                =  ButterKnife.bind(this,view);

        sharedPref               = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                   = sharedPref.edit();



        try{
            ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                    .getColor(R.color.bottombar)));
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "GRN View" + "</font>"));

            HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
            if (Build.VERSION.SDK_INT >= 21) {
                getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
            }


        }catch (Exception e){
            e.printStackTrace();
        }

        mAdapter                =       new GRN_ViewAdapter(getActivity(), grnviewlist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        grnViewApprovalList.setLayoutManager(mLayoutManager);
        grnViewApprovalList.setItemAnimator(new DefaultItemAnimator());
        grnViewApprovalList.setAdapter(mAdapter);



        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
       // categories.add("Approval");
        categories.add("Submitted");
        categories.add("Saved");
        categories.add("Pending");

        List<String> datelist = new ArrayList<String>();
        datelist.add("Today");
        datelist.add("Yesterday");

        // Creating adapter for spinner
        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statusSpinnerList.setAdapter(categoryAdapter);

        ArrayAdapter<String> dateadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datelist);
        dateadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dateSpinnerList.setAdapter(dateadapter);


         statusSpinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                itemselected = parentView.getItemAtPosition(position).toString();

                      if(itemselected.equals("Saved")){
                          itemselected = "Drafted";
                      }

                try{
                    grnviewlist.clear();
                    mAdapter.notifyDataSetChanged();
                    getGRnView(itemselected);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        dateSpinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String item = parentView.getItemAtPosition(position).toString();

               // Toast.makeText(getActivity(), "Selected: " + item, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        clearSearchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchItems.setText("");
            }
        });


        grnViewApprovalList.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        try{



                            if(itemselected.equals("Pending")){

                                editor.putInt("passgrn_number",GRN_ViewAdapter.filteredarraylist.get(position).getGRNNumber()).apply();
                                fragment = new GRN_ApprovalFragment();
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                transaction.replace(R.id.homeContainer,fragment);
                                transaction.commit();

                            }

                            if(itemselected.equals("Submitted"))
                            {
                                 indicator.setVisibility(View.VISIBLE);
                                 new DownloadFile().execute((ApiConstants.pdfurl+"/"+GRN_ViewAdapter.filteredarraylist.get(position).getPdf_Path()).replace(" ","%20"));

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                })
        );

        searchItems.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try{
                    mAdapter.getFilter().filter(s);

                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                try{
                    mAdapter.getFilter().filter(s);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

    }


    private void getGRnView(String status) {

        indicator.setVisibility(View.VISIBLE);

        final RestAdapter adapter = new RestAdapter.Builder().setEndpoint(ApiConstants.baseurl).build();
        iHttpApiCall api = adapter.create(iHttpApiCall.class);


        api.getGRNViewList(status,new Callback<GRNViewApprovalParcelable>()

        {
            @Override
            public void success(GRNViewApprovalParcelable getccParacables, Response response) {

                try{
                    if(0 != getccParacables.get$values().size()){
                        grnviewlist.addAll(getccParacables.get$values());

                        mAdapter.notifyDataSetChanged();

                    }else{

                        Toast.makeText(getActivity(),"No Item Found",Toast.LENGTH_SHORT).show();
                    }
                    indicator.setVisibility(View.GONE);

                }catch (Exception e){
                    indicator.setVisibility(View.GONE);
                    e.printStackTrace();

                }







            }

            @Override
            public void failure(RetrofitError error) {

                indicator.setVisibility(View.GONE);
                Log.e("server","---"+error.getMessage());
                //Toast.makeText(getActivity(), "Network Issues", Toast.LENGTH_SHORT).show();


            }
        });

    }


    public void viewPDF()
    {

        indicator.setVisibility(View.GONE);
        // -> filename = maven.pdf

        // if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/FNV/" + "grn.pdf");
        Uri path = FileProvider.getUriForFile(getActivity(), "fv.abrl.com.abrlav.provider", pdfFile);
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        pdfIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(pdfIntent);

    }










    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "FNV");
            folder.mkdir();
            File pdfFile = new File(folder, "grn.pdf");

            try{
                pdfFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);
            return null;

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try{

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "Download PDf successfully", Toast.LENGTH_SHORT).show();
                        viewPDF();
                    }
                }, 5000);

            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }




}