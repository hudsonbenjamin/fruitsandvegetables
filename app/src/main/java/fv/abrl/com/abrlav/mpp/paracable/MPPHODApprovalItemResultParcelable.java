
package fv.abrl.com.abrlav.mpp.paracable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MPPHODApprovalItemResultParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("approvempp")
    @Expose
    private String approvempp;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getApprovempp() {
        return approvempp;
    }

    public void setApprovempp(String approvempp) {
        this.approvempp = approvempp;
    }

}
