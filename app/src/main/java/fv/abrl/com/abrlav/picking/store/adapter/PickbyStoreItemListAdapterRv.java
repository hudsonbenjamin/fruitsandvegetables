package fv.abrl.com.abrlav.picking.store.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreItemListParacelable;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreListParacelable;


public class PickbyStoreItemListAdapterRv extends RecyclerView.Adapter<PickbyStoreItemListAdapterRv.MyViewHolder>
        implements Filterable, SectionIndexer  {

    private Context mContext;
    ArrayList<PickingStoreItemListParacelable> realarrayList;
    ArrayList<PickingStoreItemListParacelable> arrayList;

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    private ArrayList<Integer> mSectionPositions;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CheckBox productname;
        TextView  storeSoH;
        EditText storeQunantityEt;

        public MyViewHolder(View grid) {
            super(grid);

             productname        = (CheckBox)  grid.findViewById(R.id.storeName);
             storeSoH           = (TextView)  grid.findViewById(R.id.storeSoH);
             storeQunantityEt   = (EditText)  grid.findViewById(R.id.storeQunantityEt);


        }

    }

    public PickbyStoreItemListAdapterRv(Context c, ArrayList<PickingStoreItemListParacelable> productlist ) {
        mContext                =   c;
        this.realarrayList      =   productlist;
        this.arrayList          =   productlist;
        sharedPref              =   c.getSharedPreferences("CallHippoSharedPreference", 0);
        editor                  =   sharedPref.edit();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_pickbystoreitemlist, parent, false);



        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {



        holder.productname.setText(""+realarrayList.get(position).getITEMDESC()+" ("+realarrayList.get(position).getSTANDARDUOM()+")");
        editor.putString("itemid",""+realarrayList.get(position).get$id());
        holder.storeSoH.setText("SOH : "+realarrayList.get(position).getSOH());

        if(realarrayList.get(position).getCheckitem()==1){
            holder.productname.setChecked(true);
        }else{
            holder.productname.setChecked(false);
        }

        if(realarrayList.get(position).getQuantity()==0){
            holder.storeQunantityEt.setText("");
        }else{
            holder.storeQunantityEt.setText(""+realarrayList.get(position).getQuantity());
        }


        try{
            if(realarrayList.get(position).getSTANDARDUOM().equals("EA")){
                holder.storeQunantityEt.setKeyListener(DigitsKeyListener.getInstance(true,false));

            }else{
                holder.storeQunantityEt.setKeyListener(DigitsKeyListener.getInstance(true,true));
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        holder.storeQunantityEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                try{
                    if(s.length()>0){

                        if(realarrayList.get(position).getSOH()>=Integer.parseInt(s.toString())){
                            realarrayList.get(position).setCheckitem(1);
                            realarrayList.get(position).setQuantity(Integer.parseInt( holder.storeQunantityEt.getText().toString()));
                            holder.productname.setChecked(true);
                        }else{
                            holder.storeQunantityEt.setText("");
                            Toast.makeText(mContext,"Only "+realarrayList.get(position).getSOH()+" quantity available", Toast.LENGTH_SHORT).show();
                        }


                    }else{

                        realarrayList.get(position).setCheckitem(0);
                        holder.productname.setChecked(false);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }



            }
        });


        holder.productname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(! holder.storeQunantityEt.getText().toString().isEmpty()) {

                    if ( holder.productname.isChecked()) {
                        realarrayList.get(position).setCheckitem(1);
                        realarrayList.get(position).setQuantity(Integer.parseInt( holder.storeQunantityEt.getText().toString()));


                    } else {
                        holder.storeQunantityEt.setText("");
                        realarrayList.get(position).setCheckitem(0);
                        holder.productname.setChecked(false);


                    }
                }else {
                    holder.storeQunantityEt.setText("");
                    realarrayList.get(position).setCheckitem(0);
                    holder.productname.setChecked(false);
                    Toast.makeText(mContext,"Please Enter Qunantity",Toast.LENGTH_SHORT).show();

                }
            }
        });
    }




            @Override
            public Filter getFilter() {
                return new Filter() {
                    @Override
                    protected FilterResults performFiltering(CharSequence charSequence) {
                        String charString = charSequence.toString();
                        if (charString.isEmpty()) {
                            realarrayList = arrayList;
                        } else {
                            ArrayList<PickingStoreItemListParacelable> filteredList = new ArrayList<>();
                            for (PickingStoreItemListParacelable row : arrayList) {
                                if (row.getITEMDESC().toLowerCase().contains(charString.toLowerCase())) {
                                    filteredList.add(row);
                                }
                            }

                            realarrayList = filteredList;
                        }

                        FilterResults filterResults = new FilterResults();
                        filterResults.values = realarrayList;
                        return filterResults;
                    }

                    @Override
                    protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                        realarrayList = (ArrayList<PickingStoreItemListParacelable>) filterResults.values;

                        notifyDataSetChanged();
                    }
                };
            }


            @Override
            public int getSectionForPosition(int position) {
                return 0;
            }

            @Override
            public Object[] getSections() {
                List<String> sections = new ArrayList<>(26);
                mSectionPositions = new ArrayList<>(26);
                for (int i = 0, size = realarrayList.size(); i < size; i++) {
                    String section = String.valueOf(realarrayList.get(i).getITEMDESC().charAt(0)).toUpperCase();
                    if (!sections.contains(section)) {
                        sections.add(section);
                        mSectionPositions.add(i);
                    }
                }
                return sections.toArray(new String[0]);
            }

            @Override
            public int getPositionForSection(int sectionIndex) {
                return mSectionPositions.get(sectionIndex);
            }


    @Override
    public int getItemCount() {

        return realarrayList.size();

    }

    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }

}

