
package fv.abrl.com.abrlav.view.dispatch.parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DispatchInnerViewParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<DispatchInnerViewItemListParcelable> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<DispatchInnerViewItemListParcelable> get$values() {
        return $values;
    }

    public void set$values(List<DispatchInnerViewItemListParcelable> $values) {
        this.$values = $values;
    }

}
