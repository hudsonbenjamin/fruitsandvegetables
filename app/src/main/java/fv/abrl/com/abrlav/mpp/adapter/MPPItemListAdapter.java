package fv.abrl.com.abrlav.mpp.adapter;


import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.SectionIndexer;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.mpp.fragment.MppEntryFragment;
import fv.abrl.com.abrlav.mpp.paracable.MPPItemListParacelable;


/**
 * Created by ravi on 16/11/17.
 */

public class MPPItemListAdapter extends RecyclerView.Adapter<MPPItemListAdapter.MyViewHolder>
     implements Filterable, SectionIndexer {
    // implements Filterable{

    private Context mContext;


    private ArrayList<MPPItemListParacelable> arrayList;
    private ArrayList<MPPItemListParacelable> filteredarraylist;
    private ArrayList<String> checkboxcount = new ArrayList<String>();
    private ArrayList<Integer> mSectionPositions;



    private MPPItemListParacelable getItem(int position) {
        return filteredarraylist.get(position);
    }
    //private ValueFilter valueFilter;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CheckBox ch;
        EditText getMarketPrice;

        public MyViewHolder(View view) {
            super(view);

             ch               = (CheckBox) view.findViewById(R.id.vegetableName);
             getMarketPrice   = (EditText) view.findViewById(R.id.getMarketPrice);


        }
    }


    public MPPItemListAdapter(Context c, ArrayList<MPPItemListParacelable> arrayList) {
        mContext = c;
        this.arrayList = arrayList;
        this.filteredarraylist = arrayList;
        //setHasStableIds(true);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_mpp_itemview, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {



        try{
            if(filteredarraylist.get(position).getITEMDESC() != null && !filteredarraylist.get(position).getITEMDESC().equals("")){
                holder.ch.setText(filteredarraylist.get(position).getITEMDESC()+" ("+filteredarraylist.get(position).getSTANDARDUOM()+")");
            }
            else{
                holder.ch.setText("");
            }
        }catch (Exception e){
            holder.ch.setText("");
        }

        try{
            if(filteredarraylist.get(position).getCheckboxvalue()==1&&(filteredarraylist.get(position).getITEMDESC() != null && !filteredarraylist.get(position).getITEMDESC().equals(""))){
            //if(filteredarraylist.get(position).getCheckboxvalue()==1){
                holder.ch.setChecked(true);

            }else {

                holder.ch.setChecked(false);
            }
        }catch (Exception e){

            holder.ch.setChecked(false);
        }


      try{
          if(filteredarraylist.get(position).getMarketPrice() != null && !filteredarraylist.get(position).getMarketPrice().equals("")&& !filteredarraylist.get(position).getMarketPrice().equals("0")){

              holder.getMarketPrice.setText(""+filteredarraylist.get(position).getMarketPrice());

          }else{
              holder.getMarketPrice.setText("");

          }
      }catch (Exception e){
          holder.getMarketPrice.setText("");

      }


//        holder.getMarketPrice.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    hideKeyboard(v);
//                }
//            }
//        });

        holder.getMarketPrice.setKeyListener(DigitsKeyListener.getInstance(true,true));
        holder.getMarketPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.length()>0){

                    checkboxvaluereturn();
                    filteredarraylist.get(position).setCheckboxvalue(1);
                    filteredarraylist.get(position).setMarketPrice(holder.getMarketPrice.getText().toString());
                    holder.ch.setChecked(true);


                }else{
                    filteredarraylist.get(position).setMarketPrice("");
                    checkboxvaluereturn();
                    filteredarraylist.get(position).setCheckboxvalue(0);
                    holder.ch.setChecked(false);
                }




                // TODO Auto-generated method stub
            }
        });

        holder.ch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



              if(!holder.getMarketPrice.getText().toString().isEmpty()){
                    if(holder.ch.isChecked())
                    {



                        holder.ch.setChecked(true);
                        filteredarraylist.get(position).setCheckboxvalue(1);
                        filteredarraylist.get(position).setMarketPrice(holder.getMarketPrice.getText().toString());


                    }
                    else
                    {
                        checkboxvaluereturn();
                        holder.getMarketPrice.setText("");
                        holder.ch.setChecked(false);
                        filteredarraylist.get(position).setCheckboxvalue(0);


                    }
                }else{

                   filteredarraylist.get(position).setCheckboxvalue(0);
                    holder.ch.setChecked(false);
                    Toast.makeText(mContext,"Please Enter Market Price",Toast.LENGTH_SHORT).show();

                }
                //  Toast.makeText(mContext,"-"+web[position]+"----"+position,Toast.LENGTH_SHORT).show();

            }
        });


    }






    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                ArrayList<MPPItemListParacelable> filteredList = new ArrayList<>();
                if (charString.isEmpty()) {
                    filteredarraylist = arrayList;

                } else {

                    for (MPPItemListParacelable row : arrayList) {
                        if (row.getITEMDESC().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredarraylist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredarraylist;
                filterResults.count = filteredarraylist.size();
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                filteredarraylist = (ArrayList<MPPItemListParacelable>) filterResults.values;
                notifyDataSetChanged();



            }
        };
    }


    @Override
    public int getItemCount() {

        return filteredarraylist.size();

    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
    public long getItemId(int position) {
        return position;
    }

    private void checkboxvaluereturn(){
        try{
            checkboxcount.clear();
            for(int i=0;i<filteredarraylist.size();i++){
                if(filteredarraylist.get(i).getCheckboxvalue()==1){

                    checkboxcount.add(filteredarraylist.get(i).getITEMDESC());
                }


            }
            MppEntryFragment.selectedMPPListCount.setText("Lines : "+checkboxcount.size());
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>(26);
        mSectionPositions = new ArrayList<>(26);
        for (int i = 0, size = filteredarraylist.size(); i < size; i++) {
            String section = String.valueOf(filteredarraylist.get(i).getITEMDESC().charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mSectionPositions.get(sectionIndex);
    }




    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


}
