
package fv.abrl.com.abrlav.picking.store.paracelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import fv.abrl.com.abrlav.grn.paracelable.$value;

public class PickingStoreDataParacable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<PickingStoreListParacelable> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<PickingStoreListParacelable> get$values() {
        return $values;
    }

    public void set$values(List<PickingStoreListParacelable> $values) {
        this.$values = $values;
    }

}
