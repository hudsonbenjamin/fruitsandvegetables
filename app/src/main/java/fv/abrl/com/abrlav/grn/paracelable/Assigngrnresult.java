
package fv.abrl.com.abrlav.grn.paracelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Assigngrnresult {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private String resultvalue;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getResultvalue() {
        return resultvalue;
    }

    public void setResultvalue(String resultvalue) {
        this.resultvalue = resultvalue;
    }

}
