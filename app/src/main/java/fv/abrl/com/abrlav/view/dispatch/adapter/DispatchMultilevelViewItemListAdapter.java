package fv.abrl.com.abrlav.view.dispatch.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.dispatch.paracelable.DispatchItemListParacelable;
import fv.abrl.com.abrlav.view.dispatch.parcelable.DispatchInnerViewItemListParcelable;

/**
 * Created by Hudson on 7/17/2018.
 */

public class DispatchMultilevelViewItemListAdapter extends BaseAdapter {
    private Context mContext;


    ArrayList<DispatchInnerViewItemListParcelable> arrayList;

    public DispatchMultilevelViewItemListAdapter(Context c, ArrayList<DispatchInnerViewItemListParcelable> arrayList) {
        mContext = c;

        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        final DispatchInnerViewItemListParcelable retrivedata = arrayList.get(position);


        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid                                   =  new View(mContext);
            grid                                   =  inflater.inflate(R.layout.adapter_dispatch_selectstore_productlist, null);
            final TextView itemName                = (TextView) grid.findViewById(R.id.itemName);
            final TextView itemCode                = (TextView) grid.findViewById(R.id.itemCode);
            final TextView quantity                = (TextView) grid.findViewById(R.id.quanatity);
            final TextView cost                    = (TextView) grid.findViewById(R.id.cost);



            itemName.setText(""+retrivedata.getITEMDESC());

            quantity.setText("Quantity : "+retrivedata.getQuantity());
            cost.setText("Cost : "+retrivedata.getGRNCost());


        } else {
            grid = (View) convertView;
        }



        return grid;
    }


}
