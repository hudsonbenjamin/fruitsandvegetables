package fv.abrl.com.abrlav.picking.store.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import fv.abrl.com.abrlav.R;
import fv.abrl.com.abrlav.common.interfaces.iHttpApiCall;
import fv.abrl.com.abrlav.common.severapi.ApiConstants;
import fv.abrl.com.abrlav.common.utils.CommonFunction;
import fv.abrl.com.abrlav.home.activity.HomeActivity;
import fv.abrl.com.abrlav.picking.fragment.PickingMenuFragment;
import fv.abrl.com.abrlav.picking.store.adapter.PickbyStoreItemListAdapterRv;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreItemDataParacable;
import fv.abrl.com.abrlav.picking.store.paracelable.PickingStoreItemListParacelable;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Hudson on 7/14/2018.
 */

public class PickbyStoreFragmentSecondCancel extends Fragment {


    @BindView(R.id.pickbystoresecondSubmit)
    TextView pickbyItemSubmit;

    @BindView(R.id.storeNameTitle)
    TextView storeNameTitle;

    @BindView(R.id.productlistlv)
    IndexFastScrollRecyclerView productlistlv;

    @BindView(R.id.collectioncentername)
    TextView collectioncentername;

    private Unbinder unbinder;
    Fragment fragment;

    @BindView(R.id.clearSearchText)
   ImageView clearSearchText;

    @BindView(R.id.searchItems)
    EditText searchItems;

    @BindView(R.id.pickbystoreSecondCancel)
    TextView pickbystoreSecondCancel;

    @BindView(R.id.indicator)
    AVLoadingIndicatorView indicator;

    private SharedPreferences sharedPref = null;
    private SharedPreferences.Editor editor;

    public PickbyStoreFragmentSecondCancel() {
        // Required empty public constructor
    }

    PickbyStoreItemListAdapterRv mAdapter;






    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        sharedPref              = getActivity().getSharedPreferences("CallHippoSharedPreference", 0);
        editor                  = sharedPref.edit();





            try{
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                            .getColor(R.color.bottombar)));
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#4F4D4E'>" + "Pick By Store" + "</font>"));

                    HomeActivity.toolbar.setNavigationIcon(R.drawable.menu_icon_color_resize);
                    if (Build.VERSION.SDK_INT >= 21) {
                        getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorstatusbargrey));
                    }
                }
            catch (Exception e){
                    e.printStackTrace();
                }

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pickbystorefragmentsecond, container, false);
        unbinder = ButterKnife.bind(this, view);

        try{
            if (!sharedPref.getString("Storename","default").equals("default")){
                storeNameTitle.setText(""+sharedPref.getString("Storename","default")+" - "+sharedPref.getString("storecode","default"));
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        mAdapter                =       new PickbyStoreItemListAdapterRv(getActivity(), PickbyStoreFragmentSecond.cancelsenddata);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        productlistlv.setLayoutManager(mLayoutManager);
        productlistlv.setItemAnimator(new DefaultItemAnimator());
        productlistlv.setItemViewCacheSize(PickbyStoreFragmentSecond.cancelsenddata.size());
        productlistlv.setIndexTextSize(12);
        productlistlv.setIndexBarColor("#ffffff");
        productlistlv.setIndexBarCornerRadius(0);
        productlistlv.setIndexbarMargin(0);
        productlistlv.setIndexbarWidth(40);
        productlistlv.setPreviewPadding(0);
        productlistlv.setIndexBarTextColor("#818181");
        productlistlv.setIndexBarVisibility(true);
        productlistlv.setIndexbarHighLateTextColor("#ed6c09");
        productlistlv.setIndexBarHighLateTextVisibility(true);
        productlistlv.setAdapter(mAdapter);




        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pickbystoreSecondCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new PickingMenuFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer,fragment);
                transaction.commit();
            }
        });

        pickbyItemSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PickbyStoreFragmentSecond.selectedItem.clear();


                try{
                    for(int i =0;i<PickbyStoreFragmentSecond.cancelsenddata.size();i++){
                        if(PickbyStoreFragmentSecond.cancelsenddata.get(i).getCheckitem()==1){

                            PickingStoreItemListParacelable s1 = new PickingStoreItemListParacelable(
                                    PickbyStoreFragmentSecond.cancelsenddata.get(i).get$id(),
                                    PickbyStoreFragmentSecond.cancelsenddata.get(i).getITEMDESC(),
                                    PickbyStoreFragmentSecond.cancelsenddata.get(i).getITEM(),
                                    PickbyStoreFragmentSecond.cancelsenddata.get(i).getQuantity(),
                                    1,
                                    PickbyStoreFragmentSecond.cancelsenddata.get(i).getSTANDARDUOM(),
                                    sharedPref.getString("storecode","default"),
                                    sharedPref.getString("Storename","default"),
                                    PickbyStoreFragmentSecond.cancelsenddata.get(i).getSOH(),Integer.parseInt(sharedPref.getString("collectioncentercode","default")));
                               PickbyStoreFragmentSecond.selectedItem.add(s1);


                        }
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                try{
                    if( PickbyStoreFragmentSecond.selectedItem.size()>0) {


                        if(CommonFunction.isNetworkAvailable(getActivity())) {
                            showDialog(getActivity(),"Do you wish to continue?");
                        }else{
                            Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                        }


                    }else{
                        Toast.makeText(getActivity(),"Please select atleast one item",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        try{
            if(!sharedPref.getString("collectioncentername","default").equals("default")){
                collectioncentername.setText(""+sharedPref.getString("collectioncentername","default"));
            }else{
                collectioncentername.setText("Collection Center Name");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        clearSearchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchItems.setText("");
            }
        });

        searchItems.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                mAdapter.getFilter().filter(arg0);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {


                mAdapter.getFilter().filter(arg0);

            }
        });

    }



    public void showDialog(Activity activity, String msg){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_customsubmit);

        TextView text = (TextView) dialog.findViewById(R.id.customDialogMessage);
        text.setText(msg);

        TextView customSubmit = (TextView) dialog.findViewById(R.id.customSubmit);
        ImageView closeDialog = (ImageView) dialog.findViewById(R.id.closeDialog);
        TextView customDraft = (TextView) dialog.findViewById(R.id.customDraft);

        customDraft.setText("Cancel");

        customSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                fragment = new PickByStoreFragmentComplete();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.homeContainer, fragment);
                transaction.commit();

            }
        });

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        customDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();

    }
}