
package fv.abrl.com.abrlav.dispatch.paracelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrintToParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<PrintToItemParcelable> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<PrintToItemParcelable> get$values() {
        return $values;
    }

    public void set$values(List<PrintToItemParcelable> $values) {
        this.$values = $values;
    }

}
