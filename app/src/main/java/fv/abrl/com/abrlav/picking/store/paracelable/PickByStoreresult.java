
package fv.abrl.com.abrlav.picking.store.paracelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickByStoreresult {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("PickByStoreresultvalue")
    @Expose
    private String pickByStoreresultvalue;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getPickByStoreresultvalue() {
        return pickByStoreresultvalue;
    }

    public void setPickByStoreresultvalue(String pickByStoreresultvalue) {
        this.pickByStoreresultvalue = pickByStoreresultvalue;
    }
    }
