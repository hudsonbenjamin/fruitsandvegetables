
package fv.abrl.com.abrlav.mpp.paracable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import fv.abrl.com.abrlav.grn.paracelable.*;

public class MPPHODApprovalDataParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<MPPHODApprovalItemResultParcelable> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<MPPHODApprovalItemResultParcelable> get$values() {
        return $values;
    }

    public void set$values(List<MPPHODApprovalItemResultParcelable> $values) {
        this.$values = $values;
    }

}
