
package fv.abrl.com.abrlav.picking.route.parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickingRouteDataParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<PickingRouteListParcelable> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<PickingRouteListParcelable> get$values() {
        return $values;
    }

    public void set$values(List<PickingRouteListParcelable> $values) {
        this.$values = $values;
    }

}
