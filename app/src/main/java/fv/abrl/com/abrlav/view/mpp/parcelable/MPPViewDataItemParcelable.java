
package fv.abrl.com.abrlav.view.mpp.parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MPPViewDataItemParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("Y_MPP")
    @Expose
    private Integer yMPP;
    @SerializedName("Market_Price")
    @Expose
    private Integer marketPrice;
    @SerializedName("MPP")
    @Expose
    private Double mPP;
    @SerializedName("ITEM")
    @Expose
    private Integer iTEM;
    @SerializedName("ITEM_DESC")
    @Expose
    private String iTEMDESC;
    @SerializedName("STANDARD_UOM")
    @Expose
    private String sTANDARDUOM;

    @SerializedName("FMPP")
    @Expose
    private String FMPP;

    public String getFMPP() {
        return FMPP;
    }

    public void setFMPP(String FMPP) {
        this.FMPP = FMPP;
    }



    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public Integer getYMPP() {
        return yMPP;
    }

    public void setYMPP(Integer yMPP) {
        this.yMPP = yMPP;
    }

    public Integer getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Integer marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Double getMPP() {
        return mPP;
    }

    public void setMPP(Double mPP) {
        this.mPP = mPP;
    }

    public Integer getITEM() {
        return iTEM;
    }

    public void setITEM(Integer iTEM) {
        this.iTEM = iTEM;
    }

    public String getITEMDESC() {
        return iTEMDESC;
    }

    public void setITEMDESC(String iTEMDESC) {
        this.iTEMDESC = iTEMDESC;
    }

    public String getSTANDARDUOM() {
        return sTANDARDUOM;
    }

    public void setSTANDARDUOM(String sTANDARDUOM) {
        this.sTANDARDUOM = sTANDARDUOM;
    }

}
