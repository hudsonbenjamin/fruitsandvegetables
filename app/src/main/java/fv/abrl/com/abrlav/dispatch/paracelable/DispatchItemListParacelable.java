
package fv.abrl.com.abrlav.dispatch.paracelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DispatchItemListParacelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("ITEM")
    @Expose
    private int iTEM;
    @SerializedName("ITEM_DESC")
    @Expose
    private String iTEMDESC;


    @SerializedName("GRN_Quantity")
    @Expose
    private int quantity;

    public int getDispatchviewquantity() {
        return dispatchviewquantity;
    }

    public void setDispatchviewquantity(int dispatchviewquantity) {
        this.dispatchviewquantity = dispatchviewquantity;
    }

    @SerializedName("Quantity")
    @Expose
    private int dispatchviewquantity;

    @SerializedName("MPP")
    @Expose
    private int mPP;

    @SerializedName("STANDARD_UOM")
    @Expose
    private String sTANDARDUOM;

    @SerializedName("GRN_Cost")
    @Expose
    private int cost;

    @SerializedName("SOH")
    @Expose
    private int SOH;

    public int getSOH() {
        return SOH;
    }

    public void setSOH(int SOH) {
        this.SOH = SOH;
    }

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getITEM() {
        return iTEM;
    }

    public void setITEM(int iTEM) {
        this.iTEM = iTEM;
    }

    public String getITEMDESC() {
        return iTEMDESC;
    }

    public void setITEMDESC(String iTEMDESC) {
        this.iTEMDESC = iTEMDESC;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getMPP() {
        return mPP;
    }

    public void setMPP(int mPP) {
        this.mPP = mPP;
    }

    public String getSTANDARDUOM() {
        return sTANDARDUOM;
    }

    public void setSTANDARDUOM(String sTANDARDUOM) {
        this.sTANDARDUOM = sTANDARDUOM;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

}
