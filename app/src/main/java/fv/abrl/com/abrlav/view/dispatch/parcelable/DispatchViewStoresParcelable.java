
package fv.abrl.com.abrlav.view.dispatch.parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DispatchViewStoresParcelable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("$values")
    @Expose
    private List<DispatchViewStoresListParcelable> $values = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public List<DispatchViewStoresListParcelable> get$values() {
        return $values;
    }

    public void set$values(List<DispatchViewStoresListParcelable> $values) {
        this.$values = $values;
    }

}
